<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once( BASEPATH .'database/DB.php');

if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
    $config['base_url'] = "http://localhost/motoralkatresznagyker.dev/";
} else {
    $config['base_url'] = "http://motoralkatresznagyker.com";
}

$uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$uri = $full_uri = str_replace($config['base_url'],"",$uri);
$uri = explode('/',$uri);
$uri = $uri[0];

$route['default_controller'] = 'frontend/home';
$route['/'] = 'frontend/home';
$route['kereses/(:any)'] = 'frontend/search';

if(!empty($uri)) {
	$db =& DB();

	$query = $db->query("
				SELECT router_method, router_controller FROM router WHERE router_slug = '".$uri."'
				UNION
				SELECT 'item_list', 'frontend_item' FROM item WHERE item_slug = '".$uri."'
				UNION
				SELECT 'page_list', 'frontend_cms' FROM cms WHERE cms_slug = '".$uri."'
				UNION
				SELECT 'category_list', 'frontend_category' FROM category_import WHERE
				slug = '".$uri."' OR
				slug2 = '".$uri."' OR
				slug3 = '".$uri."' OR
				slug4 = '".$uri."' OR
				slug5 = '".$uri."' OR
				slug6 = '".$uri."' ");


    if($query->num_rows() == 1) {
        $result = $query->result();

        foreach( $result as $row )
        {
            $route[$uri] = $row->router_controller."/".$row->router_method;
            $route[$uri.'/:any'] = $row->router_controller."/".$row->router_method;
            $route[$row->router_controller] = 'error404';
            $route[$row->router_controller.'/:any']   = 'error404';
            break;
        }
        $route['sitemap\.xml'] = "frontend/sitemap";
    }
    else {
        $route[$full_uri] = "frontend/redirect";
    }

}

$route['404_override'] = 'home/error';
$route['translate_uri_dashes'] = FALSE;
