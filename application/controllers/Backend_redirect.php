<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Backend_redirect extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('redirect_model', 'redirect');
    }
    /**
     * redirect list.
     *
     * @return string Show redirect list page on admin
     */
    public function redirect_list()
    {
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/jquery.dataTables.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/dataTables.bootstrap.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/slimScroll/jquery.slimscroll.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/fastclick/fastclick.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/dropzone-master/dist/dropzone.js');
        $this->enqueue->enqueue_js('template/backend/modules/redirect/redirect_list.js');

        $this->enqueue->enqueue_css('template/backend/plugins/dropzone-master/dist/basic.css');
        $this->enqueue->enqueue_css('template/backend/plugins/dropzone-master/dist/dropzone.css');
        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');
        $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');

        $this->load->view('backend/header.php');
        $this->load->view('backend/redirect/redirect_list.php');
        $this->load->view('backend/footer.php');
    }
    /**
     * Load template to modal.
     *
     * @return string
     */
    public function redirect_modal()
    {
        $post = $this->input->post();
        $data = [];
        if (isset($post['id']) && !empty($post['id'])) {
            $data = $this->redirect->redirect_get((int) $post['id']);
        }

        $html = $this->load->view('backend/redirect/redirect_'.$post['type'].'.php', $data, true);
        $msg['csrf'] = csrf();
        $msg['data'] = $html;
        echo json_encode($msg);
    }

    public function redirect_datatable()
    {
        if ($this->input->post()) {
            // Datatable columns
            $columns[0] = 'redirect_from';
            $columns[1] = 'redirect_to';
            $columns[2] = 'redirect_type';

            // Datatable variables
            $limit = $this->input->post('length');
            $offset = $this->input->post('start');
            $search = $this->input->post('search') ['value'];
            $order = $this->input->post('order');
            $orderby = $order[0]['dir'];
            $order = $columns[$order[0]['column']];

            //
            $categories_fil_cnt = $this->redirect->redirect_fil_cnt($order, $orderby, $columns, $search);
            $categories_all_cnt = $this->redirect->redirect_all_cnt();
            $categories_all = $this->redirect->redirect_datatable($limit, $offset, $order, $orderby, $columns, $search);

            // Prepare datatable json data
            $categories['draw'] = $this->input->post('draw');
            $categories['recordsTotal'] = $categories_all_cnt;
            $categories['recordsFiltered'] = $categories_fil_cnt;
            $categories['data'] = [];
            $categories['csrf'] = csrf();

            if ($categories_all) {
                foreach ($categories_all as $redirect) {
                    $buttons = '<td style="width:100px;"><button class="btn btn-info" role="button" data-redirect-edit="'.$redirect->redirect_id.'"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger" role="button" data-redirect-delete="'.$redirect->redirect_id.'"><i class="fa fa-times"></i></button></td>';
                    $row[0] = $redirect->redirect_from;
                    $row[1] = $redirect->redirect_to;
                    $row[2] = $redirect->redirect_type;
                    $row[3] = $buttons;
                    $categories['data'][] = $row;
                    unset($row, $buttons);
                }
            }

            echo json_encode($categories);
            exit();
        }
    }
    /**
     * Add redirect.
     *
     * @return string
     */
    public function redirect_add()
    {
        $post = $this->input->post();
        if ($post) {
            foreach ($post as $postname => $postvalue) {
                if (!empty($postvalue)) {
                    $model[$postname] = $postvalue;
                }
            }

            if(empty($model['redirect_from'])) {
                $msg['error']['redirect_from'] = 'A mező kitöltése kötelező!';
            }

            if(empty($model['redirect_to'])) {
                $msg['error']['redirect_to'] = 'A mező kitöltése kötelező!';
            }

            if(empty($model['redirect_type'])) {
                $msg['error']['redirect_type'] = 'Az átirányitás kódja kötelező!';
            }

            if(empty($msg))
            {
                $redirect = $this->redirect->redirect_add($model);
                if ($redirect) {
                    $msg['toastr']['success'] = 'Az átirányítás sikeresen hozzáadásra került!';
                } else {
                    $msg['toastr']['error'] = 'Az átirányítást nem sikerült hozzáadni!';
                }
            } else {
                $msg['toastr']['error'] = 'Az átirányítást nem sikerült hozzáadni!';
            }
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }

    public function redirect_edit()
    {
        $post = $this->input->post();
        if ($post) {
            foreach ($post as $postname => $postvalue) {
                if (!empty($postvalue)) {
                    $model[$postname] = $postvalue;
                }
            }

            $redirect_id = $model['redirect_id'];
            unset($model['redirect_id']);

            if(empty($model['redirect_from'])) {
                $msg['error']['redirect_from'] = 'A mező kitöltése kötelező!';
            }

            if(empty($model['redirect_to'])) {
                $msg['error']['redirect_to'] = 'A mező kitöltése kötelező!';
            }

            if(empty($model['redirect_type'])) {
                $msg['error']['redirect_type'] = 'Az átirányitás kódja kötelező!';
            }

            if(empty($msg)) {
                $redirect = $this->redirect->redirect_update($redirect_id, $model);
                if ($redirect) {
                    $msg['toastr']['success'] = 'Az átirányítás sikeresen módosításra került!';
                } else {
                    $msg['toastr']['error'] = 'Az átirányítást nem sikerült módosítani!';
                }
            } else {
                $msg['toastr']['error'] = 'Az átirányítást nem sikerült módosítani!';
            }
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }

    public function redirect_delete()
    {
        $redirect_id = $this->input->post('redirect_id');

        if ($redirect_id) {
            $msg['toastr']['success'] = 'Az átirányítás törlésre került!';
            $coupon = $this->redirect->redirect_delete($redirect_id);
        } else {
            $msg['toastr']['error'] = 'Az átirányítást nem sikerült';
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }
}
