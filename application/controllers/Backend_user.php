<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Backend_user extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('user_model');
	}

	public function user_list() {

		$header['js'][] = 'template/backend/plugins/datatables/jquery.dataTables.min.js';
		$header['js'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.min.js';
		$header['js'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.min.js';
		$header['js'][] = 'template/backend/plugins/slimScroll/jquery.slimscroll.min.js';
		$header['js'][] = 'template/backend/plugins/fastclick/fastclick.min.js';
		$header['js'][] = 'template/backend/plugins/toastr/toastr.min.js';
		$header['js'][] = 'template/backend/js/pages/user_list.js';

		$header['css'][] = 'template/backend/plugins/toastr/toastr.min.css';
		$header['css'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.css';

		$data['users'] = $this->user_datatable();
		
		$this->load->view('backend/header.php', $header);
		$this->load->view('backend/user/user_list.php', $data);
		$this->load->view('backend/footer.php');
	}

	public function user_datatable() {
		$html = "";
		
		$users = $this->user_model->user_all();
		if (!empty($users)) {
			foreach ($users as $user) {
				$html .= "<tr>";
				$html .= '<td style="width:20px;">' . $user->user_id . '</td>';
				$html .= '<td><b>' . $user->user_firstname . '</b></td>';
				$html .= '<td>' . $user->user_lastname . '</td>';
				$html .= '<td><b>' . $user->user_nickname . '</b></td>';
				$html .= '<td>' . $user->user_email . '</td>';
				$html .= '<td>' . $user->user_type . '</td>';
				$html .= '<td style="width:30px;">' . ($user->user_status == 1 ? '<i class="fa fa-check fa-2x has-success green"></i>' : '<i class="fa fa-times fa-2x red"></i>') . '</td>';
				$html .= '<td style="width:100px;"><button class="btn btn-info" role="button" data-user-edit="' . $user->user_id . '"><i class="fa fa-pencil"></i></button>
              <button class="btn btn-danger" role="button" data-user-delete="' . $user->user_id . '"><i class="fa fa-times"></i></button></td>';
				$html .= "</tr>";
			}
		}
		return $html;
	}

	public function user_modal() {
		$post = $this->input->post();
		$data = [];

		if(isset($post['id']) && !empty($post['id'])) {
			$data = $this->user_model->user_get((int)$post['id']);
		}

		$html = $this->load->view('backend/user/user_'.$post['type'].'.php',$data, true);

		$msg['csrf'] = csrf();
		$msg['data'] = $html;
		echo json_encode($msg);
	}

	public function user_add() {
		$post = $this->input->post();
		if ($post) {
			foreach($post as $postname => $postvalue){
				if(!empty($postvalue)){
					$model[$postname] = $postvalue;
				}
			}
			
			if(empty($model['user_nickname'])){
					$msg['error']['user_nickname'] = "A mező kitöltése kötelező!";
			}

			if(empty($model['user_email'])){
					$msg['error']['user_email'] = "A mező kitöltése kötelező!";
			}

			if(!empty($model['user_password'])){
				
				$options = [
				    'cost' => 11,
				    'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
				];

				$model['user_password'] = password_hash($model['user_password'], PASSWORD_BCRYPT, $options);
			}

			if(isset($model['user_nickname'])) {
				$check_user_nickname = $this->user_model->user_exist('user_nickname',$model['user_nickname']);
	
			   	if($check_user_nickname) {
			   	  	$msg['error']['user_nickname'] = "A felhasználó név már használva van!";
			   	}
			}

			if(isset($model['user_email'])) {
				$check_user_email = $this->user_model->user_exist('user_email',$model['user_nickname']);
	
			   	if($check_user_email) {
			   	  	$msg['error']['user_email'] = "A felhasználó név már használva van!";
			   	}
			}

			if(empty($msg)){
				$user = $this->user_model->user_add($model);
				if ($user) {
					$msg['toastr']['success'] = "A felhasználó sikeresen hozzáadásra került!";
				} else {
					$msg['toastr']['error'] = "A felhasználót nem sikerült hozzáadni!";
				}
			}
			else 
			{
				$msg['toastr']['error'] = "A felhasználót e-mail címe sikerült hozzáadni!";
			}
		}

		$msg['csrf'] = csrf();
		$msg['data'] = $this->user_datatable();
		echo json_encode($msg);
	}

	public function user_edit() {
		$post = $this->input->post();
		if ($post) {
			foreach($post as $postname => $postvalue){
				if(!empty($postvalue)){
					$model[$postname] = $postvalue;
				}
			}

			if(empty($model['user_nickname'])){
					$msg['error']['user_nickname'] = "A mező kitöltése kötelező!";
			}

			if(empty($model['user_email'])){
					$msg['error']['user_email'] = "A mező kitöltése kötelező!";
			}

			if(!empty($model['user_password'])){
				
				$options = [
				    'cost' => 11,
				    'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
				];

				$model['user_password'] = password_hash($model['user_password'], PASSWORD_BCRYPT, $options);
			} else {
				unset($model['user_password']);
			}

			if(empty($msg)){
				
				$user_id = $model["user_id"];
				$user_nickname_original = $model["user_nickname_original"];
				$user_email_original = $model["user_email_original"];
				
				unset($model["user_id"]);
				unset($model["user_nickname_original"]);
				unset($model["user_email_original"]);

				if($user_nickname_original != $model['user_nickname']) {
				   $check_user_nickname = $this->user_model->user_exist('user_nickname',$model['user_nickname']);
			
				   if($check_user_nickname) {
				   	  $msg['error']['user_nickname'] = "A felhasználó név már használva van!";
				   }
				}

				if($user_email_original != $model['user_email']) {
				   $check_user_email = $this->user_model->user_exist('user_email',$model['user_email']);
			
				   if($check_user_email) {
				   	  $msg['error']['user_email'] = "A felhasználó e-mail címe már használva van!";
				   }
				}
			}

			if(empty($msg)){
				$user = $this->user_model->user_update($user_id, $model);
				if ($user) {
					$msg['toastr']['success'] = "A felhasználó sikeresen módosításra került!";
				} else {
					$msg['toastr']['error'] = "A felhasználót nem sikerült módosítani!";
				}
			}
			else 
			{
				$msg['toastr']['error'] = "A felhasználót nem sikerült módosítani!";
			}
		}

		$msg['csrf'] = csrf();
		$msg['data'] = $this->user_datatable();
		echo json_encode($msg);
	}

	/**
	 * [user_delete description]
	 * @return [type] [description]
	 */
	public function user_delete() {
		$user_id = $this->input->post("user_id");
		
		if($user_id) 
		{
			$msg['toastr']['success'] = "A felhasználó törlésre került!";
			$user = $this->user_model->user_delete($user_id);
		}
		else 
		{
			$msg['toastr']['error'] = "A törlés nem sikerült";
		}

		$msg['csrf'] = csrf();
		$msg['data'] = $this->user_datatable();
		echo json_encode($msg);
	}
}