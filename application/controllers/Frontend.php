<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Frontend extends CI_Controller
{
    /**
     * Frontend::__construct().
     *
     * @return
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('item_model');
        $this->load->model('category_model');
    }
    /**
     * Frontend::home().
     *
     * @return
     */
    public function home()
    {
        $this->enqueue->enqueue_css('template/frontend/assets/css/bootstrap.min.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/chosen/chosen.min.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-outline/pictopro-outline.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-normal/pictopro-normal.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/colorbox/colorbox.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/jslider/bin/jquery.slider.min.css');
        $this->enqueue->enqueue_css('template/frontend/assets/css/carat.css');
        $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');
        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');
        $this->enqueue->enqueue_css('template/backend/plugins/font-awesome/css/font-awesome.min.css');

        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.js');
        $this->enqueue->enqueue_js('http://code.jquery.com/jquery-migrate-1.2.1.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.ui.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/jquery.dataTables.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/dataTables.bootstrap.min.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/bootstrap.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
        $this->enqueue->enqueue_js('template/frontend/modules/home/cart.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/carat.js');
        $this->enqueue->enqueue_js('template/frontend/modules/home/home.js');

        $data['categories'] = $this->category_model->category_home();
        $data['product_sales'] = $this->item_model->item_sale_latest(5);
        $header['title'] = title();
        $this->load->view('frontend/header.php',$header);
        $this->load->view('frontend/home.php', $data);
        $this->load->view('frontend/footer.php');
    }

    /**
     * Frontend::home().
     *
     * @return
     */
    public function search()
    {

        $this->enqueue->enqueue_css('template/frontend/assets/css/bootstrap.min.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/chosen/chosen.min.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-outline/pictopro-outline.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-normal/pictopro-normal.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/colorbox/colorbox.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/jslider/bin/jquery.slider.min.css');
        $this->enqueue->enqueue_css('template/frontend/assets/css/carat.css');
        $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');
        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');
        $this->enqueue->enqueue_css('template/backend/plugins/font-awesome/css/font-awesome.min.css');

        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.js');
        $this->enqueue->enqueue_js('http://code.jquery.com/jquery-migrate-1.2.1.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.ui.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/jquery.dataTables.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/dataTables.bootstrap.min.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/bootstrap.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
        $this->enqueue->enqueue_js('template/frontend/modules/home/cart.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/carat.js');
        $this->enqueue->enqueue_js('template/frontend/modules/home/home.js');

        $data['categories'] = $this->category_model->category_home();
        $data['product_sales'] = $this->item_model->item_sale_latest(5);
        $header['title'] = title();
        $search = xss_clean($this->uri->segment(2));

        if(!empty($search)) {
            $data['search'] = $search;
        }

        $this->load->view('frontend/header.php',$header);
        $this->load->view('frontend/home.php', $data);
        $this->load->view('frontend/footer.php');
    }

    public function redirect() {
        $full_uri = $this->uri->uri_string();

        // Ha már rögzítve van a rendszerben
        $this->db->where('redirect_from',$full_uri);
        $query = $this->db->get('redirect');

        if($query->num_rows() == 1) {

            $redirect_to = $query->result()[0]->redirect_to;
            $redirect_type = $query->result()[0]->redirect_type;
            $redirect_to = ($redirect_to == 'home' ? '/' : $redirect_to);

            redirect($redirect_to,'location',$redirect_type);

        } else {
            // Ha nincs megkeressük a 3.-ik paramétert
            if($this->uri->total_segments() == 4) {
                $part_uri = $this->uri->segment(4);
            } else if($this->uri->total_segments() == 3) {
                $part_uri = $this->uri->segment(3);
            }
            else if($this->uri->total_segments() == 2) {
               $part_uri = $this->uri->segment(2);
           }

            if(isset($part_uri)) {
                $query = $this->db->query("
                            SELECT router_method, router_controller FROM router WHERE router_slug = '".$part_uri."'
                            UNION ALL
                            SELECT 'item_list', 'frontend_item' FROM item WHERE item_slug = '".$part_uri."'
                            UNION ALL
                            SELECT 'page_list', 'frontend_cms' FROM cms WHERE cms_slug = '".$part_uri."'
                            UNION ALL
                            SELECT 'category_list', 'frontend_category' FROM category_import WHERE
                            slug = '".$part_uri."' OR
                            slug2 = '".$part_uri."' OR
                            slug3 = '".$part_uri."' OR
                            slug4 = '".$part_uri."' OR
                            slug5 = '".$part_uri."' OR
                            slug6 = '".$part_uri."' ");

                if($query->num_rows() == 1) {
                    $redirect['redirect_to'] = $part_uri;
                } else {
                    $redirect['redirect_to'] = 'home';
                }
            } else {
                $redirect['redirect_to'] = 'home';
            }

            $redirect['redirect_from'] = $full_uri;
            $redirect['redirect_type'] = 301;

            $this->db->insert('redirect',$redirect);

            redirect($part_uri,'location',301);
        }
        show404();
    }

    public function item_search() {
        if ($this->input->post()) {
            $search = $this->input->post('search');

            $this->db->like('item_sku', $search);
            $this->db->or_like('item_name', $search);
            $this->db->limit(10);
            $query = $this->db->get('item');

            if ($query->num_rows() > 0) {
                $search_result = $query->result();

                foreach($search_result as $search_res) {
                    $result[] = ['value'=>$search_res->item_slug,'label'=>$search_res->item_name];
                }
            } else {
                $result = [];
            }
            $data['search_result'] = $result;
        }
        $data['csrf'] = csrf();
        echo json_encode($data);
    }
    /**
     * Datatable Ajax.
     *
     * @return json
     */
    public function home_datatable()
    {
        if ($this->input->post()) {
            $columns[0] = 'item_id';
            $columns[1] = 'item_name';
            $columns[2] = 'item_netprice';

            $limit = $this->input->post('length');
            $offset = $this->input->post('start');
            $search = $this->input->post('kereses') ? $this->input->post('kereses') : $this->input->post('search')['value'];
            $order = $this->input->post('order');
            $category = $this->input->post('category') ? $this->input->post('category') : '';


            $orderby = $order[0]['dir'];
            $order = $columns[$order[0]['column']];

            $this->load->model('item_model', 'item');

            $items_fil_cnt = $this->item->item_fil_cnt($order, $orderby, $columns, $search,
                $category);
            $items_all_cnt = $this->item->item_all_cnt($category);
            $items_all = $this->item->item_datatable($limit, $offset, $order, $orderby, $columns,
                $search, $category);

            $items['draw'] = $this->input->post('draw');
            $items['recordsTotal'] = $items_all_cnt;
            $items['recordsFiltered'] = $items_fil_cnt;

            $items['data'] = [];
            $items['csrf'] = csrf();

            if ($items_all) {
                foreach ($items_all as $item_all) {
                    if ($item_all->brutto != 0) {
                        $buttons = '
                        <div class="row"><div class="col-md-6"><input type="number" value="1" class="form-control" data-datatable-cart-qty=""></div>
                        <div class="col-md-6"><button data-add-to-cart="'.$item_all->item_id.
                            '" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-shopping-cart"></span> Kosárba</button></div></div>';
                    } else {
                        $buttons = 'Nem rendelhető';
                    }
                    if (!$item_all->item_image) {
                        $image = 'template/backend/dist/img/no-image-50.jpg';
                        $row[0] = '<img style="height: 50px; width: 50px;" src="'.base_url($image).'">';
                    } else {
                        $image = $item_all->item_image;
                        $row[0] = '<a data-lightbox="image-'.$item_all->item_id.'" href="'.base_url($image).'"><img style="height: 50px; width: 50px;" src="'.base_url($image).'"></a>';
                    }

                    $status = $item_all->item_status == 1 ?
                        '<i class="fa fa-check fa-2x has-success green"></i>' :
                        '<i class="fa fa-times fa-2x red"></i>';

                    if($item_all->brutto == 0) {
                        $price = 'Nincs ár';
                    } else {
                        if($item_all->brutto == $item_all->salebrutto) {
                            $price = 'Online ár: <span style="color:red">'.$item_all->brutto.' Ft</span>';
                        }
                        else if($item_all->brutto > 0 && $item_all->salebrutto == 0){
                            $price = 'Bolti ár: <span style="color:red">'.$item_all->brutto.' Ft</span>';
                        } else if($item_all->brutto > 0 && $item_all->salebrutto > 0){
                            $price = '<s>Bolti ár: '.$item_all->brutto.' Ft</s><br/>Online ár: <span style="color:red">'.$item_all->salebrutto.' Ft</span>';
                        }
                    }


                    $row[1] = '<a style="color:black" href="'.base_url($item_all->item_slug).'">'.$item_all->item_name.'</a>';
                    $row[2] = '<span style="font-size:16px">'.$price.'</span>';
                    $row[3] = $buttons;
                    $items['data'][] = $row;
                    unset($row, $buttons, $image, $slug);
                }
            }

            echo json_encode($items);
            exit();
        }
    }
}
