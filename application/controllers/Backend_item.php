<?php

defined('BASEPATH') or exit('No direct script access allowed');

class backend_item extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('item_model', 'item');
    }

    /**
     * Item list.
     *
     * @return mixed
     */
    public function item_list()
    {
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/jquery.dataTables.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/dataTables.bootstrap.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/slimScroll/jquery.slimscroll.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/fastclick/fastclick.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/dropzone-master/dist/dropzone.js');
        $this->enqueue->enqueue_js('template/backend/modules/item/item_list.js');

        $this->enqueue->enqueue_css('template/backend/plugins/dropzone-master/dist/basic.css');
        $this->enqueue->enqueue_css('template/backend/plugins/dropzone-master/dist/dropzone.css');
        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');
        $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');

        $this->load->view('backend/header.php');
        $this->load->view('backend/item/item_list.php');
        $this->load->view('backend/footer.php');
    }

    public function item_add()
    {
        if ($this->input->post()) {
            $model['item_name'] = $this->input->post('item_name');
            $model['item_sku'] = $this->input->post('item_sku');
            $model['item_slug'] = $this->input->post('item_slug');
            $model['item_longdesc'] = $this->input->post('item_longdesc');
            $model['item_status'] = $this->input->post('item_status');
            $model['item_tax'] = $this->input->post('item_tax');
            $model['item_status'] = $model['item_status'] == 'on' ? 1 : 0;
            $model['item_weight'] = $this->input->post('item_weight');
            $model['item_netprice'] = $this->input->post('item_netprice');
            $model['item_netpricesale'] = $this->input->post('item_netpricesale');
            $model['item_saledate'] = $this->input->post('item_saledate');

            if (empty($model['item_name'])) {
                $data['error']['item_name'] = 'A termék nevének megadása kötelező';
            }

            if (empty($model['item_weight'])) {
                $data['error']['item_weight'] = 'A termék súlyának megadása kötelező';
            }

            if (empty($model['item_slug'])) {
                $data['error']['item_slug'] = 'A termék url megadása kötelező';
            }

            if (empty($model['item_sku'])) {
                $data['error']['item_sku'] = 'A termék azonosító megadása kötelező';
            }

            if (empty($model['item_netprice'])) {
                $data['error']['item_netprice'] = 'Az ár megadása kötelező';
            }

            /* Router Táblába felvenni */
            if (empty($data['error'])) {
                $model['item_saledate'] = explode('-', $this->input->post('item_saledate'));
                $model['item_salestart'] = str_replace('/', '-', trim($model['item_saledate'][0]));
                $model['item_salestart'] = str_replace(' ', '', $model['item_salestart']);
                $model['item_saleend'] = str_replace('/', '-', trim($model['item_saledate'][1]));
                $model['item_saleend'] = str_replace(' ', '', $model['item_saleend']);
                unset($model['item_saledate']);
                $item_id = $this->item->item_add($model);
                if ($item_id) {
                    redirect(base_url('item-edit/'.$model['item_slug']));
                } else {
                    $data['error']['item'] = 'Nem sikerült hozzáadni a terméket.';
                }
            }
        }

        $this->enqueue->enqueue_css('template/backend/plugins/select2/select2.min.css');

        $this->enqueue->enqueue_js('https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js');
        $this->enqueue->enqueue_js('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/daterangepicker/daterangepicker.js');
        $this->enqueue->enqueue_js('template/backend/plugins/select2/select2.full.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/jquery.dataTables.min.js');
        $this->enqueue->enqueue_js('template/backend/modules/item/item_add.js');

        $data['product'] = $this->input->post();
        $data['url'] = base_url('item-add');
        $this->load->view('backend/header.php');
        $this->load->view('backend/item/item_add.php', $data);
        $this->load->view('backend/footer.php');
    }

    public function item_delete()
    {
        if ($this->input->post()) {
            $item_id = (int) $this->input->post('item_id');
            if ($item_id) {
                $res = $this->item_model->item_delete($item_id);
                if ($res) {
                    $msg['success'] = 'A termék törlése került!';
                } else {
                    $msg['error'] = 'A termék nem sikerült törölni!';
                }

                $dirname = FCPATH.'upload/item/'.$item_id;
                @array_map('unlink', glob("$dirname/*.*"));
                @rmdir($dirname);
            }
        } else {
            $msg['error'] = 'Nem sikerült a terméket törölni!';
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }

    public function item_edit()
    {
        $this->load->model('item_model');
        $slug = $this->uri->segment(2);
        if ($this->input->post()) {
            $item_id = $this->input->post('item_id');
            $model['item_name'] = $this->input->post('item_name');
            $model['item_longdesc'] = $this->input->post('item_longdesc');
            $model['item_status'] = $this->input->post('item_status');
            $model['item_sku'] = $this->input->post('item_sku');
            $model['item_tax'] = $this->input->post('item_tax');
            if ($model['item_status'] == 'on') {
                $model['item_status'] = 1;
            } else {
                $model['item_status'] = 0;
            }

            $model['item_weight'] = $this->input->post('item_weight');
            $model['item_netprice'] = $this->input->post('item_netprice');
            $model['item_netpricesale'] = $this->input->post('item_netpricesale');
            $model['item_saledate'] = $this->input->post('item_saledate');


            if(!empty($model['item_saledate']))
            {
                $model['item_saledate'] = explode('-', $this->input->post('item_saledate'));
                $model['item_salestart'] = str_replace('/', '-', trim($model['item_saledate'][0]));
                $model['item_salestart'] = str_replace(' ', '', $model['item_salestart']);
                $model['item_saleend'] = str_replace('/', '-', trim($model['item_saledate'][1]));
                $model['item_saleend'] = str_replace(' ', '', $model['item_saleend']);
            }
            else {
                $model['item_saleend'] = $model['item_salestart'] = NULL;
            }

            unset($model['item_saledate']);

            /*$this->item_model->item_category_update($item_id, $this->input->post('item_category'));*/
            $model['item_slug'] = $this->input->post('item_slug');
            $this->item_model->item_update($item_id, $model);
            $slug = $model['item_slug'];
        }

        $header['css'][] = 'template/backend/plugins/select2/select2.min.css';
        $header['css'][] = 'template/backend/plugins/dropzone-master/dist/basic.css';
        $header['css'][] = 'template/backend/plugins/dropzone-master/dist/dropzone.css';

        $header['js'][] = 'https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js';
        $header['js'][] = 'template/backend/plugins/dropzone-master/dist/dropzone.js';
        $header['js'][] = 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js';
        $header['js'][] = 'template/backend/plugins/daterangepicker/daterangepicker.js';
        $header['js'][] = 'template/backend/plugins/select2/select2.full.min.js';
        $header['js'][] = 'template/backend/modules/item/item_edit.js';

        $data['products'] = $this->item_model->item_by_slug($slug);
        $data['images'] = $this->item_model->item_image_by_id($data['products']->item_id);
        $data['url'] = base_url('item-edit/'.$data['products']->item_slug.'');
        $this->load->view('backend/header.php', $header);
        $this->load->view('backend/item/item_edit.php', $data);
        $this->load->view('backend/footer.php');
    }

    public function item_datatable()
    {
        if ($this->input->post()) {
            $columns[0] = 'item_image';
            $columns[1] = 'item_id';
            $columns[2] = 'item_name';
            $columns[3] = 'item_sku';
            $columns[4] = 'item_slug';
            $columns[5] = 'item_netprice';
            $columns[6] = 'item_netpricesale';
            $columns[7] = 'item_status';

            $limit = $this->input->post('length');
            $offset = $this->input->post('start');
            $search = $this->input->post('search') ['value'];
            $order = $this->input->post('order');
            $orderby = $order[0]['dir'];
            $order = $columns[$order[0]['column']];

            $this->load->model('item_model', 'item');

            $items_fil_cnt = $this->item->item_fil_cnt($order, $orderby, $columns, $search);
            $items_all_cnt = $this->item->item_all_cnt();
            $items_all = $this->item->item_datatable($limit, $offset, $order, $orderby, $columns, $search);

            $items['draw'] = $this->input->post('draw');
            $items['recordsTotal'] = $items_all_cnt;
            $items['recordsFiltered'] = $items_fil_cnt;

            $items['data'] = [];
            $items['csrf'] = csrf();

            if ($items_all) {
                foreach ($items_all as $item_all) {
                    $buttons = '<a href="'.base_url('item-edit/'.$item_all->item_slug).'" class="btn btn-info" role="button"><i class="fa fa-pencil"></i></a>
    <button class="btn btn-danger" role="button" data-item-modal-del="'.$item_all->item_id.'"><i class="fa fa-times"></i></button>';

                    $image = ($item_all->item_image) ? $item_all->item_image : 'template/backend/dist/img/no-image-50.jpg';
                    $slug = '<a class="dotted" href="'.base_url($item_all->item_slug).'">'.$item_all->item_slug.'</a>';
                    $status = $item_all->item_status == 1 ? '<i class="fa fa-check fa-2x has-success green"></i>' : '<i class="fa fa-times fa-2x red"></i>';

                    $price = $item_all->item_netprice.' Ft<br/>'.$item_all->brutto.' Ft';

                    $saleprice = $item_all->item_netpricesale ? $item_all->item_netpricesale.' Ft<br/>'.$item_all->salebrutto.' Ft' : 'Nincs akció';

                    $row[0] = '<img style="height: 50px; widht: 50px;" src="'.base_url($image).'">';
                    $row[1] = $item_all->item_id;
                    $row[2] = $item_all->item_name;
                    $row[3] = $item_all->item_sku;
                    $row[4] = $slug;
                    $row[5] = $price;
                    $row[6] = $saleprice;
                    $row[7] = $status;
                    $row[8] = $buttons;
                    $items['data'][] = $row;
                    unset($row, $buttons, $image, $slug);
                }
            }

            echo json_encode($items);
            exit();
        }
    }

    /*
    public function datatable_product() {
    $html = "";
    $this->load->model('item_model');
    $products = $this->item_model->item_all();
    if (!empty($products)) {
    foreach ($products as $product) {
    $html .= "<tr>";
    $html .= '<td class="img" style="width:20px;"><img style="height: 50px; widht: 50px;" src="';
    if (!empty($product->image_url)) {
    $html .= base_url($product->image_url);
    } else {
    $html .= base_url('template/backend/dist/img/no-image-50.jpg');
    }

    $html .= '"></td>';
    $html .= '<td style="width:20px;">' . $product->item_id . '</td>';
    $html .= '<td style="width:600px;"><b>' . $product->item_name . '</b></td>';
    $html .= '<td>' . $product->item_sku . '</td>';
    $html .= '<td></td>';
    $html .= '<td>' . $product->item_netprice . ' Ft<br/>' . round($product->item_netprice * (($product->item_tax / 100) + 1)) . ' Ft</td>';
    $html .= '<td>' . ($product->item_netpricesale ? $product->item_netpricesale . " Ft" : "") . '<br/>' . ($product->item_netpricesale ? round($product->item_netpricesale * (($product->item_tax / 100) + 1)) . " Ft" : "Nincs akció") . '</td>';
    $html .= '<td style="width:30px;">' . ($product->item_status == 1 ? '<i class="fa fa-check fa-2x has-success green"></i>' : '<i class="fa fa-times fa-2x red"></i>') . '</td>';
    $html .= '<td style="width:80px;"><a href="' . base_url('item-edit/' . $product->item_slug) . '" class="btn btn-info" role="button"><i class="fa fa-pencil"></i></a>
    <button class="btn btn-danger" role="button" data-item-modal-del="' . $product->item_id . '"><i class="fa fa-times"></i></button></td>';
    $html .= "</tr>";
    }
    }

    return $html;
    }*/
    /**
     * @param $item_id
     *
     * @return mixed
     */
    public function item_categorylist($item_id)
    {
        $this->load->model('item_model');
        $categories = $this->item_model->item_category_list($item_id);
        $categorylist = [];
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $categorylist[] = $category['category_id'];
            }
        }

        return $categorylist;
    }

    public function item_imageupload()
    {
        if ($this->input->post()) {
            $item_id = xss_clean($this->input->post('item_id'));
            if ($item_id) {
                $target_path = FCPATH.'upload/item/'.$item_id;
                $this->load->model('item_model');
                $image_storeid = $this->item_model->item_image_count($item_id);
                $target_file = $target_path.'/'.$image_storeid.'.jpg';
                if ($_FILES['file']['tmp_name']) {
                    if ($_FILES['file']['size'] > 500000) {
                        return json_encode(array(
                            'type' => 'error',
                            'message' => 'Túl nagy kép!',
                        ));
                    }

                    if (!file_exists($target_path)) {
                        mkdir($target_path, 0777, true);
                    }

                    include APPPATH.'libraries/SimpleImage.php';

                    $img = new abeautifulsite\SimpleImage($_FILES['file']['tmp_name']);
                    $img->thumbnail(500, 500);
                    $uploaded = $img->save($target_file);
                    if ($uploaded) {
                        $file['image_url'] = 'upload/item/'.$item_id.'/'.$image_storeid.'.jpg';
                        $file['image_featured'] = ($image_storeid == 1) ? 1 : 0;
                        $file['image_storeid'] = $image_storeid;
                        $this->db->insert('image', $file);
                        $insert_id = $this->db->insert_id();
                        $file_business['item_id'] = $item_id;
                        $file_business['image_id'] = $this->db->insert_id();
                        $this->db->insert('item_image', $file_business);
                        echo json_encode(array(
                            'type' => 'success',
                            'message' => 'Sikeres feltöltés',
                            'imgurl' => base_url($file['image_url']),
                            'file_store_id' => $file['image_storeid'],
                            'file_id' => $insert_id,
                            'csrf' => csrf(),
                        ));
                    }
                }
            }
        }
    }

    public function item_imagedefault()
    {
        if ($this->input->post()) {
            $item_id = xss_clean($this->input->post('item_id'));
            $image_id = xss_clean($this->input->post('image_id'));
            $this->load->model('item_model');
            $this->item_model->item_image_default($item_id, $image_id);
        }

        echo json_encode(['csrf' => csrf()]);
    }

    public function item_imagedelete()
    {
        if ($this->input->post()) {
            $item_id = xss_clean($this->input->post('item_id'));
            $image_id = xss_clean($this->input->post('image_id'));
            $store_id = xss_clean($this->input->post('store_id'));
            $this->db->where('image_id', $image_id);
            $this->db->delete('image');
            $this->db->where('image_id', $image_id);
            $this->db->where('item_id', $item_id);
            $this->db->delete('item_image');
            unlink(FCPATH.'upload/item/'.$item_id.'/'.$store_id.'.jpg');
        }

        echo json_encode(['csrf' => csrf()]);
    }

    /**
     * @param $parent_id
     * @param $level
     *
     * @return mixed
     */
    public function recursiveCategory($parent_id = 0, $level = 0)
    {
        $this->load->model('item_model');
        $parents = $this->item_model->item_category($parent_id);
        $cat = array();
        if ($parents) {
            ++$level;
            foreach ($parents as $parent => $value) {
                $cat[$value->category_id]['category_name'] = $value->category_name;
                $cat[$value->category_id]['category_id'] = $value->category_id;
                $cat[$value->category_id]['category_slug'] = $value->category_slug;
                $cat[$value->category_id]['level'] = $level;
                $cat[$value->category_id]['category_parentid'] = $value->category_parentid;
                $subcat = $this->recursiveCategory($value->category_id, $level);
                if (!empty($subcat)) {
                    $cat[$value->category_id]['children'] = $subcat;
                }
            }

            --$level;
        }

        return $cat;
    }

    /**
     * @param $item_id
     * @param $item
     * @param $i
     *
     * @return mixed
     */
    public function recurselist($item_id, $item, $i = 0)
    {
        $html = '';
        foreach ($item as $key => $value) {
            if ($item_id != 0 && !empty($this->item_categorylist($item_id))) {
                if (in_array($value['category_id'], $this->item_categorylist($item_id))) {
                    $html .= "<option value='".$value['category_id']."' selected>".$value['category_name'].'</option>';
                } else {
                    $html .= "<option value='".$value['category_id']."'>".$value['category_name'].'</option>';
                }
            } else {
                $html .= "<option value='".$value['category_id']."'>".$value['category_name'].'</option>';
            }

            if (isset($value['children'])) {
                $html .= $this->recurselist($item_id, $value['children'], $i);
            }
        }

        return $html;
    }

    /*exit();
    require_once FCPATH . 'vendor/autoload.php';
    $tmp = 0;
    $inputFileName = FCPATH.'upload/import/products.csv';
    $csv = new parseCSV($inputFileName);
    $i=0;
    foreach($csv->data as $import) {
    $model['item_name'] = $import['product_name'];
    $model['item_longdesc'] = $import['description'];
    $model['item_status'] = 1;
    $model['item_weight'] = $import['weight'];
    $model['item_netprice'] = $import['price'];
    $model['item_sku'] = $import['sku'];
    $model['item_slug'] = create_slug($import['product_name']);
    $model['item_tax'] = 27;
    $this->db->insert('item',$model);
    }

    exit();*/
    public function item_category()
    {
        require_once FCPATH.'vendor/autoload.php';

        $inputFileName = FCPATH.'upload/import/categories.csv';
        $csv = new parseCSV($inputFileName);

        // Összes termék id

        $this->db->select('item_id, item_sku');
        $query = $this->db->get('item');
        $allitems = $query->result();
        $items = [];
        foreach ($allitems as $allitem) {
            $items[$allitem->item_id] = strtoupper($allitem->item_sku);
        }

        $this->load->model('item_model');
        $i = 0;
        $j = 0;
        foreach ($csv->data as $import) {
            $item_id = $this->item_model->item_id_by_sku($import['sku']);
            if ($item_id) {
                echo 'van '.$item_id;
            }

            if ($i++ == 1000) {
                ++$j;
                $i = 0;
                echo(1000 * $j).'<br/>';
            }
        }
    }

    public function item_modal()
    {
        $post = $this->input->post();
        $data = [];
        if (isset($post['id']) && !empty($post['id'])) {
            $data = $this->item_model->item_get((int) $post['id']);
        }

        $html = $this->load->view('backend/item/item_'.$post['type'].'.php', $data, true);
        $msg['csrf'] = csrf();
        $msg['data'] = $html;
        echo json_encode($msg);
    }

    public function item_import()
    {
        require_once FCPATH.'vendor/autoload.php';
        $inputFileName = FCPATH.'upload/import/categories.csv';
        $csv = new parseCSV($inputFileName);
        $this->load->model('item_model');
        $this->load->model('category_model');

        // Összes kategória id, slug
        $this->db->select('category_id, category_slug');
        $query = $this->db->get('category');

        $allcategories = $query->result();
        $allcat = [];
        foreach ($allcategories as $allcategory) {
            $allcat[$allcategory->category_id] = $allcategory->category_slug;
        }

        $i = 0;
        $j = 0;

        foreach ($csv->data as $import) {
            $item_id = $this->item_model->item_id_by_sku($import['sku']);

            $category['category_slug'] = create_slug($import['kategoria']);
            $category['category_fullname'] = $import['kategoria'];

            $parentid = array_search($category['category_slug'], $allcat);

            if (!$parentid) {
                $category['category_name'] = $import['kategoria'];
                $category['category_parentid'] = 0;
                $this->db->insert('category', $category);
                $parentid = $this->db->insert_id();
                $allcat[$parentid] = $category['category_slug'];
            }

            if ($item_id != null) {
                $cat['item_id'] = $item_id;
                $cat['category_id'] = $parentid;
                @$this->db->insert('item_category', $cat);
            }

            $parentid = array_search($category['category_slug'], $allcat);
            $category['category_slug'] = $category['category_slug'].'-'.create_slug($import['alkategoria']);
            $category['category_fullname'] = $category['category_fullname'].' '.$import['alkategoria'];
            $catid = array_search($category['category_slug'], $allcat);

            if (!$catid) {
                $category['category_name'] = $import['alkategoria'];
                $category['category_parentid'] = $parentid;
                $this->db->insert('category', $category);
                $parentid = $this->db->insert_id();
                $allcat[$parentid] = $category['category_slug'];
            }

            if ($item_id != null) {
                $cat['item_id'] = $item_id;
                $cat['category_id'] = $parentid;
                @$this->db->insert('item_category', $cat);
            }

            $parentid = array_search($category['category_slug'], $allcat);
            $category['category_slug'] = $category['category_slug'].'-'.create_slug($import['gyarto']);
            $category['category_fullname'] = $category['category_fullname'].' '.$import['gyarto'];
            $catid = array_search($category['category_slug'], $allcat);

            if (!$catid) {
                $category['category_name'] = $import['gyarto'];

                $category['category_parentid'] = $parentid;
                $this->db->insert('category', $category);
                $parentid = $this->db->insert_id();
                $allcat[$parentid] = $category['category_slug'];
            }
            if ($item_id != null) {
                $cat['item_id'] = $item_id;
                $cat['category_id'] = $parentid;
                @$this->db->insert('item_category', $cat);
            }

            $parentid = array_search($category['category_slug'], $allcat);
            $category['category_slug'] = $category['category_slug'].'-'.create_slug($import['ccm']).'ccm3';
            $category['category_fullname'] = $category['category_fullname'].' '.$import['ccm'].' ccm³';
            $catid = array_search($category['category_slug'], $allcat);

            if (!$catid) {
                $category['category_name'] = $import['ccm'].' ccm³';
                $category['category_parentid'] = $parentid;
                $this->db->insert('category', $category);
                $parentid = $this->db->insert_id();
                $allcat[$parentid] = $category['category_slug'];
            }
            if ($item_id != null) {
                $cat['item_id'] = $item_id;
                $cat['category_id'] = $parentid;
                @$this->db->insert('item_category', $cat);
            }

            $parentid = array_search($category['category_slug'], $allcat);
            $category['category_slug'] = $category['category_slug'].'-'.create_slug($import['tipus']);
            $category['category_fullname'] = $category['category_fullname'].' '.$import['tipus'];
            $catid = array_search($category['category_slug'], $allcat);

            if (!$catid) {
                $category['category_name'] = $import['tipus'];

                $category['category_parentid'] = $parentid;
                $this->db->insert('category', $category);
                $parentid = $this->db->insert_id();
                $allcat[$parentid] = $category['category_slug'];
            }

            if ($item_id != null) {
                $cat['item_id'] = $item_id;
                $cat['category_id'] = $parentid;
                @$this->db->insert('item_category', $cat);
            }

            $parentid = array_search($category['category_slug'], $allcat);
            $category['category_slug'] = $category['category_slug'].'-'.create_slug($import['evjarat']);
            $category['category_fullname'] = $category['category_fullname'].' '.$import['evjarat'];
            $catid = array_search($category['category_slug'], $allcat);

            if (!$catid) {
                $category['category_name'] = $import['evjarat'];
                $category['category_parentid'] = $parentid;
                $this->db->insert('category', $category);
                $parentid = $this->db->insert_id();
                $allcat[$parentid] = $category['category_slug'];
            }

            if ($item_id != null) {
                $cat['item_id'] = $item_id;
                $cat['category_id'] = $parentid;
                @$this->db->insert('item_category', $cat);
            }

            unset($category, $parentid, $item_id);
            unset($item_id);
        }
    }

    public function item_import_upload()
    {
        $target_file = FCPATH.'upload/import/products.csv';

        if ($_FILES['file']['tmp_name']) {
            if (move_uploaded_file($_FILES['file']['tmp_name'], $target_file)) {
                echo json_encode(array(
                    'type' => 'success',
                    'message' => 'Sikeres feltöltés',
                    'csrf' => csrf(),
                ));
            } else {
                return json_encode(array(
                    'type' => 'error',
                    'message' => 'A feltöltés nem sikerült!',
                ));
            }
        }
    }

    public function item_product_import()
    {
        require_once FCPATH.'vendor/autoload.php';
        $tmp = 0;
        $inputFileName = FCPATH.'upload/import/product_list.csv';
        $csv = new parseCSV($inputFileName, 1000, 11);
        $i = 0;
        pre($csv->data);
        exit();
        foreach ($csv->data as $import) {
            $model['item_name'] = $import['name'];
            $model['item_longdesc'] = $import['desc'];
            $model['item_status'] = 1;
            $model['item_weight'] = $import['weight'];
            $model['item_netprice'] = $import['price'];
            $model['item_sku'] = $import['sku'];
            $model['item_slug'] = create_slug($import['name']);
            $model['item_tax'] = 27;
            $this->db->insert('item', $model);
        }
    }
}
