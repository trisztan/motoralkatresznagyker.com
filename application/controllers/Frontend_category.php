<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Frontend_category extends CI_Controller
{
    private $slug;

    public function __construct()
    {
        parent::__construct();
        $this->slug = $this->uri->segment(1);
        $this->load->model('category_model');
        $this->load->model('item_model');
        //$this->session->unset_userdata('cart');
    }
    /**
     * Category list.
     *
     * @return string Show category page
     */
    public function category_list()
    {
        $this->enqueue->enqueue_css('template/frontend/assets/css/bootstrap.min.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-outline/pictopro-outline.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-normal/pictopro-normal.css');
        $this->enqueue->enqueue_css('template/frontend/assets/css/carat.css');
        $this->enqueue->enqueue_css('template/backend/plugins/font-awesome/css/font-awesome.min.css');
        $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');
        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');

        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/jquery.dataTables.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/dataTables.bootstrap.min.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/bootstrap.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/carat.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
        $this->enqueue->enqueue_js('template/frontend/modules/home/cart.js');
        $this->enqueue->enqueue_js('template/frontend/modules/category/category.js');

        $data['categories_main'] = $this->category_model->category_home();
        $data['categories'] = $this->category_model->category_search($this->slug);
        $data['product_sales'] = $this->item_model->item_sale_latest(5);

        $header['title'] = title(category_title());
        $this->load->view('frontend/header.php',$header);
        $this->load->view('frontend/category/category_list.php', $data);
        $this->load->view('frontend/footer.php');
    }
}
