<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Backend_category extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('category_model', 'category');
    }
    /**
     * Category list.
     *
     * @return string Show category list page on admin
     */
    public function category_list()
    {
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/jquery.dataTables.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/dataTables.bootstrap.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/slimScroll/jquery.slimscroll.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/fastclick/fastclick.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/dropzone-master/dist/dropzone.js');
        $this->enqueue->enqueue_js('template/backend/modules/category/category_list.js');

        $this->enqueue->enqueue_css('template/backend/plugins/dropzone-master/dist/basic.css');
        $this->enqueue->enqueue_css('template/backend/plugins/dropzone-master/dist/dropzone.css');
        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');
        $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');

        $this->load->view('backend/header.php');
        $this->load->view('backend/category/category_list.php');
        $this->load->view('backend/footer.php');
    }
    public function category_image()
    {
        $post = $this->input->post();

        $target_path = FCPATH.'upload/category';
        $target_file = $target_path.'/'.$post['image_name'].'.jpg';

        if ($_FILES['file']['tmp_name']) {
            if ($_FILES['file']['size'] > 500000) {
                return json_encode(array(
                    'type' => 'error',
                    'message' => 'Túl nagy kép!',
                ));
            }

            include APPPATH.'libraries/SimpleImage.php';

            $img = new abeautifulsite\SimpleImage($_FILES['file']['tmp_name']);
            $img->maxareafill(240,240, 255, 255, 255);

            $img->save($target_file);
        }
        redirect(base_url('category-list'));
    }
    /**
     * Load template to modal.
     *
     * @return string
     */
    public function category_modal()
    {
        $post = $this->input->post();
        $data = [];
        if (isset($post['id']) && !empty($post['id'])) {
            $data = $this->category->category_get((int) $post['id']);
        }
        if($post['type'] == 'image'){
            unset($data);
            $data['id'] = $post['id'];
        }

        $html = $this->load->view('backend/category/category_'.$post['type'].'.php', $data, true);
        $msg['csrf'] = csrf();
        $msg['data'] = $html;
        echo json_encode($msg);
    }

    public function category_datatable()
    {
        if ($this->input->post()) {
            // Datatable columns
            $columns[0] = 'kategoria';
            $columns[1] = 'alkategoria';
            $columns[2] = 'gyarto';
            $columns[3] = 'ccm';
            $columns[4] = 'tipus';
            $columns[5] = 'evjarat';
            $columns[6] = 'sku';
            $columns[7] = 'slug6';
            // Datatable variables
            $limit = $this->input->post('length');
            $offset = $this->input->post('start');
            $search = $this->input->post('search') ['value'];
            $order = $this->input->post('order');
            $orderby = $order[0]['dir'];
            $order = $columns[$order[0]['column']];

            //
            $categories_fil_cnt = $this->category->category_fil_cnt($order, $orderby, $columns, $search);
            $categories_all_cnt = $this->category->category_all_cnt();
            $categories_all = $this->category->category_datatable($limit, $offset, $order, $orderby, $columns, $search);

            // Prepare datatable json data
            $categories['draw'] = $this->input->post('draw');
            $categories['recordsTotal'] = $categories_all_cnt;
            $categories['recordsFiltered'] = $categories_fil_cnt;
            $categories['data'] = [];
            $categories['csrf'] = csrf();

            if ($categories_all) {
                foreach ($categories_all as $category) {
                    $buttons = '<td style="width:100px;"><button class="btn btn-info" role="button" data-category-edit="'.$category->category_id.'"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger" role="button" data-category-delete="'.$category->category_id.'"><i class="fa fa-times"></i></button></td>';
                    if(file_exists(FCPATH.'upload/category/'.create_slug($category->kategoria).'.jpg'))
                    {
                        $img1 = '<img style="height: 50px; width: 50px;" src="'.base_url('upload/category/'.create_slug($category->kategoria).'.jpg').'">';
                    }
                    else {
                        $img1 = '<img src="'.base_url('template/backend/dist/img/no-image-50.jpg').'">';
                    }

                    if(file_exists(FCPATH.'upload/category/'.create_slug($category->alkategoria).'.jpg'))
                    {
                        $img2 = '<img style="height: 50px; width: 50px;" src="'.base_url('upload/category/'.create_slug($category->alkategoria).'.jpg').'">';
                    }
                    else {
                        $img2 = '<img src="'.base_url('template/backend/dist/img/no-image-50.jpg').'">';
                    }

                    if(file_exists(FCPATH.'upload/category/'.create_slug($category->gyarto).'.jpg'))
                    {
                        $img3 = '<img style="height: 50px; width: 50px;" src="'.base_url('upload/category/'.$category->gyarto.'.jpg').'">';
                    }
                    else {
                        $img3 = '<img src="'.base_url('template/backend/dist/img/no-image-50.jpg').'">';
                    }

                    if(file_exists(FCPATH.'upload/category/'.create_slug($category->tipus).'.jpg'))
                    {
                        $img5 = '<img style="height: 50px; width: 50px;" src="'.base_url('upload/category/'.$category->tipus.'.jpg').'">';
                    }
                    else {
                        $img5 = '<img src="'.base_url('template/backend/dist/img/no-image-50.jpg').'">';
                    }

                    $row[0] = $img1.$category->kategoria.' <br/><button class="btn btn-success btn-sm" role="button" data-category-image="'.create_slug($category->kategoria).'"><i class="fa fa-plus"></i> Kép feltöltés</button>';

                    $row[1] = $img2.$category->alkategoria.' <br/><button class="btn btn-success btn-sm" role="button" data-category-image="'.create_slug($category->alkategoria).'"><i class="fa fa-plus"></i> Kép feltöltés</button>';
                    $row[2] = $img3.$category->gyarto.' <br/><button class="btn btn-success btn-sm" role="button" data-category-image="'.create_slug($category->gyarto).'"><i class="fa fa-plus"></i> Kép feltöltés</button>';
                    $row[3] = $category->ccm;
                    $row[4] = $img5.$category->tipus.' <br/><button class="btn btn-success btn-sm" role="button" data-category-image="'.create_slug($category->tipus).'"><i class="fa fa-plus"></i> Kép feltöltés</button>';
                    $row[5] = $category->evjarat;
                    $row[6] = $category->sku;
                    $row[7] = $category->slug6;
                    $row[8] = $buttons;
                    $categories['data'][] = $row;
                    unset($row, $buttons, $image, $slug);
                }
            }

            echo json_encode($categories);
            exit();
        }
    }
    /**
     * Add category.
     *
     * @return string
     */
    public function category_add()
    {
        $post = $this->input->post();
        if ($post) {
            foreach ($post as $postname => $postvalue) {
                if (!empty($postvalue)) {
                    $model[$postname] = $postvalue;
                }
            }

            if(empty($model['kategoria'])) {
                $msg['error']['kategoria'] = 'A mező kitöltése kötelező!';
            }

            /*if(empty($model['sku'])) {
                $msg['error']['sku'] = 'Az SKU megadása kötelező!';
            }*/

            if (empty($msg)) {

                $model['slug'] = create_slug($model['kategoria']);

                if(!empty($model['kategoria'])  && !empty($model['alkategoria'])) {
                    $model['slug2'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria']);
                }
                else {
                    $model['slug2'] = "";
                }

                if(!empty($model['kategoria'])  && !empty($model['alkategoria'])  && !empty($model['gyarto'])) {
                    $model['slug3'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria'].'-'.
                                                  $model['gyarto']);
                }
                else {
                    $model['slug3'] = "";
                }

                if(!empty($model['kategoria'])  && !empty($model['alkategoria'])  && !empty($model['gyarto']) && !empty($model['ccm'])) {
                    $model['slug4'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria'].'-'.
                                                  $model['gyarto'].'-'.
                                                  $model['ccm'].'ccm');
                }
                else {
                    $model['slug3'] = "";
                }

                if(!empty($model['kategoria'])  && !empty($model['alkategoria'])  && !empty($model['gyarto']) && !empty($model['ccm']) && !empty($model['tipus'])) {
                    $model['slug5'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria'].'-'.
                                                  $model['gyarto'].'-'.
                                                  $model['ccm'].'ccm-'.
                                                  $model['tipus']);
                }
                else {
                    $model['slug5'] = "";
                }

                if(!empty($model['kategoria'])  && !empty($model['alkategoria'])  && !empty($model['gyarto']) && !empty($model['ccm']) && !empty($model['tipus']) && !empty($model['evjarat'])) {
                    $model['slug6'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria'].'-'.
                                                  $model['gyarto'].'-'.
                                                  $model['ccm'].'ccm-'.
                                                  $model['tipus'].'-'.
                                                  $model['evjarat']);
                }
                else {
                    $model['slug6'] = "";
                }

                $category = $this->category->category_add($model);
                if ($category) {
                    $msg['toastr']['success'] = 'A kategória sikeresen hozzáadásra került!';
                } else {
                    $msg['toastr']['error'] = 'A kategóriát nem sikerült hozzáadni!';
                }
            } else {
                $msg['toastr']['error'] = 'A kategóriát nem sikerült hozzáadni!';
            }
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }

    public function category_edit()
    {
        $post = $this->input->post();
        if ($post) {
            foreach ($post as $postname => $postvalue) {
                if (!empty($postvalue)) {
                    $model[$postname] = $postvalue;
                }
            }
            $category_id = $model['category_id'];
            unset($model['category_id']);

            if(empty($model['kategoria'])) {
                $msg['error']['kategoria'] = 'A mező kitöltése kötelező!';
            }

            /*if(empty($model['sku'])) {
                $msg['error']['sku'] = 'Az SKU megadása kötelező!';
            }*/

            $model['slug'] = create_slug($model['kategoria']);

                if(!empty($model['kategoria'])  && !empty($model['alkategoria'])) {
                    $model['slug2'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria']);
                }
                else {
                    $model['slug2'] = "";
                }

                if(!empty($model['kategoria'])  && !empty($model['alkategoria'])  && !empty($model['gyarto'])) {
                    $model['slug3'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria'].'-'.
                                                  $model['gyarto']);
                }
                else {
                    $model['slug3'] = "";
                }

                if(!empty($model['kategoria'])  && !empty($model['alkategoria'])  && !empty($model['gyarto']) && !empty($model['ccm'])) {
                    $model['slug4'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria'].'-'.
                                                  $model['gyarto'].'-'.
                                                  $model['ccm'].'ccm');
                }
                else {
                    $model['slug3'] = "";
                }

                if(!empty($model['kategoria'])  && !empty($model['alkategoria'])  && !empty($model['gyarto']) && !empty($model['ccm']) && !empty($model['tipus'])) {
                    $model['slug5'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria'].'-'.
                                                  $model['gyarto'].'-'.
                                                  $model['ccm'].'ccm-'.
                                                  $model['tipus']);
                }
                else {
                    $model['slug5'] = "";
                }

                if(!empty($model['kategoria'])  && !empty($model['alkategoria'])  && !empty($model['gyarto']) && !empty($model['ccm']) && !empty($model['tipus']) && !empty($model['evjarat'])) {
                    $model['slug6'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria'].'-'.
                                                  $model['gyarto'].'-'.
                                                  $model['ccm'].'ccm-'.
                                                  $model['tipus'].'-'.
                                                  $model['evjarat']);
                }
                else {
                    $model['slug6'] = "";
                }

                $category = $this->category->category_update($category_id, $model);
                if ($category) {
                    $msg['toastr']['success'] = 'A kategória sikeresen módosításra került!';
                } else {
                    $msg['toastr']['error'] = 'A kategóriát nem sikerült módosítani!';
                }
            } else {
                $msg['toastr']['error'] = 'A kategóriát nem sikerült módosítani!';
            }


        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }

    public function category_delete()
    {
        $category_id = $this->input->post('category_id');

        if ($category_id) {
            $msg['toastr']['success'] = 'A kupon törlésre került!';
            $coupon = $this->category->category_delete($category_id);
        } else {
            $msg['toastr']['error'] = 'A törlés nem sikerült';
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }
}
