<?php

defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Frontend Cart.
 */
class Frontend_cart extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

    }
    /**
     * Add item to cart.
     *
     * @return string Return json with cart content
     */
    public function cart_add()
    {
        if ($this->input->post()) {
            /* Cart logic */
            $item_id = $this->input->post('item_id');
            $item_qty = $this->input->post('item_qty');
            $cart = $this->session->userdata('cart');
            @$cart[$item_id] = @$cart[$item_id] + $item_qty;
            $this->session->set_userdata('cart', $cart);

            $response['success'] = 'A termék hozzáadva a kosárhoz';
        } else {
            $response['error'] = 'Hiba történt';
        }

        $response['cart'] = cart_content();
        $response['total'] = cart_total();
        $response['csrf'] = csrf();
        echo json_encode($response);
    }

    /**
     * Empty cart content.
     *
     * @return string Return success/failure message with cart content
     */
    public function cart_delete()
    {
        if ($this->input->post()) {
            $item_id = (int) $this->input->post('item_id');

            $cart = $this->session->userdata('cart');
            unset($cart[$item_id]);
            $this->session->set_userdata('cart', $cart);
            $response['cart'] = cart_page();
            $response['success'] = 'A kosár tartalma frissítve';
        }
        else {
            $this->session->unset_userdata('cart');
            $response['cart'] = cart_content();
            $response['success'] = 'A kosár kiürítve';
        }

        $response['total'] = cart_total();
        $response['csrf'] = csrf();

        echo json_encode($response);
    }
    /**
     * Cart page.
     *
     * @return string Return view with cart content
     */
    public function cart_list()
    {
        $this->enqueue->enqueue_css('template/frontend/assets/css/bootstrap.min.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-outline/pictopro-outline.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-normal/pictopro-normal.css');
        $this->enqueue->enqueue_css('template/frontend/assets/css/carat.css');
        $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');
        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');

        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.js');
        $this->enqueue->enqueue_js('http://code.jquery.com/jquery-migrate-1.2.1.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.ui.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/bootstrap.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/carat.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/basket.js');

        $header['title'] = "Kosár";
        $this->load->view('frontend/header.php',$header);
        $this->load->view('frontend/cart/cart_list.php');
        $this->load->view('frontend/footer.php');
    }
    /**
     * Update cart item quantity.
     *
     * @return string return json with cart page content
     */
    public function cart_update()
    {
        if ($this->input->post()) {
            $item_id = (int) $this->input->post('item_id');
            $item_qty = (int) $this->input->post('item_qty');
            $cart = $this->session->userdata('cart');

            if($item_qty == 0){
                unset($cart[$item_id]);
            }
            else {
                @$cart[$item_id] = $item_qty;
            }

            $this->session->set_userdata('cart', $cart);
            $response['success'] = 'A kosár tartalma frissítve';
        } else {
            $response['error'] = 'Hiba történt';
        }

        $response['total'] = cart_total();
        $response['cart'] = cart_page();
        $response['csrf'] = csrf();

        echo json_encode($response);
    }

    public function cart_shippingcalculate() {
        $this->load->model('item_model');
        $cart = $this->session->userdata('cart');

        foreach ($cart as $key => $value) {
            $cart_contents[$key] = $this->item_model->item_by_id($key);
            $cart_contents[$key]['qty'] = $value;
        }
        $total_weight = 0;
        foreach ($cart_contents as $cart_content) {
            $total_weight = $total_weight + ($cart_content['qty'] * $cart_content['item_weight']);
        }

        $query = $this->db->query("SELECT * FROM ship WHERE ".$total_weight." BETWEEN ship_valuefrom AND ship_valueto");
        $results = $query->num_rows() > 0 ? $query->result() : false;

        foreach($results as $result) {
            $ship['ship'][] = ['ship_id'=>$result->ship_id,'ship_name'=>$result->ship_name,'ship_price'=>$result->ship_price];
        }
        $response['ship'] = $ship['ship'];
        $response['weight'] = $total_weight;
        $response['csrf'] = csrf();
        echo json_encode($response);
    }

    /**
     * Order page
     * @return view
     */
    public function cart_order()
    {
        $this->enqueue->enqueue_css('template/frontend/assets/css/bootstrap.min.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-outline/pictopro-outline.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-normal/pictopro-normal.css');
        $this->enqueue->enqueue_css('template/frontend/assets/css/carat.css');
        $this->enqueue->enqueue_css('template/backend/plugins/font-awesome/css/font-awesome.min.css');
        $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');
        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');

        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.js');
        $this->enqueue->enqueue_js('http://code.jquery.com/jquery-migrate-1.2.1.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.ui.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/bootstrap.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/carat.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
        $this->enqueue->enqueue_js('template/frontend/modules/cart/cart_order.js');

        $header['title'] = title('Megrendelés');

        $this->load->view('frontend/header.php',$header);
        $this->load->view('frontend/cart/cart_order.php');
        $this->load->view('frontend/footer.php');

    }

    public function cart_thankyou()
    {
        $this->enqueue->enqueue_css('template/frontend/assets/css/bootstrap.min.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-outline/pictopro-outline.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-normal/pictopro-normal.css');
        $this->enqueue->enqueue_css('template/frontend/assets/css/carat.css');
        $this->enqueue->enqueue_css('template/backend/plugins/font-awesome/css/font-awesome.min.css');
        $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');
        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');

        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.js');
        $this->enqueue->enqueue_js('http://code.jquery.com/jquery-migrate-1.2.1.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.ui.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/bootstrap.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/carat.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
        $this->enqueue->enqueue_js('template/frontend/modules/cart/cart_order.js');


        $data['total_price'] = $this->session->userdata('thankyou');
        $data['title'] = title('Megrendelés');
        $order_id = $this->session->userdata('order_id');
        $data['order_track_url'] = base_url('nyomkovetes/'.$order_id);
        $this->session->unset_userdata('order_id');
        $this->session->unset_userdata('thankyou');
        if($order_id && $data['order_track_url']){
            $this->load->view('frontend/header.php',$data);
            $this->load->view('frontend/cart/cart_thankyou.php',$data);
            $this->load->view('frontend/footer.php');
        } else {
            redirect(base_url());
        }
    }
    public function cart_ordertrack() {
        $order_id = $this->uri->segment(2);
        if($order_id) {
            $query = $this->db->query("SELECT o.*,oi.*,ot.*,os.*,i.item_sku,i.item_name FROM `order` o
                LEFT JOIN order_item oi ON oi.order_id = o.order_id
                LEFT JOIN order_template ot ON o.status_id = ot.ordertemplate_id
                LEFT JOIN item i ON oi.item_sku = i.item_sku
                LEFT JOIN order_ship os ON os.order_id = o.order_id
                WHERE o.order_id = '".$order_id."'");
            $result = $query->num_rows() > 0 ? $query->result() : false;

            if($result) {
                $data['order'] = $result;
                $this->enqueue->enqueue_css('template/frontend/assets/css/bootstrap.min.css');
                $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-outline/pictopro-outline.css');
                $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-normal/pictopro-normal.css');
                $this->enqueue->enqueue_css('template/frontend/assets/css/carat.css');
                $this->enqueue->enqueue_css('template/backend/plugins/font-awesome/css/font-awesome.min.css');
                $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');
                $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');

                $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.js');
                $this->enqueue->enqueue_js('http://code.jquery.com/jquery-migrate-1.2.1.js');
                $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.ui.js');
                $this->enqueue->enqueue_js('template/frontend/assets/js/bootstrap.js');
                $this->enqueue->enqueue_js('template/frontend/assets/js/carat.js');
                $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
                $header['title'] = title('Rendelés nyomonkövetése');
                $this->load->view('frontend/header.php',$header);
                $this->load->view('frontend/cart/cart_ordertrack.php',$data);
                $this->load->view('frontend/footer.php');
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function cart_ordersave() {
        $this->load->model('item_model');

        if ($this->input->post()) {
            $ship = $this->input->post('ship');
            $user_data = $this->input->post('data');

            foreach($user_data as $user) {
                $user_model[$user['name']] = $user['value'];
            }
            $user_model['delivery'] =  $this->input->post('delivery');
            $user_model['billing'] =  $this->input->post('bill');
            $user_model['order_date'] = date('Y-m-d H:i:s');
            $user_model['order_subscribe'] = $this->input->post('order_subscribe');
            $this->db->insert('order',$user_model);
            $order_id = $this->db->insert_id();

            $user_cart = $this->session->userdata('cart');

            foreach($user_cart as $item_id => $qty) {
                $item_data = $this->item_model->item_by_id($item_id);
                $cart_model['item_sku'] = $item_data['item_sku'];
                $cart_model['item_qty'] = $qty;
                $cart_model['item_price'] = price_calculator($item_data['item_netprice'],
                                                            $item_data['item_netpricesale'],
                                                            $item_data['item_tax']);
                $cart_model['order_id'] = $order_id;
                $this->db->insert('order_item',$cart_model);
            }

            $ship = explode('_',$ship);
            $ship_model['ship_name'] = $ship[0];
            $ship_model['ship_price'] = $ship[1];
            $ship_model['order_id'] = $order_id;

            $this->db->insert('order_ship',$ship_model);

            $this->session->unset_userdata('cart');
            $this->session->set_userdata('thankyou', (int)($this->input->post('total_price')+$ship_model['ship_price']));
            $this->session->set_userdata('order_id', $order_id);

            /* E-mail küldése a felhasználónak */
            $this->email->from($this->config->item('baseEmailAddress'), $this->config->item('baseEmailName'));
            $this->email->to($user_model['order_email']);
            $this->email->subject('Megrendelés #'.$order_id);
            $this->email->bcc($this->config->item('baseEmailAddress'));

            $query = $this->db->query('SELECT * FROM order_template WHERE ordertemplate_id = 0');
            $order_template = $query->num_rows() > 0 ? $query->result()[0] : false;

            $order_track_url = base_url('nyomkovetes/'.$order_id);

            $order_items = '
            <table cellspacing="0" cellpadding="10" width="100%">
                <thead>
                    <tr>
                        <th style="font-size:14px;border-bottom:2px solid black" align="left">Termék</th>
                        <th style="font-size:14px;border-bottom:2px solid black" align="left">Mennyiség</th>
                        <th style="font-size:14px;border-bottom:2px solid black" align="left">Web shop ár</th>
                        <th style="font-size:14px;border-bottom:2px solid black" align="left">Összesen</th>
                    </tr>
                </thead>
            <tbody style="border:1px solid black">';

            $total_amount = "";
            foreach($user_cart as $item_id => $qty) {
                $item_data = $this->item_model->item_by_id($item_id);
                $price = price_calculator($item_data['item_netprice'],$item_data['item_netpricesale'],$item_data['item_tax']);
                $order_items .= '<tr>
                    <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left">'.$item_data['item_name'].'</td>
                    <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left">'.$qty.' db</td>
                    <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left">'.number_format($price,0,'','.').'</td>
                    <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left">'.number_format(($price*$qty),0,'','.').' Ft</td>
                </tr>';
                $total_amount = $total_amount + ($price * $qty);
            }
            $total_amount = $total_amount + $ship[1];
            $order_items .= "</tbody>";

            $body = $order_template->ordertemplate_template;
            $body = str_replace('#email_logo',get_cfg('email_logo'),$body);
            $body = str_replace('#order_id',$order_id,$body);
            $body = str_replace('#order_date',$user_model['order_date'],$body);
            $body = str_replace('#order_curier',$ship[0],$body);
            $body = str_replace('#order_bill',billing($user_model['billing']),$body);
            $body = str_replace('#order_username',$user_model['order_invlname']." ".$user_model['order_invfname'],$body);
            $body = str_replace('#order_track_url',$order_track_url,$body);
            $body = str_replace('#order_items',$order_items,$body);
            $body = str_replace('#order_ship',number_format($ship[1],0,'','.')." Ft",$body);
            $body = str_replace('#order_total',number_format($total_amount,0,'','.')." Ft",$body);
            $body = str_replace('#tel',get_cfg('company_phone'),$body);
            $body = str_replace('#email',get_cfg('email'),$body);
            $body = str_replace('#base_url',base_url(),$body);
            $body = str_replace('#aszf',base_url(get_cfg('aszf')),$body);

            $this->email->message($body);
            $this->email->send();

            $response['success'] = 'Köszönjük megrendelését';
        } else {
            $response['error'] = 'Hiba történt';
        }

        $response['csrf'] = csrf();
        echo json_encode($response);
    }
}
