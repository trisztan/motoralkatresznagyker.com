<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Backend_ordertemplate extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('ordertemplate_model');
	}

	public function ordertemplate_list() {

		$header['js'][] = 'template/backend/plugins/summernote/summernote.min.js';
		$header['js'][] = 'template/backend/plugins/datatables/jquery.dataTables.min.js';
		$header['js'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.min.js';
		$header['js'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.min.js';
		$header['js'][] = 'template/backend/plugins/slimScroll/jquery.slimscroll.min.js';
		$header['js'][] = 'template/backend/plugins/fastclick/fastclick.min.js';
		$header['js'][] = 'template/backend/plugins/toastr/toastr.min.js';
		$header['js'][] = 'template/backend/modules/ordertemplate/ordertemplate_list.js';
		$header['js'][] = 'template/backend/plugins/summernote/lang/summernote-hu-HU.min.js';
		
		$header['css'][] = 'template/backend/plugins/summernote/summernote.css';
		$header['css'][] = 'template/backend/plugins/toastr/toastr.min.css';
		$header['css'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.css';

		$data['ordertemplates'] = $this->ordertemplate_datatable();

		$this->load->view('backend/header.php', $header);
		$this->load->view('backend/ordertemplate/ordertemplate_list.php', $data);
		$this->load->view('backend/footer.php');
	}

	public function ordertemplate_datatable() {
		$html = "";

		$ordertemplates = $this->ordertemplate_model->ordertemplate_all();
		if (!empty($ordertemplates)) {
			foreach ($ordertemplates as $ordertemplate) {
				$html .= "<tr>";
				$html .= '<td style="width:20px;">' . $ordertemplate->ordertemplate_id . '</td>';
				$html .= '<td><b>' . $ordertemplate->ordertemplate_name . '</b></td>';
				$html .= '<td>' . $ordertemplate->ordertemplate_color . '</td>';
				$html .= '<td style="width:100px;"><button class="btn btn-info" role="button" data-ordertemplate-edit="' . $ordertemplate->ordertemplate_id . '"><i class="fa fa-pencil"></i></button>
              <button class="btn btn-danger" role="button" data-ordertemplate-delete="' . $ordertemplate->ordertemplate_id . '"><i class="fa fa-times"></i></button></td>';
				$html .= "</tr>";
			}
		}
		return $html;
	}

	public function ordertemplate_modal() {
		$post = $this->input->post();
		$data = [];

		if(isset($post['id'])) {
			$data = $this->ordertemplate_model->ordertemplate_get((int)$post['id']);
		}

		$html = $this->load->view('backend/ordertemplate/ordertemplate_'.$post['type'].'.php',$data, true);

		$msg['csrf'] = csrf();
		$msg['data'] = $html;
		echo json_encode($msg);
	}

	public function ordertemplate_add() {
		$post = $_POST;
		if ($post) {
			foreach($post as $postname => $postvalue){
				if(!empty($postvalue)){
					$model[$postname] = $postvalue;
				}
			}

			if(empty($model['ordertemplate_name'])){
					$msg['error']['ordertemplate_name'] = "A mező kitöltése kötelező!";
			}

			if(empty($model['ordertemplate_template'])){
					$msg['error']['ordertemplate_template'] = "A mező kitöltése kötelező!";
			}

			if(empty($model['ordertemplate_color'])){
					$msg['error']['ordertemplate_color'] = "A mező kitöltése kötelező!";
			}

			if(empty($msg)){
				$ordertemplate = $this->ordertemplate_model->ordertemplate_add($model);
				if ($ordertemplate) {
					$msg['toastr']['success'] = "A sablon sikeresen hozzáadásra került!";
				} else {
					$msg['toastr']['error'] = "A sablont nem sikerült hozzáadni!";
				}
			}
			else
			{
				$msg['toastr']['error'] = "A sablont nem sikerült hozzáadni!";
			}
		}

		$msg['csrf'] = csrf();
		$msg['data'] = $this->ordertemplate_datatable();
		echo json_encode($msg);
	}

	public function ordertemplate_edit() {
		$post = $_POST;
		if ($post) {
			foreach($post as $postname => $postvalue){
				if(!empty($postvalue) OR ($postvalue == 0)){
					$model[$postname] = $postvalue;
				}
			}
			$ot_id = $model['ordertemplate_id'];
			unset($model['ordertemplate_id']);

			if(empty($model['ordertemplate_name'])){
					$msg['error']['ordertemplate_name'] = "A mező kitöltése kötelező!";
			}

			if(empty($model['ordertemplate_template'])){
					$msg['error']['ordertemplate_template'] = "A mező kitöltése kötelező!";
			}

			if(empty($model['ordertemplate_color'])){
					$msg['error']['ordertemplate_color'] = "A mező kitöltése kötelező!";
			}

			if(empty($msg)){
				$ordertemplate = $this->ordertemplate_model->ordertemplate_update($ot_id,$model);
				if ($ordertemplate) {
					$msg['toastr']['success'] = "A sablon sikeresen frissítésre került!";
				} else {
					$msg['toastr']['error'] = "A sablont nem sikerült frissíteni!";
				}
			}
			else
			{
				$msg['toastr']['error'] = "A sablont nem sikerült frissíteni!";
			}
		}

		$msg['csrf'] = csrf();
		$msg['data'] = $this->ordertemplate_datatable();
		echo json_encode($msg);
	}

	/**
	 * [ordertemplate_delete description]
	 * @return [type] [description]
	 */
	public function ordertemplate_delete() {
		$ordertemplate_id = $this->input->post("ordertemplate_id");

		if($ordertemplate_id)
		{
			$msg['toastr']['success'] = "A felhasználó törlésre került!";
			$ordertemplate = $this->ordertemplate_model->ordertemplate_delete($ordertemplate_id);
		}
		else
		{
			$msg['toastr']['error'] = "A törlés nem sikerült";
		}

		$msg['csrf'] = csrf();
		$msg['data'] = $this->ordertemplate_datatable();
		echo json_encode($msg);
	}
}
