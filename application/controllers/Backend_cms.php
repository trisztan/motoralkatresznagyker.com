<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Backend_cms extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('cms_model');
	}

	public function cms_list() {

		$header['js'][] = 'template/backend/plugins/datatables/jquery.dataTables.min.js';
		$header['js'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.min.js';
		$header['js'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.min.js';
		$header['js'][] = 'template/backend/plugins/slimScroll/jquery.slimscroll.min.js';
		$header['js'][] = 'template/backend/plugins/fastclick/fastclick.min.js';
		$header['js'][] = 'template/backend/plugins/toastr/toastr.min.js';
		$header['js'][] = 'https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js';
		$header['js'][] = 'template/backend/js/pages/cms_list.js';

		$header['css'][] = 'template/backend/plugins/toastr/toastr.min.css';
		$header['css'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.css';

		$data['cmss'] = $this->cms_datatable();

		$this->load->view('backend/header.php', $header);
		$this->load->view('backend/cms/cms_list.php', $data);
		$this->load->view('backend/footer.php');
	}

	public function cms_datatable() {
		$html = "";

		$cmss = $this->cms_model->cms_all();
		if (!empty($cmss)) {
			foreach ($cmss as $cms) {
				$html .= "<tr>";
				$html .= '<td style="width:20px;">' . $cms->cms_id . '</td>';
				$html .= '<td><b>' . $cms->cms_title . '</b></td>';
				$html .= '<td><a href="' .base_url($cms->cms_slug) . '">'.$cms->cms_slug.'</a></td>';
				$html .= '<td><b>' . $cms->cms_keywords . '</b></td>';
				$html .= '<td>' . $cms->cms_description . '</td>';
				$html .= '<td>' . ucfirst($cms->cms_position) . '</td>';
				$html .= '<td style="width:30px;">' . ($cms->cms_status == 1 ? '<i class="fa fa-check fa-2x has-success green"></i>' : '<i class="fa fa-times fa-2x red"></i>') . '</td>';
				$html .= '<td style="width:100px;"><button class="btn btn-info" role="button" data-cms-edit="' . $cms->cms_id . '"><i class="fa fa-pencil"></i></button>
              <button class="btn btn-danger" role="button" data-cms-delete="' . $cms->cms_id . '"><i class="fa fa-times"></i></button></td>';
				$html .= "</tr>";
			}
		}
		return $html;
	}

	public function cms_modal() {
		$post = $this->input->post();
		$data = [];

		if(isset($post['id']) && !empty($post['id'])) {
			$data = $this->cms_model->cms_get((int)$post['id']);
		}

		$html = $this->load->view('backend/cms/cms_'.$post['type'].'.php',$data, true);

		$msg['csrf'] = csrf();
		$msg['data'] = $html;
		echo json_encode($msg);
	}

	public function cms_add() {
		$post = $_POST;
		if ($post) {
			foreach($post as $postname => $postvalue){
				if(!empty($postvalue)){
					$model[$postname] = $postvalue;
				}
			}

			if(empty($model['cms_title'])){
					$msg['error']['cms_title'] = "A mező kitöltése kötelező!";
			}

			if(empty($model['cms_slug'])){
					$msg['error']['cms_slug'] = "A mező kitöltése kötelező!";
			}

			if(empty($model['cms_position'])){
					$msg['error']['cms_position'] = "A mező kitöltése kötelező!";
			}

			if(empty($msg) && isset($model['cms_slug'])) {
				$check_cms_slug = $this->cms_model->cms_exist($model['cms_slug']);

			   	if($check_cms_slug) {
			   	  	$msg['error']['cms_slug'] = "Az oldal neve már használva van!";
			   	}
			}

			if(empty($msg)){
				$cms = $this->cms_model->cms_add($model);
				if ($cms) {
					$msg['toastr']['success'] = "Az oldal sikeresen hozzáadásra került!";
				} else {
					$msg['toastr']['error'] = "Az oldalt nem sikerült hozzáadni!";
				}
			}
			else
			{
				$msg['toastr']['error'] = "Az oldalt nem sikerült hozzáadni!";
			}
		}

		$msg['csrf'] = csrf();
		$msg['data'] = $this->cms_datatable();
		echo json_encode($msg);
	}

	public function cms_edit() {
		$this->config->set_item('global_xss_filtering', false);
		$post = $this->input->post();
		$this->config->set_item('global_xss_filtering', true);
		if ($post) {
			foreach($post as $postname => $postvalue){
				if(!empty($postvalue)){
					$model[$postname] = $postvalue;
				}
			}

			$cms_id = $model["cms_id"];
			unset($model["cms_id"]);

			if(empty($model['cms_title'])){
				$msg['error']['cms_title'] = "A mező kitöltése kötelező!";
			}

			if(empty($model['cms_slug'])){
				$msg['error']['cms_slug'] = "A mező kitöltése kötelező!";
			}

			if(empty($model['cms_position'])){
				$msg['error']['cms_position'] = "A mező kitöltése kötelező!";
			}

			if(empty($msg) && ($model['cms_slug'] != $model['cms_slug_original'])) {
				$check_cms_slug = $this->cms_model->cms_exist($model['cms_slug']);

			   	if($check_cms_slug) {
			   	  	$msg['error']['cms_slug'] = "Az oldal neve már használva van!";
			   	}
			}
			unset($model['cms_slug_original']);

			if(empty($msg)){
				$cms = $this->cms_model->cms_update($cms_id, $model);
				if ($cms) {
					$msg['toastr']['success'] = "Az oldal sikeresen módosításra került!";
				} else {
					$msg['toastr']['error'] = "Az oldalt nem sikerült módosítani!";
				}
			}
			else
			{
				$msg['toastr']['error'] = "Az oldalt nem sikerült módosítani!";
			}
		}

		$msg['csrf'] = csrf();
		$msg['data'] = $this->cms_datatable();
		echo json_encode($msg);
	}

	/**
	 * [cms_delete description]
	 * @return [type] [description]
	 */
	public function cms_delete() {
		$cms_id = $this->input->post("cms_id");

		if($cms_id)
		{
			$msg['toastr']['success'] = "Az oldal törlésre került!";
			$cms = $this->cms_model->cms_delete($cms_id);
		}
		else
		{
			$msg['toastr']['error'] = "A törlés nem sikerült";
		}

		$msg['csrf'] = csrf();
		$msg['data'] = $this->cms_datatable();
		echo json_encode($msg);
	}
}
