<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Backend_order extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('order_model', 'order');
    }

    public function order_list()
    {
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/jquery.dataTables.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/dataTables.bootstrap.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/dataTables.bootstrap.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/slimScroll/jquery.slimscroll.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/fastclick/fastclick.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
        $this->enqueue->enqueue_js('template/backend/modules/order/order_list.js');

        $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');
        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');

        $this->load->view('backend/header.php');
        $this->load->view('backend/order/order_list.php');
        $this->load->view('backend/footer.php');
    }

    public function order_datatable()
    {
        if ($this->input->post()) {
            // Datatable columns
            $columns[0] = 'order_id';
            $columns[1] = 'order_invlname, order_invfname';
            $columns[2] = 'order_invcity, order_invstreet';
            $columns[3] = 'order_city, order_street';
            $columns[4] = 'status_id';
            $columns[5] = 'order_date';
            $columns[6] = 'order_subscribe';

            // Datatable variables
            $limit = $this->input->post('length');
            $offset = $this->input->post('start');
            $search = $this->input->post('search') ['value'];
            $order = $this->input->post('order');
            $orderby = $order[0]['dir'];
            $order = $columns[$order[0]['column']];

            $orders_fil_cnt = $this->order->order_fil_cnt($order, $orderby, $columns, $search);
            $orders_all_cnt = $this->order->order_all_cnt();
            $orders_all = $this->order->order_datatable($limit, $offset, $order, $orderby, $columns, $search);

            // Prepare datatable json data
            $orders['draw'] = $this->input->post('draw');
            $orders['recordsTotal'] = $orders_all_cnt;
            $orders['recordsFiltered'] = $orders_fil_cnt;
            $orders['data'] = [];
            $orders['csrf'] = csrf();

            if ($orders_all) {
                foreach ($orders_all as $order) {
                    $buttons = '<td style="width:100px;"><a href="'.base_url('order-edit/'.$order->order_id).'" class="btn btn-info"><i class="fa fa-pencil"></i></a>
                    <button class="btn btn-danger" role="button" data-order-delete="'.$order->order_id.'"><i class="fa fa-times"></i></button></td>';
                    $row[0] = $order->order_id;
                    $row[1] = '<b>'.$order->order_invlname.' '.$order->order_invfname.'</b><br>'.$order->order_invcompany.'<br/>'.$order->order_taxnumber.'<br><b>'.$order->order_email.'</b><br/>'.$order->order_invmobile;
                    $row[2] = '<br/>'.$order->order_invcity.'<br/>'.$order->order_invstreet.'<br/>'.$order->order_invzip;
                    $row[3] = '<br/>'.$order->order_city.'<br/>'.$order->order_street.'<br/>'.$order->order_zip;
                    $row[4] = '<span class="label bg-'.$order->ordertemplate_color.'">'.$order->ordertemplate_name.'</span>';
                    $row[5] = '<span class="label bg-blue">'.$order->order_date.'</span>';
                    $row[6] = $order->order_subscribe == 1 ? '<i class="fa fa-check fa-2x has-success green"></i>' : '<i class="fa fa-times fa-2x red"></i>';
                    $row[7] = $buttons;
                    $orders['data'][] = $row;
                    unset($row, $buttons, $image, $slug);
                }
            }

            echo json_encode($orders);
            exit();
        }
    }
    public function order_modal()
    {
        $post = $this->input->post();
        $data = [];
        if (!empty($post['id'])) {
            $data = $this->order->order_get($post['id']);
        }

        $html = $this->load->view('backend/order/order_'.$post['type'].'.php', $data, true);

        $msg['csrf'] = csrf();
        $msg['data'] = $html;
        echo json_encode($msg);
    }

    public function order_add()
    {
        $post = $this->input->post();
        if ($post) {
            foreach ($post as $postname => $postvalue) {
                if (!empty($postvalue)) {
                    $model[$postname] = $postvalue;
                }
            }

            if (empty($msg)) {

                $model['order_date'] = date('Y-m-d H:i:s');

                $order = $this->order->order_add($model);
                if ($order) {
                    $msg['toastr']['success'] = 'A rendelés sikeresen hozzáadásra került!';
                } else {
                    $msg['toastr']['error'] = 'A rendelést nem sikerült hozzáadni!';
                }
            } else {
                $msg['toastr']['error'] = 'A rendelést nem sikerült hozzáadni!';
            }
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }

    public function order_edit_item_add()
    {
        $post = $this->input->post();
        if ($post) {
            foreach ($post as $postname => $postvalue) {
                if (!empty($postvalue)) {
                    $model[$postname] = $postvalue;
                }
                else {
                    $msg['error'][$postname] = 'A mezők kitöltése kötelező!';
                }
            }

            if (empty($msg)) {
                $check = $this->order->order_check_sku($model['item_sku']);

                if(!$check) {
                    $msg['error']['item_sku'] = 'A termék nem létezik.';
                }
            }

            if (empty($msg)) {
                $order = $this->order->order_item_add($model);
                if ($order) {
                    $msg['toastr']['success'] = 'A termék sikeresen hozzáadásra került!';
                } else {
                    $msg['toastr']['error'] =  'A terméket nem sikerült hozzáadni!';
                }
            }
            else {

                $msg['toastr']['error'] = 'A terméket nem sikerült hozzáadni!';
            }
        } else {
            $msg['toastr']['error'] = 'A terméket nem sikerült hozzáadni!';
        }


        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }

    public function order_edit_item_delete()
    {
        $post = $this->input->post();

        if ($post) {

            $order_id = $_POST['order_id'];
            $item_sku = $_POST['item_sku'];

            $order = $this->order->order_item_delete($order_id, $item_sku);

            if ($order) {
                $msg['toastr']['success'] =  'A termék sikeresen törlésre került!';
            } else {
                $msg['toastr']['error'] = 'A terméket nem sikerült törölni!';
            }
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }

    public function order_edit()
    {
        $order_id = $this->uri->segment(2);

        if($order_id){
            $data['order'] = $this->order->order_get($order_id);
            $this->load->model('ordertemplate_model', 'template');
            $this->load->model('ship_model', 'ship');
            $data['order_template'] = $this->template->ordertemplate_all();

            $query = $this->db->query("SELECT o.*,oi.*,ot.*,os.*,i.item_sku,i.item_name FROM `order` o
                LEFT JOIN order_item oi ON oi.order_id = o.order_id
                LEFT JOIN order_template ot ON o.status_id = ot.ordertemplate_id
                LEFT JOIN item i ON oi.item_sku = i.item_sku
                LEFT JOIN order_ship os ON os.order_id = o.order_id
                WHERE o.order_id = '".$order_id."'");

            $data['order_items'] = $query->num_rows() > 0 ? $query->result() : false;
            $query = $this->db->query("select * from order_log where order_id = ".$order_id." order by order_date desc");
            $data['order_log'] = $query->num_rows() > 0 ? $query->result() : false;

            if($data['order']) {
                $this->enqueue->enqueue_js('template/backend/plugins/datatables/jquery.dataTables.min.js');
                $this->enqueue->enqueue_js('template/backend/plugins/datatables/dataTables.bootstrap.min.js');
                $this->enqueue->enqueue_js('template/backend/plugins/datatables/dataTables.bootstrap.min.js');
                $this->enqueue->enqueue_js('template/backend/plugins/slimScroll/jquery.slimscroll.min.js');
                $this->enqueue->enqueue_js('template/backend/plugins/fastclick/fastclick.min.js');
                $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
                $this->enqueue->enqueue_js('template/backend/modules/order/order_show.js');
                $this->enqueue->enqueue_js('template/backend/modules/order/order_edit.js');

                $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');
                $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');

                $this->load->view('backend/header.php');
                $this->load->view('backend/order/order_edit.php',$data);
                $this->load->view('backend/footer.php');
            }
            else {
                show_404();
            }
        }
        else {
            show_404();
        }
    }

    public function order_delete()
    {
        $order_id = $this->input->post('order_id');

        if ($order_id) {
            $msg['toastr']['success'] = 'Az oldal törlésre került!';
            $order = $this->order->order_delete($order_id);
        } else {
            $msg['toastr']['error'] = 'A törlés nem sikerült';
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }

    public function order_edit_save(){
        if($_POST) {
            $order_id = $_POST['order_id'];
            $email_alert = $_POST['email_alert'];

            unset($_POST['order_id'],$_POST['email_alert']);
            foreach($_POST['form_data'] as $value){
                $model[$value['name']] = $value['value'];
            }
            $email_comment = $model['email_comment'];
            $ship['ship_name'] = $model['ship_name'];
            $ship['ship_price'] = $model['ship_price'];
            unset($model['email_comment'],$model['ship_name'],$model['ship_price']);

            $this->order->order_ship_update($order_id, $ship);
            $this->order->order_update($order_id, $model);

            if(!empty($email_comment)) {
                $this->order->order_log_update($order_id, $email_comment);
            }

            if($email_alert == 'true') {

                send_email_alert($order_id, $email_comment);
            }
            $msg['toastr']['success'] = 'A megredelés módosítva lett.';
        }
        else {
            $msg['toastr']['error'] = 'A megredelést nem sikerült módosítani.';
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }
}
