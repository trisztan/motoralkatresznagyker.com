<?php

defined('BASEPATH') or exit('No direct script access allowed');

class backend_impexp extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('category_model');
        $this->load->model('item_model');
    }

    public function impexp_list()
    {
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/jquery.dataTables.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/dataTables.bootstrap.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/slimScroll/jquery.slimscroll.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/fastclick/fastclick.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/dropzone-master/dist/dropzone.js');
        $this->enqueue->enqueue_js('template/backend/plugins/jstree/dist/jstree.min.js');
        $this->enqueue->enqueue_js('template/backend/modules/impexp/impexp_list.js');

        $this->enqueue->enqueue_css('template/backend/plugins/dropzone-master/dist/basic.css');
        $this->enqueue->enqueue_css('template/backend/plugins/dropzone-master/dist/dropzone.css');
        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');
        $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');
        $this->enqueue->enqueue_css('template/backend/plugins/jstree/dist/themes/default/style.min.css');

        $this->load->view('backend/header.php');
        $this->load->view('backend/impexp/impexp_list.php');
        $this->load->view('backend/footer.php');
    }

    public function impexp_upload()
    {
        if ($this->input->post()) {
            $upload_type = xss_clean($this->input->post('upload_type'));

            if ($upload_type) {
                $target_file = FCPATH.'upload/import/'.$upload_type.'.csv';

                if ($_FILES['file']['tmp_name']) {
                    if (move_uploaded_file($_FILES['file']['tmp_name'], $target_file)) {
                        echo json_encode(array(
                            'type' => 'success',
                            'message' => 'Sikeres feltöltés',
                            'csrf' => csrf(),
                        ));
                    } else {
                        echo json_encode(array(
                            'type' => 'error',
                            'message' => 'A feltöltés nem sikerült!',
                        ));
                    }
                }
            }
        }
    }


    public function impexp_export()
    {
        if($this->input->post()) {
            $export_type = $this->input->post('export_type');

            require_once FCPATH.'vendor/parsecsv/php-parsecsv/parsecsv.lib.php';
            $csv = new parseCSV();
            file_put_contents(FCPATH.'upload/export/export.csv', "");

            if($export_type == 'product') {

                $ids = $this->input->post('export');
                $sku = [];
                foreach($ids as $id) {
                    $id = str_replace("_anchor","",$id);
                    $result = $this->item_model->item_by_cat($id);
                    if($result) {
                        foreach($result as $res){
                            $sku[] = $res->sku;
                        }
                    }
                }

                $sku = array_unique($sku);


                $csv->save(FCPATH.'upload/export/export.csv', array(
                    array(
                        'id',
                        'nev',
                        'cikkszam',
                        'leiras',
                        'aktiv',
                        'suly',
                        'afa',
                        'bruttoar',
                        'bruttoarakcios',
                        'akciokezdete',
                        'akciovege',
                        'torles'
                    )
                ), true);

                foreach($sku as $s) {
                    $res = $this->item_model->item_by_sku($s);
                    if($res){
                        $data = array($res->item_id,
                        $res->item_name,
                        $res->item_sku,
                        $res->item_longdesc,
                        $res->item_status,
                        $res->item_weight,
                        $res->item_tax,
                        $res->item_netprice*(($res->item_tax/100)+1),
                        $res->item_netpricesale*(($res->item_tax/100)+1),
                        $res->item_salestart,
                        $res->item_saleend,0);
                        $csv->save(FCPATH.'upload/export/export.csv', array($data), true);
                    }
                }
            }
            if($export_type == 'unproduct') {
                $this->db->select("item_sku  as sku");
                $query = $this->db->get('item');

                $products = $query->num_rows() > 0 ? $query->result() : false;

                $this->db->select("sku");
                $this->db->group_by('sku');
                $query = $this->db->get('category_import');
                $categorys = $query->num_rows() > 0 ? $query->result() : false;

                $cat = [];
                foreach($categorys as $category) {
                    $cat[] = trim($category->sku);
                }

                $prod = [];
                foreach($products as $product) {
                    $sku = trim($product->sku);
                    if(!in_array($sku,$cat)) {
                        $prod[] = $sku;
                    }
                }

                $csv->save(FCPATH.'upload/export/export.csv', array(
                    array(
                        'id',
                        'nev',
                        'cikkszam',
                        'leiras',
                        'aktiv',
                        'suly',
                        'afa',
                        'bruttoar',
                        'bruttoarakcios',
                        'akciokezdete',
                        'akciovege',
                        'torles'
                    )
                ), true);

                foreach($prod as $s) {
                    $res = $this->item_model->item_by_sku($s);
                    if($res){
                        $data = array($res->item_id,
                        $res->item_name,
                        $res->item_sku,
                        $res->item_longdesc,
                        $res->item_status,
                        $res->item_weight,
                        $res->item_tax,
                        $res->item_netprice*(($res->item_tax/100)+1),
                        $res->item_netpricesale*(($res->item_tax/100)+1),
                        $res->item_salestart,
                        $res->item_saleend,
                        0);
                        $csv->save(FCPATH.'upload/export/export.csv', array($data), true);
                    }
                }
            }
            if($export_type == 'category') {
                $ids = $this->input->post('export');
                $category = [];
                $query = "";
                $query .= "SELECT * FROM category_import WHERE";

                foreach($ids as $id) {
                    $slug = str_replace("_anchor","",$id);
                    $query .= " slug = '".$slug."' OR";
                    $query .= " slug2 = '".$slug."' OR";
                    $query .= " slug3 = '".$slug."' OR";
                    $query .= " slug4 = '".$slug."' OR";
                    $query .= " slug5 = '".$slug."' OR";
                    $query .= " slug6 = '".$slug."' OR";
                }
                $query = rtrim($query,'OR');
                $query = $this->db->query($query);
                $result = $query->result();

                $csv->save(FCPATH.'upload/export/export.csv', array(
                    array(
                        'kategoria',
                        'alkategoria',
                        'gyarto',
                        'ccm',
                        'tipus',
                        'evjarat',
                        'sku',
                        'torles'
                    )
                ), true);

                foreach($result as $res) {
                    $data = array(
                        $res->kategoria,
                        $res->alkategoria,
                        $res->gyarto,
                        str_replace(" cm³","",$res->ccm),
                        $res->tipus,
                        $res->evjarat,
                        $res->sku,
                        0);
                    $csv->save(FCPATH.'upload/export/export.csv', array($data), true);
                }
            }
        } else {
            $path = FCPATH.'upload/export/export.csv';
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            header('Content-Type: '.finfo_file($finfo, $path));

            $finfo = finfo_open(FILEINFO_MIME_ENCODING);
            header('Content-Transfer-Encoding: '.finfo_file($finfo, $path));

            header('Content-disposition: attachment; filename="'.basename($path).'"');
            readfile($path);
        }
    }

    public function impexp_jstree() {

            $parent = $_REQUEST["parent"];
            $data = array();

            if ($parent == "#") {
                $this->db->group_by('kategoria');
                $query = $this->db->get('category_import');
                $roots = $query->result();
                foreach($roots as $root){
                    $data[] = array(
                        "id" => $root->slug,
                        "text" =>  $root->kategoria,
                        "icon" => "fa fa-folder",
                        "children" => true,
                        "type" => "root"
                    );
                }
            } else {

                $category = $this->category_model->category_search($parent);
                foreach($category as $cat){
                    $data[] = array(
                        "id" => $cat->slug,
                        "text" =>  $cat->kategoria,
                        "icon" => "fa fa-folder",
                        "children" => true,
                    );
                }
            }

            header('Content-type: text/json');
            header('Content-type: application/json');
            echo json_encode($data);
    }

    public function impexp_import()
    {
        if ($this->input->post()) {
            $import_type = xss_clean($this->input->post('import_type'));
            $limit = xss_clean($this->input->post('limit'));
            $offset = xss_clean($this->input->post('offset'));
            require_once FCPATH.'vendor/parsecsv/php-parsecsv/parsecsv.lib.php';

            if(!file_exists(FCPATH.'upload/import/'.$import_type.'.csv')) {
                $return['csrf'] = csrf();
                $return['msg'] = "Nem található importálható file. Kérlek tölts fel egyet.";
                echo json_encode($return);
                exit();
            }

            if ($import_type == 'products') {
                $file = FCPATH.'upload/import/'.$import_type.'.csv';

                if ($offset == 0) {
                    $csv = new parseCSV($file);
                    $csvsize = count($csv->data);
                    $this->session->set_userdata('csvsize', $csvsize);
                    $offset = 1;
                }
                $csv = new parseCSV();
                $csv->heading = false;
                $csv->limit = $limit;
                $csv->offset = $offset;
                $csv->auto_depth = 6;
                $csv->auto($file);

                foreach ($csv->data as $import) {
                    $model['item_id'] = $import[0];
                    $model['item_name'] = $import[1];
                    $model['item_sku'] = $import[2];
                    $model['item_longdesc'] = $import[3];
                    $model['item_status'] = $import[4];
                    $model['item_weight'] = $import[5];
                    $model['item_tax'] = $import[6];
                    $model['item_netprice'] = ( $import[7] / ( ( $import[6] /100) +1) );
                    $model['item_netpricesale'] = ( $import[8] / ( ( $import[6] / 100 ) +1) );
                    $model['item_salestart'] = $import[9];
                    $model['item_saleend'] = $import[10];
                    $model['item_slug'] = create_slug($import[1]);

                    if($import[11] == 1){
                        $this->db->where('item_id',$model['item_id']);
                        $this->db->delete('item');
                        //echo "delete";
                        //exit();
                    }
                    else {
                        $this->db->where('item_sku',$model['item_sku']);
                        $query = $this->db->get('item');
                        if($query->num_rows() < 1) {
                            unset($model['item_id']);
                            //echo "insert";
                            //exit();
                            $this->db->insert('item', $model);
                        } else {
                            $this->db->where('item_id',$model['item_id']);
                            unset($model['item_id']);
                            //echo "update";
                            //exit();
                            $this->db->update('item',$model);
                        }
                    }

                }
                $maxsize = $this->session->userdata('csvsize');
                $offset = $offset + $limit;

                $return['limit'] = $limit;
                $return['offset'] = $offset;
                $return['percent'] = $offset / ($maxsize / 100);
                $return['csrf'] = csrf();
                $return['import_type'] = $import_type;

                if($return['percent'] > 100){
                    @unlink(FCPATH.'upload/import/'.$import_type.'.csv');
                }

                echo json_encode($return);
            }

            if ($import_type == 'categories') {
                $file = FCPATH.'upload/import/'.$import_type.'.csv';

                if ($offset == 0) {
                    $csv = new parseCSV($file);
                    $csvsize = count($csv->data);
                    $this->session->set_userdata('csvsize', $csvsize);
                    $offset = 1;
                }
                $csv = new parseCSV();
                $csv->heading = false;
                $csv->limit = $limit;
                $csv->offset = $offset;
                $csv->auto_depth = 7;
                $csv->auto($file);
                    /*
                [0] => kategoria
                [1] => alkategoria
                [2] => gyarto
                [3] => ccm
                [4] => tipus
                [5] => evjarat
                [6] => sku*/
                foreach ($csv->data as $import) {
                    $model['kategoria'] = $import[0];
                    $model['alkategoria'] = $import[1];
                    $model['gyarto'] = $import[2];
                    $model['ccm'] = $import[3].' cm³';
                    $model['tipus'] = $import[4];
                    $model['evjarat'] = $import[5];
                    $model['sku'] = $import[6];

                    $model['slug'] = create_slug($model['kategoria']);

                    $model['slug2'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria']);

                    $model['slug3'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria'].'-'.
                                                  $model['gyarto']);

                    $model['slug4'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria'].'-'.
                                                  $model['gyarto'].'-'.
                                                  $import[3].'ccm');

                    $model['slug5'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria'].'-'.
                                                  $model['gyarto'].'-'.
                                                  $import[3].'ccm-'.
                                                  $model['tipus']);

                    $model['slug6'] = create_slug($model['kategoria'].'-'.
                                                  $model['alkategoria'].'-'.
                                                  $model['gyarto'].'-'.
                                                  $import[3].'ccm-'.
                                                  $model['tipus'].'-'.
                                                  $model['evjarat']);

                    if($import[7] == 1) {
                        $this->db->where('slug',$model['slug']);
                        $this->db->where('slug2',$model['slug2']);
                        $this->db->where('slug3',$model['slug3']);
                        $this->db->where('slug4',$model['slug4']);
                        $this->db->where('slug5',$model['slug5']);
                        $this->db->where('slug6',$model['slug6']);
                        $this->db->where('sku',$model['sku']);
                        $this->db->delete('category_import');
                    }
                    else {
                        $this->db->where('slug',$model['slug']);
                        $this->db->where('slug2',$model['slug2']);
                        $this->db->where('slug3',$model['slug3']);
                        $this->db->where('slug4',$model['slug4']);
                        $this->db->where('slug5',$model['slug5']);
                        $this->db->where('slug6',$model['slug6']);
                        $this->db->where('sku',$model['sku']);
                        $query = $this->db->get('category_import');

                        if($query->num_rows() == 0) {
                            $this->db->insert('category_import', $model);
                        }
                    }
                }

                $maxsize = $this->session->userdata('csvsize');
                $offset = $offset + $limit;

                $return['limit'] = $limit;
                $return['offset'] = $offset;
                $return['percent'] = $offset / ($maxsize / 100);

                if($return['percent'] > 100){
                    @unlink(FCPATH.'upload/import/'.$import_type.'.csv');
                }

                $return['csrf'] = csrf();
                $return['import_type'] = $import_type;
                echo json_encode($return);
            }
        }
    }
}
