<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Example use of the CodeIgniter Sitemap Generator Model
 *
 * @author Gerard Nijboer <me@gerardnijboer.com>
 * @version 1.0
 * @access public
 *
 */
class Sitemap extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('sitemapmodel');
	}

	/**
	 * Generate a sitemap index file
	 * More information about sitemap indexes: http://www.sitemaps.org/protocol.html#index
	 */
	public function index() {
		$this->sitemapmodel->add(base_url('sitemap-general.xml'), date('Y-m-d', time()));
		$this->sitemapmodel->add(base_url('sitemap-products.xml'), date('Y-m-d', time()));

		$query = $this->db->query('SELECT slug FROM category_Import
		UNION
		SELECT slug2 FROM category_Import
		UNION
		SELECT slug3 FROM category_Import
		UNION
		SELECT slug4 FROM category_Import
		UNION
		SELECT slug5 FROM category_Import
		UNION
		SELECT slug6 FROM category_Import');

		$categories = ceil($query->num_rows() / 10000);

		for($i=1;$i <= $categories; $i++)
		{
			$this->db->where('router_slug','sitemap-category-'.$i.'.xml');
			$query = $this->db->get('router');

			if($query->num_rows() == 0){
				$model['router_slug'] = 'sitemap-category-'.$i.'.xml';
				$model['router_method'] = 'category';
				$model['router_controller'] = 'sitemap';
				$this->db->insert('router',$model);
			}

			$this->sitemapmodel->add(base_url('sitemap-category-'.$i.'.xml'), date('Y-m-d', time()));
		}

		$this->sitemapmodel->output('sitemapindex');
	}

	/**
	 * Generate a sitemap both based on static urls and an array of urls
	 */
	public function general() {
		$this->sitemapmodel->add(base_url(), NULL, 'monthly', 1);

		$query = $this->db->get('cms');
		$cmss = $query->result();
		foreach ($cmss as $cms) {
			$this->sitemapmodel->add(base_url($cms->cms_slug),date('Y-m-d', time()),'weekly', '0.8');
		}
		$this->sitemapmodel->output();
	}

	public function products() {
		$query = $this->db->get('item');
		$cmss = $query->result();
		foreach ($cmss as $cms) {
			$this->sitemapmodel->add(base_url($cms->item_slug),date('Y-m-d', time()),'weekly', '0.8');
		}
		$this->sitemapmodel->output();
	}

	public function category() {
		$parts = $this->uri->segment(1);
		$parts = $str = preg_replace('/[^0-9.]+/', '', $parts);

		$offset = ($parts-1)*10000;
		$limit = $parts*10000;

		$query = $this->db->query('SELECT slug FROM (SELECT slug FROM category_Import
		UNION
		SELECT slug2 FROM category_Import
		UNION
		SELECT slug3 FROM category_Import
		UNION
		SELECT slug4 FROM category_Import
		UNION
		SELECT slug5 FROM category_Import
		UNION
		SELECT slug6 FROM category_Import) as t LIMIT '.$limit.' OFFSET '.$offset);

		$categories = $query->result();
		foreach ($categories as $category) {
			$this->sitemapmodel->add(base_url($category->slug),date('Y-m-d', time()),'weekly', '0.8');
		}

		$this->sitemapmodel->output();
	}

}
