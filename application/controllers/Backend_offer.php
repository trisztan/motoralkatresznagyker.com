<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Backend_offer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('offer_model', 'offer');
    }
    /**
     * offer list.
     *
     * @return string Show offer list page on admin
     */
    public function offer_list()
    {
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/jquery.dataTables.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/dataTables.bootstrap.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/slimScroll/jquery.slimscroll.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/fastclick/fastclick.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/dropzone-master/dist/dropzone.js');
        $this->enqueue->enqueue_js('template/backend/modules/offer/offer_list.js');

        $this->enqueue->enqueue_css('template/backend/plugins/dropzone-master/dist/basic.css');
        $this->enqueue->enqueue_css('template/backend/plugins/dropzone-master/dist/dropzone.css');
        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');
        $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');

        $this->load->view('backend/header.php');
        $this->load->view('backend/offer/offer_list.php');
        $this->load->view('backend/footer.php');
    }
    /**
     * Load template to modal.
     *
     * @return string
     */
    public function offer_modal()
    {
        $post = $this->input->post();
        $data = [];
        if (isset($post['id']) && !empty($post['id'])) {
            $data = $this->offer->offer_get((int) $post['id']);
        }
        $this->load->model('offertemplate_model','offer_template');
        $data->offers = $this->offer_template->offertemplate_all();

        $html = $this->load->view('backend/offer/offer_'.$post['type'].'.php', $data, true);
        $msg['csrf'] = csrf();
        $msg['data'] = $html;
        echo json_encode($msg);
    }

    public function offer_edit()
    {
        $post = $this->input->post();
        if ($post) {
            foreach ($post as $postname => $postvalue) {
                if (!empty($postvalue) OR ($postvalue == 0)) {
                    $model[$postname] = $postvalue;
                }
            }

            $offer_id = $model['offer_id'];
            unset($model['offer_id']);
            $model['offer_answered'] = 1;
            $offer = $this->offer->offer_update($offer_id, $model);
            if(!isset($model['offer_replymsg'])){
                $model['offer_replymsg'] = "";
            }
            send_offer_alert($offer_id,$model['offer_replymsg']);

            if ($offer) {
                $msg['toastr']['success'] = 'Az ajánlatkérés sikeresen megválaszolásra került!';
            } else {
                $msg['toastr']['error'] = 'Az ajánlatkérést nem sikerült megválaszolni!';
            }
        } else {
            $msg['toastr']['error'] = 'Az ajánlatkérést nem sikerült megválaszolni!';
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }

    public function offer_datatable()
    {
        if ($this->input->post()) {
            // Datatable columns
            $columns[0] = 'offer_id';
            $columns[1] = 'offer_title';
            $columns[2] = 'offer_message';
            $columns[3] = 'offer_name';
            $columns[4] = 'offer_created';
            $columns[5] = 'offer_email';
            $columns[6] = 'offer_phone';
            $columns[7] = 'offer_answered';
            $columns[8] = 'offer_archived';
            // Datatable variables
            $limit = $this->input->post('length');
            $offset = $this->input->post('start');
            $search = $this->input->post('search') ['value'];
            $order = $this->input->post('order');
            $orderby = $order[0]['dir'];
            $order = $columns[$order[0]['column']];

            //
            $categories_fil_cnt = $this->offer->offer_fil_cnt($order, $orderby, $columns, $search);
            $categories_all_cnt = $this->offer->offer_all_cnt();
            $categories_all = $this->offer->offer_datatable($limit, $offset, $order, $orderby, $columns, $search);

            // Prepare datatable json data
            $categories['draw'] = $this->input->post('draw');
            $categories['recordsTotal'] = $categories_all_cnt;
            $categories['recordsFiltered'] = $categories_fil_cnt;
            $categories['data'] = [];
            $categories['csrf'] = csrf();

            if ($categories_all) {
                foreach ($categories_all as $offer) {
                    $buttons = '<td style="width:100px;"><button class="btn btn-info" role="button" data-offer-edit="'.$offer->offer_id.'"><i class="fa fa-reply"></i></button>
                    <button class="btn btn-warning" role="button" data-offer-archive="'.$offer->offer_id.'"><i class="fa fa-archive"></i></button></td>
                    <button class="btn btn-danger" role="button" data-offer-delete="'.$offer->offer_id.'"><i class="fa fa-times"></i></button>';
                    $row[0] = $offer->offer_id;
                    $row[1] = $offer->offer_title;
                    $row[2] = $offer->offer_message;
                    $row[3] = $offer->offer_name;
                    $row[4] = $offer->offer_created;
                    $row[5] = $offer->offer_email;
                    $row[6] = $offer->offer_phone;
                    $row[7] = $offer->offer_answered == 1 ? '<i class="fa fa-check fa-2x has-success green"></i>':'<i class="fa fa-times fa-2x has-error red"></i>';
                    $row[8] = $offer->offer_archived == 1 ? '<i class="fa fa-check fa-2x has-success green"></i>':'<i class="fa fa-times fa-2x has-error red"></i>';
                    $row[9] = $buttons;
                    // Datatable variables
                    $categories['data'][] = $row;
                    unset($row, $buttons, $image, $slug);
                }
            }

            echo json_encode($categories);
            exit();
        }
    }


    public function offer_delete()
    {
        $offer_id = $this->input->post('offer_id');

        if ($offer_id) {
            $msg['toastr']['success'] = 'A kupon törlésre került!';
            $coupon = $this->offer->offer_delete($offer_id);
        } else {
            $msg['toastr']['error'] = 'A törlés nem sikerült';
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }

    public function offer_archived()
    {
        $offer_id = $this->input->post('offer_id');
        if ($offer_id) {
            $msg['toastr']['success'] = 'Sikeresen archiválva';
            $model['offer_archived'] = 1;

            $offer = $this->offer->offer_update($offer_id,$model);
        } else {
            $msg['toastr']['error'] = 'Az archíválás nem sikerült';
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }
}
