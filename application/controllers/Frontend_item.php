<?php

defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Frontend CMS Class.
 */
class Frontend_item extends CI_Controller
{
    /**
     * $slug Uri Segment.
     *
     * @var string
     */
    private $slug;
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        /* Set Slug */
        $this->slug = $this->uri->segment(1);

        /* Load CSS */
        $this->enqueue->enqueue_css('template/frontend/assets/css/bootstrap.min.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-outline/pictopro-outline.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-normal/pictopro-normal.css');
        $this->enqueue->enqueue_css('template/frontend/assets/css/carat.css');
        $this->enqueue->enqueue_css('template/backend/plugins/font-awesome/css/font-awesome.min.css');
        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');
        /* Load JS */
        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.js');
        $this->enqueue->enqueue_js('http://code.jquery.com/jquery-migrate-1.2.1.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.ui.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/bootstrap.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/carat.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
        $this->enqueue->enqueue_js('template/frontend/modules/home/cart.js');

        /* Load Model */
        $this->load->model('item_model');
        $this->load->model('category_model');
    }

    public function item_list()
    {
        $data['item'] = $this->item_model->item_by_slug($this->slug);
        $data['images'] =  $this->item_model->item_image_by_id($data['item']->item_id);
        $data['categories'] = $this->category_model->category_home();
        $data['product_sales'] = $this->item_model->item_sale_latest(5);

        $header['title'] = title($data['item']->item_name);

        $this->load->view('frontend/header.php',$header);
        $this->load->view('frontend/item/item.php',$data);
        $this->load->view('frontend/footer.php');
    }
}
