<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Backend_config extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('config_model');
	}

	public function config_list() {

		$header['js'][] = 'template/backend/plugins/datatables/jquery.dataTables.min.js';
		$header['js'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.min.js';
		$header['js'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.min.js';
		$header['js'][] = 'template/backend/plugins/slimScroll/jquery.slimscroll.min.js';
		$header['js'][] = 'template/backend/plugins/fastclick/fastclick.min.js';
		$header['js'][] = 'template/backend/plugins/toastr/toastr.min.js';
		$header['js'][] = 'template/backend/modules/config/config_list.js';

		$header['css'][] = 'template/backend/plugins/toastr/toastr.min.css';
		$header['css'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.css';

		$data['configs'] = $this->config_datatable();

		$this->load->view('backend/header.php', $header);
		$this->load->view('backend/config/config_list.php', $data);
		$this->load->view('backend/footer.php');
	}

	public function config_datatable() {
		$html = "";

		$configs = $this->config_model->config_all();
		if (!empty($configs)) {
			foreach ($configs as $config) {
				$html .= "<tr>";
				$html .= '<td style="width:20px;">' . $config->config_id . '</td>';
				$html .= '<td style="width:200px;"><b>' . __e($config->config_key) . '</b></td>';
				$html .= '<td>' . htmlspecialchars($config->config_value) . '</td>';
				$html .= '<td style="width:100px;"><button class="btn btn-info" role="button" data-config-edit="' . $config->config_id . '"><i class="fa fa-pencil"></i></button></td>';
				$html .= "</tr>";
			}
		}
		return $html;
	}

	public function config_modal() {
		$post = $this->input->post();
		$data = [];

		if(isset($post['id']) && !empty($post['id'])) {
			$data = $this->config_model->config_get((int)$post['id']);
		}

		$html = $this->load->view('backend/config/config_'.$post['type'].'.php',$data, true);

		$msg['csrf'] = csrf();
		$msg['data'] = $html;
		echo json_encode($msg);
	}

	public function config_edit() {
		$post = $_POST;
		if ($post) {
			foreach($post as $postname => $postvalue){
				if(!empty($postvalue)){
					$model[$postname] = $postvalue;
				} else {
					$msg['error'][$postname] = "A mező kitöltése kötelező!";
				}
			}

			if(empty($msg)){
				$config_id = $model["config_id"];
				$config = $this->config_model->config_update($config_id, $model);
				if ($config) {
					$msg['toastr']['success'] = "A konfig sikeresen módosításra került!";
				} else {
					$msg['toastr']['error'] = "A konfigot nem sikerült módosítani!";
				}
			}
			else
			{
				$msg['toastr']['error'] = "A konfigot nem sikerült módosítani!";
			}
		}

		$msg['csrf'] = csrf();
		$msg['data'] = $this->config_datatable();
		echo json_encode($msg);
	}

	/**
	 * [config_delete description]
	 * @return [type] [description]
	 */
	public function config_delete() {
		$config_id = $this->input->post("config_id");

		if($config_id)
		{
			$msg['toastr']['success'] = "A felhasználó törlésre került!";
			$config = $this->config_model->config_delete($config_id);
		}
		else
		{
			$msg['toastr']['error'] = "A törlés nem sikerült";
		}

		$msg['csrf'] = csrf();
		$msg['data'] = $this->config_datatable();
		echo json_encode($msg);
	}

	/**
	 * [config_delete description]
	 * @return [type] [description]
	 */
	public function openhours_data() {
		$msg['csrf'] = csrf();
		$msg['data'] = get_cfg('openhours');
		echo json_encode($msg);
	}

	/**
	 * [config_delete description]
	 * @return [type] [description]
	 */
	public function openhours_save() {

		$model['config_value'] = (string)$this->input->post("bhours");
		$model['config_value'] = ltrim($model['config_value'],'[');
		$model['config_value'] = rtrim($model['config_value'],']');
		if($model['config_value'])
		{
			$this->db->where('config_key','openhours');
			$res = $this->db->update('config',$model);

			if($res){
				$msg['success'] = "A nyitvatartás mentésre került.";
			}
			else {
				$msg['error'] = "A nyitvatartás nem sikerült menteni.";
			}
		}
		else
		{
			$msg['error'] = "A nyitvatartás nem sikerült menteni.";
		}

		$msg['csrf'] = csrf();
		echo json_encode($msg);
	}

	public function openhours_list() {
		$header['js'][] = 'template/backend/plugins/slimScroll/jquery.slimscroll.min.js';
		$header['js'][] = 'template/backend/plugins/fastclick/fastclick.min.js';
		$header['js'][] = 'template/backend/plugins/toastr/toastr.min.js';
		$header['js'][] = 'template/backend/plugins/jquery.businessHours-master/jquery.businessHours.js';
		$header['js'][] = 'http://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.js';
		$header['js'][] = 'template/backend/modules/config/openhours_list.js';

		$header['css'][] = 'template/backend/plugins/jquery.businessHours-master/jquery.businessHours.css';
		$header['css'][] = 'template/backend/plugins/toastr/toastr.min.css';
		$header['css'][] = 'http://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.2.17/jquery.timepicker.min.css';
		$header['css'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.css';

		$header['title'] = title();
		$data['openhours'] = get_cfg('openhours');
		$this->load->view('backend/header.php', $header);
		$this->load->view('backend/config/openhours_list.php', $data);
		$this->load->view('backend/footer.php');
	}
}
