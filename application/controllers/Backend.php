<?php

defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Backend Controller.
 */
class Backend extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Dashboard.
     *
     * @return view Display admin
     */
    public function admin_home()
    {
        $this->enqueue->enqueue_js('template/backend/modules/home/home_list.js');

        $this->load->view('backend/header.php');
        $this->load->view('backend/admin/admin_home.php');
        $this->load->view('backend/footer.php');
        //$this->output->cache(10);
    }
    /**
     * Dashboard Login.
     *
     * @return view Display admin login page.
     */
    public function admin_login()
    {
        $this->enqueue->enqueue_js('template/backend/js/pages/admin_login.js');
        $data['url'] = base_url(str_replace('_', '-', __FUNCTION__));

        if ($this->input->post()) {
            $this->load->model('user_model');
            $this->load->helper('email');

            $mail = xss_clean($this->input->post('email'));
            $pass = xss_clean($this->input->post('pass'));

            $user = $this->user_model->admin_login($mail);

            if (valid_email($mail)) {
                if ($user) {
                    if (password_verify($pass, $user->user_password)) {
                        $this->session->set_userdata('user', base64_encode($user->user_email));
                        redirect(base_url('admin'));
                    } else {
                        $data['error']['pass'] = 'Rossz jelszó!';
                    }
                } else {
                    $data['error']['email'] = 'Heytelen e-mail cím';
                    $data['error']['pass'] = 'Heytelen helszó';
                }
            } else {
                $data['error']['email'] = 'Heytelen e-mail cím';
            }
        }
        $this->load->view('backend/admin/admin_login.php', $data);
    }
    /**
     * Admin logout.
     *
     * @return redirect Redirect user when logout
     */
    public function admin_logout()
    {
        $this->session->unset_userdata('user');
        redirect('admin-login');
    }

    public function sales_chart()
    {
        $this->db->select('o.order_id, SUM(oi.item_price*oi.item_qty) price, o.order_date');
        $this->db->join('order_item oi', 'oi.order_id = o.order_id');
        $this->db->group_by(' o.order_id');
        $result = $this->db->get('order o');

        if ($_POST['data'] == 'day') {
            $last_7_days = date_range(date('Y-m-d', strtotime('-7 day')), date('Y-m-d'), '+1 day', 'Y-m-d');

            foreach ($last_7_days as $key => $value) {
                $sum = 0;
                foreach ($result->result() as $res) {
                    $createDate = new DateTime($res->order_date);
                    if ($createDate->format('Y-m-d') == $value) {
                        $sum += $res->price;
                    }
                }
                $last_7_day[] = array('y' => $value, 'a' => (int) $sum);
            }
            echo json_encode(array('result' => $last_7_day, 'type' => 'day', 'csrf' => csrf()));
        } elseif ($_POST['data'] == 'month') {
            $last_30_days = date_range(date('Y-m-d', strtotime('-1 year')), date('Y-m-d'), '+1 day', 'Y-m');

            foreach ($last_30_days as $key => $value) {
                $sum = 0;
                foreach ($result->result() as $res) {
                    $createDate = new DateTime($res->order_date);
                    if ($createDate->format('Y-m') == $value) {
                        $sum += $res->price;
                    }
                }
                $last_30_day[] = array('y' => $value, 'a' => (int) $sum);
            }
            echo json_encode(array('result' => $last_30_day, 'type' => 'month', 'csrf' => csrf()));
        } elseif ($_POST['data'] == 'year') {
            $last_years = date_range(date('Y-m-d', strtotime('-5 year')), date('Y-m-d'), '+1 day', 'Y');
            foreach ($last_years as $key => $value) {
                $sum = 0;
                foreach ($result->result() as $res) {
                    $createDate = new DateTime($res->order_date);
                    if ($createDate->format('Y') == $value) {
                        $sum += $res->price;
                    }
                }
                $last_year[] = array('y' => $value, 'a' => (int) $sum);
            }
            echo json_encode(array('result' => $last_year, 'type' => 'year', 'csrf' => csrf()));
        }
    }
}
