<?php

defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Frontend CMS Class.
 */
class Frontend_cms extends CI_Controller
{
    /**
     * $slug Uri Segment.
     *
     * @var string
     */
    private $slug;
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        /* Set Slug */
        $this->slug = $this->uri->segment(1);

        /* Load CSS */
        $this->enqueue->enqueue_css('template/frontend/assets/css/bootstrap.min.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-outline/pictopro-outline.css');
        $this->enqueue->enqueue_css('template/frontend/libraries/pictopro-normal/pictopro-normal.css');
        $this->enqueue->enqueue_css('template/frontend/assets/css/carat.css');
        $this->enqueue->enqueue_css('template/backend/plugins/font-awesome/css/font-awesome.min.css');
        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');
        /* Load JS */
        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.js');
        $this->enqueue->enqueue_js('http://code.jquery.com/jquery-migrate-1.2.1.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/jquery.ui.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/bootstrap.js');
        $this->enqueue->enqueue_js('template/frontend/assets/js/carat.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');

        /* Load Model */
        $this->load->model('cms_model');
    }
    /**
     * Cms page.
     *
     * @return string Show cms page
     */
    public function page_list()
    {
        $data['cms'] = $this->cms_model->cms_by_slug($this->slug);

        $header['title'] = title($data['cms']->cms_title);

        $this->load->view('frontend/header.php',$header);
        $this->load->view('frontend/cms/page_list.php', $data);
        $this->load->view('frontend/footer.php');
        /*$this->output->cache(10);*/
    }
    /**
     * Contact Page.
     *
     * @return string Show contact page
     */
    public function page_contact()
    {
        $this->enqueue->enqueue_js('template/frontend/modules/contact/contact.js');

        $header['title'] = title('Kapcsolat');

        $this->load->view('frontend/header.php',$header);
        $this->load->view('frontend/cms/page_contact.php');
        $this->load->view('frontend/footer.php');
        /*$this->output->cache(10);*/
    }
    /**
     * Offer Page.
     *
     * @return string Show Offer page
     */
    public function page_offer()
    {
        $this->enqueue->enqueue_js('template/frontend/modules/offer/offer.js');

        $header['title'] = title('Árajánlatkérés');

        $this->load->view('frontend/header.php',$header);
        $this->load->view('frontend/cms/page_offer.php');
        $this->load->view('frontend/footer.php');
        /*$this->output->cache(10);*/
    }

    public function offer_send() {

        $post = $this->input->post();
        if ($post) {
            foreach($post as $postname => $postvalue){
                if(!empty($postvalue)){
                    $model[$postname] = $postvalue;
                }
                else {
                    $msg['error'][$postname] = "A mező kitöltése kötelező!";
                }
            }

            if(empty($msg)) {
                $this->load->model('offer_model','offer');
                $offer = $this->offer->offer_add($model);
                if ($offer) {
                    $msg['toastr']['success'] = "Az árajánlatkérését sikeresen rögzítettük, hamarosan kapcsolatba lépünk önnel.";
                } else {
                    $msg['toastr']['error'] = "Az árajánlatkérését nem sikerült rögzíteni, kérjük küldje el ismét.";
                }
            } else {
                $msg['toastr']['error'] = "Az árajánlatkérését nem sikerült rögzíteni, kérjük küldje el ismét.";
            }
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }

    public function contact_send() {

        $post = $this->input->post();
        if ($post) {
            foreach($post as $postname => $postvalue){
                if(!empty($postvalue)){
                    $model[$postname] = $postvalue;
                }
                else {
                    $msg['error'][$postname] = "A mező kitöltése kötelező!";
                }
            }

            if(empty($msg)) {
                // Sender details
                $email['email'] = $this->config->item('baseEmailAddress');
                $email['sender'] = $this->config->item('baseEmailName');

                // Email küldése
                $body = "";
                $body .= "<h1>Új üzenet érkezett<h1><br/>";
                $body .= "<p>Küldő: ".$model['contact_name']."</p>";
                $body .= "<p>E-mail: ".$model['contact_email']."</p>";
                $body .= "<p>Telefon: ".$model['contact_phone']."</p>";
                $body .= "<br/>";
                $body .= "<p>Üzenet:</p>";
                $body .= $model['contact_message'];

                $this->email->from($model['contact_email'], $model['contact_name']);
                $this->email->to($email['email']);
                $this->email->subject('Kapcsolat '.$model['contact_name']);
                $this->email->message($body);

                if ($this->email->send()) {
                    $msg['toastr']['success'] = "Köszönjük hogy üzenetet küldött nekünk, hamarosan kapcsolatba lépünk önnel.";
                } else {
                    $msg['toastr']['error'] = "Az üzenetet nem sikerült rögzíteni, kérjük küldje el ismét.";
                }
            } else {
                $msg['toastr']['error'] = "Az üzenetet nem sikerült rögzíteni, kérjük küldje el ismét.";
            }
        }

        $msg['csrf'] = csrf();
        echo json_encode($msg);
    }
}
