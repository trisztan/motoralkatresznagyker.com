<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Backend_coupon extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('coupon_model');
    }
    
    public function coupon_list() {
        
        $header['js'][] = 'template/backend/plugins/datatables/jquery.dataTables.min.js';
        $header['js'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.min.js';
        $header['js'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.min.js';
        $header['js'][] = 'template/backend/plugins/slimScroll/jquery.slimscroll.min.js';
        $header['js'][] = 'template/backend/plugins/fastclick/fastclick.min.js';
        $header['js'][] = 'template/backend/plugins/toastr/toastr.min.js';
        $header['js'][] = 'template/backend/js/pages/coupon_list.js';
        
        $header['css'][] = 'template/backend/plugins/toastr/toastr.min.css';
        $header['css'][] = 'template/backend/plugins/datatables/dataTables.bootstrap.css';
        
        $data['coupons'] = $this->coupon_datatable();
        
        $this->load->view('backend/header.php', $header);
        $this->load->view('backend/coupon/coupon_list.php', $data);
        $this->load->view('backend/footer.php');
    }
    
    public function coupon_datatable() {
        $html = "";
        
        $coupons = $this->coupon_model->coupon_all();
        if (!empty($coupons)) {
            foreach ($coupons as $coupon) {
                $html .= "<tr>";
                $html .= '<td style="width:20px;">' . $coupon->coupon_id . '</td>';
                $html .= '<td><b>' . $coupon->coupon_name . '</b></td>';
                $html .= '<td>' . $coupon->coupon_code . '</td>';
                $html .= '<td><b>' . $coupon->coupon_value . '</b></td>';
                $html .= '<td>' . $coupon->coupon_datestart . '</td>';
                $html .= '<td>' . $coupon->coupon_dateend . '</td>';
                $html .= '<td style="width:30px;">' . ($coupon->coupon_status == 1 ? '<i class="fa fa-check fa-2x has-success green"></i>' : '<i class="fa fa-times fa-2x red"></i>') . '</td>';
                $html .= '<td style="width:100px;"><button class="btn btn-info" role="button" data-coupon-edit="' . $coupon->coupon_id . '"><i class="fa fa-pencil"></i></button>
                <button class="btn btn-danger" role="button" data-coupon-delete="' . $coupon->coupon_id . '"><i class="fa fa-times"></i></button></td>';
                $html .= "</tr>";
            }
        }
        return $html;
    }
    
    public function coupon_modal() {
        $post = $this->input->post();
        $data = [];
        
        if(isset($post['id']) && !empty($post['id'])) {
            $data = $this->coupon_model->coupon_get((int)$post['id']);
        }
        
        $html = $this->load->view('backend/coupon/coupon_'.$post['type'].'.php',$data, true);
        
        $msg['csrf'] = csrf();
        $msg['data'] = $html;
        echo json_encode($msg);
    }
    
    public function coupon_add() {
        $post = $this->input->post();
        if ($post) {
            foreach($post as $postname => $postvalue){
                if(!empty($postvalue)){
                    $model[$postname] = $postvalue;
                }
                else {
                    $msg['error'][$postname] = "A mező kitöltése kötelező!";
                }
            }
            
            if(isset($model['coupon_code'])) {
                $check_coupon_code = $this->coupon_model->coupon_exist($model['coupon_code']);
                
                if($check_coupon_code) {
                    $msg['error']['coupon_code'] = "A kupon kód már használva van!";
                }
            }
            
            if(empty($msg)) {
                $coupon = $this->coupon_model->coupon_add($model);
                if ($coupon) {
                    $msg['toastr']['success'] = "A kupon sikeresen hozzáadásra került!";
                } else {
                    $msg['toastr']['error'] = "A kupont nem sikerült hozzáadni!";
                }
            } else {
                $msg['toastr']['error'] = "A kupont nem sikerült hozzáadni!";
            }
        }
        
        $msg['csrf'] = csrf();
        $msg['data'] = $this->coupon_datatable();
        echo json_encode($msg);
    }
    
    public function coupon_edit() {
        $post = $this->input->post();
        if ($post) {
            foreach($post as $postname => $postvalue){
                if(!empty($postvalue)){
                    $model[$postname] = $postvalue;
                }
                else {
                    $msg['error'][$postname] = "A mező kitöltése kötelező!";
                }
            }
            
            if(empty($msg)){
                $coupon_id = $model["coupon_id"];
                $coupon_code_original = $model["coupon_code_original"];
                unset($model["coupon_id"]);
                unset($model["coupon_code_original"]);
                
                if($coupon_code_original != $model['coupon_code']) {
                    $check_coupon_code = $this->coupon_model->coupon_exist($model['coupon_code']);
                    
                    if($check_coupon_code) {
                        $msg['error']['coupon_code'] = "A kupon kód már használva van!";
                    }
                }
            }
            
            if(empty($msg)){
                $coupon = $this->coupon_model->coupon_update($coupon_id, $model);
                if ($coupon) {
                    $msg['toastr']['success'] = "A kupon sikeresen módosításra került!";
                } else {
                    $msg['toastr']['error'] = "A kupont nem sikerült módosítani!";
                }
            }
            else
            {
                $msg['toastr']['error'] = "A kupont nem sikerült módosítani!";
            }
        }
        
        $msg['csrf'] = csrf();
        $msg['data'] = $this->coupon_datatable();
        echo json_encode($msg);
    }
    
    /**
    * [coupon_delete description]
    * @return [type] [description]
    */
    public function coupon_delete() {
        $coupon_id = $this->input->post("coupon_id");
        
        if($coupon_id)
        {
            $msg['toastr']['success'] = "A kupon törlésre került!";
            $coupon = $this->coupon_model->coupon_delete($coupon_id);
        }
        else
        {
            $msg['toastr']['error'] = "A törlés nem sikerült";
        }
        
        $msg['csrf'] = csrf();
        $msg['data'] = $this->coupon_datatable();
        echo json_encode($msg);
    }
}