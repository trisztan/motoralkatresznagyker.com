<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Backend_ship extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ship_model');
    }

    public function ship_list()
    {
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/jquery.dataTables.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/dataTables.bootstrap.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/datatables/dataTables.bootstrap.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/slimScroll/jquery.slimscroll.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/fastclick/fastclick.min.js');
        $this->enqueue->enqueue_js('template/backend/plugins/toastr/toastr.min.js');
        $this->enqueue->enqueue_js('template/backend/js/pages/ship_list.js');

        $this->enqueue->enqueue_css('template/backend/plugins/toastr/toastr.min.css');
        $this->enqueue->enqueue_css('template/backend/plugins/datatables/dataTables.bootstrap.css');

        $data['ship'] = $this->datatable_ship($this->recursive_ship());

        $this->load->view('backend/header.php');
        $this->load->view('backend/ship/ship_list.php', $data);
        $this->load->view('backend/footer.php');
    }

    public function datatable_ship($ship, $i = 0)
    {
        $html = '';
        foreach ($ship as $key => $value) {
            $html .= '<tr>';
            $html .= '<td style="width: 20px;">'.$value['ship_id'].'</td>';
            $html .= '<td class="tree'.$value['level'].'">'.$value['ship_name'].'</td>';
            if ($value['ship_parentid'] == 0) {
                $html .= '<td><b>Szállítási osztály</td><td>-</td>';
                $buttonadd = '<button data-ship-modal-add="'.$value['ship_id'].'" class="btn btn-info" role="button"><i class="fa fa-plus"></i></button>';
            } else {
                $html .= '<td>'.$value['ship_valuefrom'].' kg - '.(intval($value['ship_valueto']) == 0 ? '∞' : $value['ship_valueto']).' kg </td>';
                $html .= '<td>'.$value['ship_price'].' Ft </td>';
                $buttonadd = '';
            }

            $html .= '<td style="width: 120px;">'.$buttonadd.'
				<button data-ship-modal-edit="'.$value['ship_id'].'" class="btn btn-info" role="button"><i class="fa fa-pencil"></i></button>
                <button data-ship-modal-del="'.$value['ship_id'].'" class="btn btn-danger" role="button"><i class="fa fa-times"></i></button></td>';
            $html .= '</tr>';

            if (isset($value['children'])) {
                $html .= $this->datatable_ship($value['children'], $i);
            }
        }

        return $html;
    }

    public function recursive_ship($parent_id = 0, $level = 0)
    {
        $parents = $this->ship_model->ship_parent($parent_id);
        $ship = array();

        if ($parents) {
            ++$level;
            foreach ($parents as $value) {
                $ship[$value->ship_id]['ship_id'] = $value->ship_id;
                $ship[$value->ship_id]['ship_parentid'] = $value->ship_parentid;
                $ship[$value->ship_id]['ship_name'] = $value->ship_name;
                $ship[$value->ship_id]['ship_valuefrom'] = $value->ship_valuefrom;
                $ship[$value->ship_id]['ship_valueto'] = $value->ship_valueto;
                $ship[$value->ship_id]['ship_price'] = $value->ship_price;
                $ship[$value->ship_id]['level'] = $level;

                $subship = $this->recursive_ship($value->ship_id, $level);

                if (!empty($subship)) {
                    $ship[$value->ship_id]['children'] = $subship;
                }
            }
            --$level;
        }

        return $ship;
    }

    public function ship_modal()
    {
        $post = $this->input->post();
        $data = [];

        if($post['id'] == 0) {
            $data['ship_parentid'] = 0;
        } else {
            $data = $this->ship_model->ship_get((int) $post['id']);
        }

        $html = $this->load->view('backend/ship/ship_'.$post['type'].'.php', $data, true);

        $msg['csrf'] = csrf();
        $msg['data'] = $html;
        echo json_encode($msg);
    }


    public function ship_add()
    {
        $post = $this->input->post();

        if ($post) {
            foreach ($post as $postname => $postvalue) {
                if (!empty($postvalue) || ($postvalue == 0)) {
                    $model[$postname] = $postvalue;
                }
            }
            if($model['ship_parentid'] == 0) {
                if(empty($model['ship_name'])) {
                    $msg['error']['ship_name'] = 'A mező kitöltése kötelező!';
                }
            }
            else {
                if(empty($model['ship_valuefrom'])) {
                    $msg['error']['ship_valuefrom'] = 'A mező kitöltése kötelező!';
                }

                if(empty($model['ship_valueto'])) {
                    $msg['error']['ship_valueto'] = 'A mező kitöltése kötelező!';
                }

                if(empty($model['ship_price'])) {
                    $msg['error']['ship_price'] = 'A mező kitöltése kötelező!';
                }
            }

            if (empty($msg)) {

                $ship = $this->ship_model->ship_add($model);

                if ($ship) {
                    $msg['toastr']['success'] = 'A Szálíltás sikeresen hozzáadásra került!';
                } else {
                    $msg['toastr']['error'] = 'A Szálíltást nem sikerült hozzáadni!';
                }
            } else {
                $msg['toastr']['error'] = 'A Szálíltás nem sikerült hozzáadni!';
            }
        }

        $msg['csrf'] = csrf();
        $msg['data'] = $this->datatable_ship($this->recursive_ship());
        echo json_encode($msg);
    }

    public function ship_edit()
    {
        $post = $this->input->post();

        if ($post) {
            foreach ($post as $postname => $postvalue) {
                if (!empty($postvalue) || ($postvalue == 0)) {
                    $model[$postname] = $postvalue;
                }
            }
            if($model['ship_parentid'] == 0) {
                if(empty($model['ship_name'])) {
                    $msg['error']['ship_name'] = 'A mező kitöltése kötelező!';
                }
            }
            else {
                if(empty($model['ship_valuefrom'])) {
                    $msg['error']['ship_valuefrom'] = 'A mező kitöltése kötelező!';
                }

                if(empty($model['ship_valueto'])) {
                    $msg['error']['ship_valueto'] = 'A mező kitöltése kötelező!';
                }

                if(empty($model['ship_price'])) {
                    $msg['error']['ship_price'] = 'A mező kitöltése kötelező!';
                }
            }

            $ship_id = $model['ship_id'];
            unset($model['ship_id']);

            if (empty($msg)) {

                $ship = $this->ship_model->ship_update($ship_id,$model);

                if ($ship) {
                    $msg['toastr']['success'] = 'A Szálíltás sikeresen módosításra került!';
                } else {
                    $msg['toastr']['error'] = 'A Szálíltást nem sikerült módosítani!';
                }
            } else {
                $msg['toastr']['error'] = 'A Szálíltás nem sikerült módosítani!';
            }
        }

        $msg['csrf'] = csrf();
        $msg['data'] = $this->datatable_ship($this->recursive_ship());
        echo json_encode($msg);
    }

    public function ship_delete()
    {
        if($_POST) {
            $model['ship_id'] = $this->input->post('ship_id');
            $model['ship_parentid'] = $this->input->post('ship_parentid');

            if ($model['ship_id']) {

                $deleted = $this->ship_model->ship_delete($model);

                if($deleted) {
                    $msg['toastr']['success'] = 'A szállítás törlésre került!';
                }
                else {
                    $msg['toastr']['error'] = 'A törlés nem sikerült';
                }
            } else {
                $msg['toastr']['error'] = 'A törlés nem sikerült';
            }
        } else {
            $msg['toastr']['error'] = 'A törlés nem sikerült';
        }

        $msg['csrf'] = csrf();
        $msg['data'] = $this->datatable_ship($this->recursive_ship());

        echo json_encode($msg);
    }
}
