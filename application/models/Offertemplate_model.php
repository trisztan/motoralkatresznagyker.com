<?php
class Offertemplate_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function offertemplate_all() {
        $query = $this->db->get('offer_template');
        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function offertemplate_add($model) {
        return $this->db->insert('offer_template', $model);
    }

    public function offertemplate_get($offertemplate_id) {
        $this->db->where('offertemplate_id', $offertemplate_id);
        $query = $this->db->get('offer_template');
        return $query->num_rows() > 0 ? $query->result()[0] : false;
    }

    public function offertemplate_update($offertemplate_id, $model) {
        $this->db->where('offertemplate_id', $offertemplate_id);
        return $this->db->update('offer_template',$model);
    }

    public function offertemplate_exist($what,$offertemplate_code) {
        $this->db->where($what,$offertemplate_code);
        $query = $this->db->get('offer_template');
        return $query->num_rows() == 1 ? true : false;
    }

    public function offertemplate_delete($offertemplate_id) {
        $this->db->where('offertemplate_id', $offertemplate_id);
        return $this->db->delete('offer_template');
    }
}
?>
