<?php
class Redirect_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function redirect_all() {
		$query = $this->db->get('redirect');
		return $query->num_rows() > 0 ? $query->result() : false;
	}

	public function redirect_add($model) {
		return $this->db->insert('redirect', $model);
	}

	public function redirect_get($redirect_id) {
		$this->db->where('redirect_id', $redirect_id);
		$query = $this->db->get('redirect');
		return $query->num_rows() > 0 ? $query->result()[0] : false;
	}

	public function redirect_all_cnt()
    {
        $query = $this->db->get('redirect');

        return $query->num_rows();
    }

    public function redirect_fil_cnt($order, $orderby, $columns, $search)
    {
        if (!empty($search)) {
            foreach ($columns as $column) {
                $this->db->or_like($column, $search);
            }
        }
        $query = $this->db->get('redirect');

        return $query->num_rows();
    }

    public function redirect_datatable($limit, $offset, $order, $orderby, $columns, $search)
    {
        if (!empty($search)) {
            foreach ($columns as $column) {
                $this->db->or_like($column, $search);
            }
        }
        $this->db->limit($limit, $offset);
        $this->db->order_by($order, $orderby);
        $query = $this->db->get('redirect');

        return $query->num_rows() > 0 ? $query->result() : false;
    }

	public function redirect_update($redirect_id, $model) {
		$this->db->where('redirect_id', $redirect_id);
		return $this->db->update('redirect',$model);
	}

	public function redirect_delete($redirect_id) {
		$this->db->where('redirect_id', $redirect_id);
		return $this->db->delete('redirect');
	}
}
?>
