<?php
class User_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function admin_login($email)
    {      
        $this->db->where('user_email',$email);
        $query = $this->db->get('user');
        return $query->num_rows() > 0 ? $query->result()[0] : false;
    }

    public function user_all() {
        $query = $this->db->get('user');
        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function user_add($model) {
        return $this->db->insert('user', $model);
    }

    public function user_get($user_id) {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('user');
        return $query->num_rows() > 0 ? $query->result()[0] : false;
    }
    
    public function user_update($user_id, $model) {
        $this->db->where('user_id', $user_id);
        return $this->db->update('user',$model);
    }

    public function user_exist($what,$user_code) {
        $this->db->where($what,$user_code);
        $query = $this->db->get('user');
        return $query->num_rows() == 1 ? true : false;
    }

    public function user_delete($user_id) {
        $this->db->where('user_id', $user_id);
        return $this->db->delete('user');
    }
}
?>