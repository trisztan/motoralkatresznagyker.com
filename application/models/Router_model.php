<?php
class Router_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get_config() {
		$query = $this->db->get('config');
		return $query->num_rows() > 0 ? $query->result() : false;
	}

	public function get_router($slug) {
		$this->db->where('router_slug', $slug);
		$query = $this->db->get('router');
		return $query->num_rows() > 0 ? $query->result()[0] : false;
	}

	public function router_add($model) {
		return $this->db->insert('router',$model);
	}

	public function router_update($slug, $model) {
		$this->db->where('router_slug', $slug);
		return $this->db->update('router',$model);
	}

	public function router_exist($slug) {
		$this->db->query("SELECT router_slug as 'slug' FROM router
					 UNION ALL
					 SELECT item_slug as 'slug' FROM item
					 UNION ALL
					 SELECT category_slug as 'slug' FROM category
					 UNION ALL
					 SELECT cms_slug as 'slug' FROM cms WHERE slug = '".$slug."'");
		$query = $this->db->get('router');
		return $query->num_rows() == 1 ? true : false;
	}

}
?>
