<?php
class Ship_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function ship_parent($ship_parentid = 0) {
		$this->db->where('ship_parentid', $ship_parentid);
		$query = $this->db->get('ship');
		return $query->num_rows() > 0 ? $query->result() : false;
	}

	public function ship_get($ship_id)
    {
        $this->db->where('ship_id', $ship_id);
        $query = $this->db->get('ship');

        return $query->num_rows() > 0 ? $query->result()[0] : false;
    }

    public function ship_add($model)
    {
        return $this->db->insert('ship', $model);
    }

    public function ship_update($ship_id, $model)
    {
		if($model['ship_parentid'] == 0) {
			$this->db->where('ship_name', $model['ship_nameorig']);
			unset($model['ship_parentid']);
			unset($model['ship_nameorig']);
		} else {
			$this->db->where('ship_id', $ship_id);
		}

        return $this->db->update('ship', $model);
    }

    public function ship_delete($model)
    {
		if($model['ship_parentid'] == 0){
			$this->db->where('ship_id', $model['ship_id']);
			$this->db->or_where('ship_parentid', $model['ship_id']);
		} else {
			$this->db->where('ship_id', $model['ship_id']);
		}

        return $this->db->delete('ship');
    }
}
?>
