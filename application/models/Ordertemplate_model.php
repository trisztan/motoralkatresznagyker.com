<?php
class Ordertemplate_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function ordertemplate_all() {
        $query = $this->db->get('order_template');
        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function ordertemplate_add($model) {
        $this->db->limit(1);
        $this->db->offset(0);
        $this->db->order_by('ordertemplate_id','DESC');
        $query = $this->db->get('order_template');
        $model['ordertemplate_id'] = $query->result()[0]->ordertemplate_id+1;
        return $this->db->insert('order_template', $model);
    }

    public function ordertemplate_get($ordertemplate_id) {
        $this->db->where('ordertemplate_id', $ordertemplate_id);
        $query = $this->db->get('order_template');
        return $query->num_rows() > 0 ? $query->result()[0] : false;
    }

    public function ordertemplate_update($ordertemplate_id, $model) {
        $this->db->where('ordertemplate_id', $ordertemplate_id);
        return $this->db->update('order_template',$model);
    }

    public function ordertemplate_exist($what,$ordertemplate_code) {
        $this->db->where($what,$ordertemplate_code);
        $query = $this->db->get('order_template');
        return $query->num_rows() == 1 ? true : false;
    }

    public function ordertemplate_delete($ordertemplate_id) {
        $this->db->where('ordertemplate_id', $ordertemplate_id);
        return $this->db->delete('order_template');
    }
}
?>
