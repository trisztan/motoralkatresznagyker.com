<?php

class offer_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function offer_get($offer_id)
    {
        $this->db->where('offer_id', $offer_id);
        $query = $this->db->get('offer');

        return $query->num_rows() > 0 ? $query->result()[0] : false;
    }

    public function offer_add($model)
    {
        $model['offer_created'] = date('Y-m-d H:i:s');
        return $this->db->insert('offer', $model);
    }

    public function offer_update($offer_id, $model)
    {
        $this->db->where('offer_id', $offer_id);

        return $this->db->update('offer', $model);
    }

    public function offer_delete($offer_id)
    {
        $this->db->where('offer_id', $offer_id);

        return $this->db->delete('offer');
    }

    public function offer_all_cnt()
    {
        $query = $this->db->get('offer');

        return $query->num_rows();
    }

    public function offer_fil_cnt($order, $orderby, $columns, $search)
    {
        if (!empty($search)) {
            foreach ($columns as $column) {
                $this->db->or_like($column, $search);
            }
        }
        $query = $this->db->get('offer');

        return $query->num_rows();
    }

    public function offer_datatable($limit, $offset, $order, $orderby, $columns, $search)
    {
        if (!empty($search)) {
            foreach ($columns as $column) {
                $this->db->or_like($column, $search);
            }
        }
        $this->db->limit($limit, $offset);
        $this->db->order_by($order, $orderby);
        $query = $this->db->get('offer');

        return $query->num_rows() > 0 ? $query->result() : false;
    }
}
