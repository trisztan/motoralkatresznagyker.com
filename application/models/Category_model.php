<?php

class Category_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function category_home()
    {
        $this->db->select('ci.kategoria as kategoria, ci.slug, (
        SELECT COUNT(DISTINCT(it.item_id)) FROM item it
        LEFT JOIN category_import ci10 ON ci10.sku = it.item_sku
        WHERE ci10.slug = ci.slug
        ) as cnt');
        $this->db->group_by('slug');
        $query = $this->db->get('category_import ci');

        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function category_search($slug)
    {
        $query = $this->db->query("
		SELECT ci.alkategoria as kategoria, ci.slug2 as slug, (
            SELECT COUNT(DISTINCT(it.item_id)) FROM item it
            LEFT JOIN category_import ci10 ON ci10.sku = it.item_sku
            WHERE ci10.slug2 = ci.slug2
        ) as cnt
        FROM category_import ci WHERE ci.slug = '".$slug."' GROUP BY ci.slug2");
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        $query = $this->db->query("
        SELECT ci.gyarto as kategoria, ci.slug3 as slug, (
        SELECT COUNT(DISTINCT(it.item_id)) FROM item it
            LEFT JOIN category_import ci10 ON ci10.sku = it.item_sku
            WHERE ci10.slug3 = ci.slug3
        ) as cnt FROM category_import ci WHERE ci.slug2 = '".$slug."' GROUP BY ci.slug3");
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        $query = $this->db->query("
        SELECT ci.ccm as kategoria, ci.slug4 as slug, (
            SELECT COUNT(DISTINCT(it.item_id)) FROM item it
            LEFT JOIN category_import ci10 ON ci10.sku = it.item_sku
            WHERE ci10.slug4 = ci.slug4
        ) as cnt FROM category_import ci WHERE ci.slug3 = '".$slug."' GROUP BY ci.slug4 ORDER BY length(ci.slug4),ci.slug4 ASC");
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        $query = $this->db->query("SELECT ci.tipus as kategoria, ci.slug5 as slug, (
            SELECT COUNT(DISTINCT(it.item_id)) FROM item it
            LEFT JOIN category_import ci10 ON ci10.sku = it.item_sku
            WHERE ci10.slug5 = ci.slug5
        ) as cnt FROM category_import ci WHERE ci.slug4 = '".$slug."' GROUP BY ci.slug5");
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        $query = $this->db->query("SELECT ci.evjarat as kategoria, ci.slug6 as slug, (
            SELECT COUNT(DISTINCT(it.item_id)) FROM item it
            LEFT JOIN category_import ci10 ON ci10.sku = it.item_sku
            WHERE ci10.slug6 = ci.slug6
        ) as cnt FROM category_import ci WHERE ci.slug5 = '".$slug."' GROUP BY ci.slug6");

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function category_get($category_id)
    {
        $this->db->where('category_id', $category_id);
        $query = $this->db->get('category_import');

        return $query->num_rows() > 0 ? $query->result()[0] : false;
    }

    public function category_add($model)
    {
        return $this->db->insert('category_import', $model);
    }

    public function category_update($category_id, $model)
    {
        $this->db->where('category_id', $category_id);

        return $this->db->update('category_import', $model);
    }

    public function category_delete($category_id)
    {
        $this->db->where('category_id', $category_id);

        return $this->db->delete('category_import');
    }

    public function category_all_cnt()
    {
        $query = $this->db->get('category_import');

        return $query->num_rows();
    }

    public function category_fil_cnt($order, $orderby, $columns, $search)
    {
        if (!empty($search)) {
            foreach ($columns as $column) {
                $this->db->or_like($column, $search);
            }
        }
        $query = $this->db->get('category_import');

        return $query->num_rows();
    }

    public function category_datatable($limit, $offset, $order, $orderby, $columns, $search)
    {
        if (!empty($search)) {
            foreach ($columns as $column) {
                $this->db->or_like($column, $search);
            }
        }
        $this->db->limit($limit, $offset);
        $this->db->order_by($order, $orderby);
        $query = $this->db->get('category_import');

        return $query->num_rows() > 0 ? $query->result() : false;
    }
}
