<?php
class Cms_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function cms_all() {
		$query = $this->db->get('cms');
		return $query->num_rows() > 0 ? $query->result() : false;
	}

	public function cms_add($model) {
		return $this->db->insert('cms', $model);
	}

	public function cms_get($cms_id) {
		$this->db->where('cms_id', $cms_id);
		$query = $this->db->get('cms');
		return $query->num_rows() > 0 ? $query->result()[0] : false;
	}

	public function cms_by_slug($cms_slug) {
		$this->db->where('cms_slug', $cms_slug);
		$query = $this->db->get('cms');
		return $query->num_rows() > 0 ? $query->result()[0] : false;
	}
	
	public function cms_update($cms_id, $model) {
		$this->db->where('cms_id', $cms_id);
		return $this->db->update('cms',$model);
	}

	public function cms_exist($cms_slug) {
		$this->db->where('cms_slug',$cms_slug);
		$query = $this->db->get('cms');
		return $query->num_rows() == 1 ? true : false;
	}

	public function cms_delete($cms_id) {
		$this->db->where('cms_id', $cms_id);
		return $this->db->delete('cms');
	}
}
?>