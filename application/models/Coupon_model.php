<?php
class Coupon_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function coupon_all() {
		$query = $this->db->get('coupon');
		return $query->num_rows() > 0 ? $query->result() : false;
	}

	public function coupon_add($model) {
		return $this->db->insert('coupon', $model);
	}

	public function coupon_get($coupon_id) {
		$this->db->where('coupon_id', $coupon_id);
		$query = $this->db->get('coupon');
		return $query->num_rows() > 0 ? $query->result()[0] : false;
	}
	
	public function coupon_update($coupon_id, $model) {
		$this->db->where('coupon_id', $coupon_id);
		return $this->db->update('coupon',$model);
	}

	public function coupon_exist($coupon_code) {
		$this->db->where('coupon_code',$coupon_code);
		$query = $this->db->get('coupon');
		return $query->num_rows() == 1 ? true : false;
	}

	public function coupon_delete($coupon_id) {
		$this->db->where('coupon_id', $coupon_id);
		return $this->db->delete('coupon');
	}
}
?>