<?php
class Config_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function config_all() {
        $query = $this->db->get('config');
        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function config_get($config_id) {
        $this->db->where('config_id', $config_id);
        $query = $this->db->get('config');
        return $query->num_rows() > 0 ? $query->result()[0] : false;
    }

    public function config_update($config_id, $model) {
        $this->db->where('config_id', $config_id);
        return $this->db->update('config',$model);
    }
}
?>
