<?php

class Item_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function item_all()
    {
        $this->db->select('it.*,(SELECT
									i.image_url
									FROM
									image i
									LEFT JOIN item_image ii ON ii.image_id = i.image_id
									WHERE i.image_featured = 1 AND ii.item_id = it.item_id) image_url');
        $query = $this->db->get('item it');

        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function item_all_cnt($category = '')
    {
        if ($category) {
            $this->db->join('category_import ci', '
			ci.sku = it.item_sku');

            $this->db->where("(ci.slug='".$category."'
				OR ci.slug2='".$category."'
				OR ci.slug3='".$category."'
				OR ci.slug4='".$category."'
				OR ci.slug5='".$category."'
				OR ci.slug6='".$category."')");
        }
        $this->db->group_by('it.item_sku');
        $query = $this->db->get('item it');
        return $query->num_rows();
    }

    public function item_fil_cnt($order, $orderby, $columns, $search, $category = '')
    {
        if ($category) {
            $this->db->join('category_import ci', '
			ci.sku = it.item_sku');

            $this->db->where("(ci.slug='".$category."'
				OR ci.slug2='".$category."'
				OR ci.slug3='".$category."'
				OR ci.slug4='".$category."'
				OR ci.slug5='".$category."'
				OR ci.slug6='".$category."')");
        }
        $sql = '';
        if (!empty($search)) {
            foreach ($columns as $column) {
                $sql .= 'it.'.$column." LIKE '%".$search."%' OR ";
            }
            $sql .= "round(it.item_netprice*((it.item_tax/100)+1)) LIKE '%".$search."%' OR ";
            $sql .= "round(it.item_netpricesale*((it.item_tax/100)+1)) LIKE '%".$search."%'";
            $this->db->where('('.$sql.')');
        }

        $this->db->group_by('it.item_sku');
        $query = $this->db->get('item it');

        return $query->num_rows();
    }

    public function item_datatable($limit, $offset, $order, $orderby, $columns, $search, $category = '')
    {
        $this->db->select('it.item_image,
						   it.item_id,
						   it.item_name,
						   it.item_slug,
						   it.item_sku,
						   it.item_status,
						   round(it.item_netprice) item_netprice,
						   (round(it.item_netprice*((it.item_tax/100)+1))) as brutto,
						   round(it.item_netpricesale) item_netpricesale,
						   (round(it.item_netpricesale*((it.item_tax/100)+1))) as salebrutto,
						   it.item_tax,
						   it.item_weight');

        $this->db->join('category_import ci', 'ci.sku = it.item_sku');

        $sql = '';
        if (!empty($search)) {
            foreach ($columns as $column) {
                $sql .= 'it.'.$column." LIKE '%".$search."%' OR ";
            }
            $sql .= "round(it.item_netprice*((it.item_tax/100)+1)) LIKE '%".$search."%' OR ";
            $sql .= "round(it.item_netpricesale*((it.item_tax/100)+1)) LIKE '%".$search."%'";
            $this->db->where('('.$sql.')');
        }

        if ($category) {
            $this->db->where("(ci.slug='".$category."'
				OR ci.slug2='".$category."'
				OR ci.slug3='".$category."'
				OR ci.slug4='".$category."'
				OR ci.slug5='".$category."'
				OR ci.slug6='".$category."')");
        }
        $this->db->limit($limit, $offset);
        $this->db->order_by('it.'.$order, $orderby);
        $this->db->group_by('it.item_sku');
        $query = $this->db->get('item it');

        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function item_by_id($item_id)
    {
        $this->db->where('item_id', $item_id);
        $query = $this->db->get('item');

        return $query->num_rows() > 0 ? $query->result_array()[0] : false;
    }

    public function item_latest($limit)
    {
        $this->db->limit($limit);
        $query = $this->db->get('item');

        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function item_by_category($slug)
    {
        $this->db->select('i.*,c.category_slug');
        $this->db->join('item_category ic', 'c.category_id = ic.category_id', 'left');
        $this->db->join('item i', 'i.item_id = ic.item_id', 'left');
        $this->db->where('c.category_slug', $slug);
        $this->db->where('i.item_id IS NOT NULL');
        $query = $this->db->get('category c');

        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function item_by_slug($slug)
    {
        $this->db->select('item.*,
    						   round(item_netprice) item_netprice,
    						   (round(item_netprice*((item_tax/100)+1))) as brutto,
    						   round(item_netpricesale) item_netpricesale,
    						   (round(item_netpricesale*((item_tax/100)+1))) as salebrutto');

        $this->db->where('item_slug', $slug);
        $query = $this->db->get('item');

        return $query->num_rows() > 0 ? $query->result()[0] : false;
    }

    public function item_update($id, $model)
    {
        $this->db->where('item_id', $id);

        return $this->db->update('item', $model);
    }

    public function item_add($model)
    {
        $this->db->insert('item', $model);

        return $this->db->insert_id();
    }

    public function item_category_update($id, $categories)
    {
        $this->db->where('item_id', $id);
        $this->db->delete('item_category');

        if (!empty($categories)) {
            $model['item_id'] = $id;

            foreach ($categories as $category) {
                $model['category_id'] = $category;
                $query = $this->db->insert('item_category', $model);
                if (!$query) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public function item_sale_latest($limit)
    {
        $this->db->where('item_saleend >=', 'DATE(NOW())', false);
        $this->db->where('item_salestart <=', 'DATE(NOW())', false);
        $this->db->limit($limit);
        $query = $this->db->get('item');

        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function item_category_list($id)
    {
        $this->db->where('item_id', $id);
        $query = $this->db->get('item_category');

        return $query->num_rows() > 0 ? $query->result_array() : false;
    }

    public function item_image_by_id($id)
    {
        $this->db->where('ii.item_id', $id);
        $this->db->join('item_image ii', 'ii.image_id = i.image_id', 'left');
        $query = $this->db->get('image i');

        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function item_image_count($id)
    {
        $this->db->select_max('i.image_storeid');
        $this->db->where('ii.item_id', $id);
        $this->db->join('item_image ii', 'ii.image_id = i.image_id', 'left');
        $query = $this->db->get('image i');

        return $query->num_rows() > 0 ? ($query->result()[0]->image_storeid + 1) : false;
    }

    public function item_image_default($item_id, $image_id)
    {
        $this->db->select('i.image_id');
        $this->db->join('item_image ii', 'ii.image_id = i.image_id', 'left');
        $this->db->where('ii.item_id', $item_id);
        $this->db->where('i.image_featured', 1);
        $query = $this->db->get('image i');
        $def_file_id = $query->num_rows() > 0 ? $query->result()[0]->image_id : false;

        if ($def_file_id) {
            $this->db->where('image_id', $def_file_id);
            $this->db->update('image', array('image_featured' => 0));
        }

        $this->db->where('image_id', $image_id);
        $this->db->update('image', array('image_featured' => 1));

        $this->db->select('i.image_url');
        $this->db->join('item_image ii', 'ii.image_id = i.image_id', 'left');
        $this->db->where('ii.item_id', $item_id);
        $this->db->where('i.image_featured', 1);
        $query = $this->db->get('image i');
        $image_url = $query->num_rows() > 0 ? $query->result()[0]->image_url : false;

        $this->db->where('item_id',$item_id);
        return  $this->db->update('item',array('item_image'=>$image_url));

    }

    public function item_image_remove($id)
    {
        $this->db->select_max('i.image_storeid');
        $this->db->where('ii.item_id', $id);
        $this->db->join('item_image ii', 'ii.image_id = i.image_id', 'left');
        $query = $this->db->get('image i');

        return $query->num_rows() > 0 ? ($query->result()[0]->image_storeid + 1) : false;
    }

    public function item_category($parent_id = 0)
    {
        $this->db->where('category_parentid', $parent_id);
        $query = $this->db->get('category');

        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function item_delete($id)
    {

        /* category törlése */
        $this->db->select('c.category_id');
        $this->db->join('item i', 'i.item_slug = c.category_slug', 'left');
        $query = $this->db->get('category c');

        $route_id = $query->num_rows() > 0 ? $query->result()[0]->category_id : false;

        if ($route_id) {
            $this->db->where('category_id', $route_id);
            $this->db->delete('category');
        }

        /* Képek törlése */
        $this->db->select('i.image_id');
        $this->db->join('item_image ii', 'ii.image_id = i.image_id', 'left');
        $this->db->where('ii.item_id', $id);
        $query = $this->db->get('image i');

        $delete_images = $query->num_rows() > 0 ? $query->result() : false;

        if ($delete_images) {
            foreach ($delete_images as $delete_image) {
                $this->db->where('image_id', $delete_image->image_id);
                $this->db->delete('image');
            }
        }

        $this->db->where('item_id', $id);
        $this->db->delete('item_image');

        /* Kategóriák törlése*/
        $this->db->where('item_id', $id);
        $this->db->delete('item_category');

        /* Item törlése */
        $this->db->where('item_id', $id);

        return $this->db->delete('item');
    }

    public function item_id_by_sku($sku)
    {
        $this->db->where('UPPER(item_sku)', strtoupper($sku));
        $query = $this->db->get('item');

        return $query->num_rows() > 0 ? $query->result()[0]->item_id : false;
    }

    public function item_by_sku($sku)
    {
        $this->db->where('item_sku', $sku);
        $query = $this->db->get('item');

        return $query->num_rows() > 0 ? $query->result()[0] : false;
    }

    public function item_by_cat($slug) {
        $this->db->where('slug',$slug);
        $this->db->or_where('slug2',$slug);
        $this->db->or_where('slug3',$slug);
        $this->db->or_where('slug4',$slug);
        $this->db->or_where('slug5',$slug);
        $this->db->or_where('slug6',$slug);
        $query = $this->db->get('category_import');
        return $query->num_rows() > 0 ? $query->result() : false;
    }
}
