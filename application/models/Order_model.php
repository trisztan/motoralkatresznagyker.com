<?php

class Order_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function order_all_cnt()
    {
        $this->db->join('order_template', 'order_template.ordertemplate_id = order.status_id');
        $query = $this->db->get('order');

        return $query->num_rows();
    }

    public function order_fil_cnt($order, $orderby, $columns, $search)
    {
        $this->db->join('order_template', 'order_template.ordertemplate_id = order.status_id');
        if (!empty($search)) {
            foreach ($columns as $column) {
                $this->db->or_like($column, $search);
            }
        }
        $query = $this->db->get('order');

        return $query->num_rows();
    }

    public function order_datatable($limit, $offset, $order, $orderby, $columns, $search)
    {
        $this->db->join('order_template', 'order_template.ordertemplate_id = order.status_id');
        if (!empty($search)) {
            foreach ($columns as $column) {
                $this->db->or_like("order.".$column, $search);
            }
        }

        $this->db->limit($limit, $offset);
        $this->db->order_by($order, $orderby);
        $query = $this->db->get('order');

        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function order_all()
    {
        $this->db->join('order_template ot', 'ot.ordertemplate_id = o.status_id', 'left');
        $query = $this->db->get('order o');

        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function order_add($model)
    {
        return $this->db->insert('order', $model);
    }

    public function order_get($order_id)
    {
        $this->db->select('o.*,
                            ot.ordertemplate_id,
                            ot.ordertemplate_color,
                            ot.ordertemplate_name,
                            os.ship_name,ordertemplate_name,
                            os.ship_price,');
        $this->db->where('o.order_id', $order_id);
        $this->db->join('order_template ot', 'ot.ordertemplate_id = o.status_id', 'left');
        $this->db->join('order_ship os', 'os.order_id = o.order_id', 'left');
        $query = $this->db->get('order o');

        return $query->num_rows() > 0 ? $query->result()[0] : false;
    }


    public function order_check_sku($sku)
    {
        $this->db->where('item_sku', $sku);
        $query = $this->db->get('item');

        return $query->num_rows() > 0 ? true : false;
    }

    public function order_update($order_id, $model)
    {
        $this->db->where('order_id', $order_id);

        return $this->db->update('order', $model);
    }

    public function order_ship_update($order_id, $model)
    {
        $this->db->where('order_id', $order_id);
        $query = $this->db->get('order_ship');
        $exist = $query->num_rows() > 0 ? true : false;

        if($exist) {
            $this->db->where('order_id', $order_id);
            $result = $this->db->update('order_ship', $model);
        } else {
            $model['order_id'] = $order_id;
            $result = $this->db->insert('order_ship', $model);
        }

        return $result;
    }

    public function order_log_update($order_id, $comment)
    {
        $model['order_id'] = $order_id;
        $model['order_text'] = $comment;
        $model['order_date'] = date('Y-m-d H:i:s');
        return $this->db->insert('order_log', $model);
    }

    public function order_delete($order_id)
    {
        $this->db->where('order_id', $order_id);

        return $this->db->delete('order');
    }

    public function order_item_delete($order_id, $item_sku)
    {
        $this->db->where('item_sku', $item_sku);
        $this->db->where('order_id', $order_id);
        return $this->db->delete('order_item');
    }

    public function order_item_add($model)
    {
        return $this->db->insert('order_item',$model);
    }

}
