<?php defined('BASEPATH') OR exit('No direct script access allowed');
class checkuser {

	var $CI;

	function __construct() {
		$this->CI = &get_instance();
		$this->CI->load->library('session');

		set_lang();
	}

	function checkuser() {
		$slug = $this->CI->uri->segment(1);
		$user = $this->CI->session->userdata('user');

		$this->CI->db->where('router_slug',$slug);
		
		$query = $this->CI->db->get('router');
		
		if($query->num_rows() == 1 && !empty($slug)) {
			$res = $query->result()[0];
			if($res->router_locked == 1) {
				if(empty($user)) {
					redirect('admin-login');
				}
			}
		}
	}
}
?>