<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Enqueue {
	public $scripts;
	public $styles;
	
	public function __construct() {
		$this->scripts = [];
		$this->styles = [];
	}
	
	public function enqueue_js($script) {

		$this->scripts[] = (string)$script;
	}
	
	public function enqueue_css($style) {
		$this->styles[] = (string)$style;
	}
	
	public function loadjs() {
		if(!empty($this->scripts)){
			$this->scripts = array_unique($this->scripts);
			foreach( $this->scripts as $script ) {
				$script = (strpos($script, '://') !== false) ? $script : base_url($script);
				echo '<script src="' . $script . '"></script>'."\n";
			}
		}
	}
	
	public function loadcss() {
		if(!empty($this->styles)){
			$this->styles = array_unique($this->styles);
			foreach( $this->styles as $style ) {
				$style = (strpos($style, '://') !== false) ? $style : base_url($style);
				echo '<link rel="stylesheet" href="' . $style . '" />'."\n";
			}
		}
	}
}