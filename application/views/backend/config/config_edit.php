<div class="modal fade" data-config-url="<?php echo base_url('config-edit'); ?>" id="configmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Konfig szerkesztés</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-config">
                    <div class="form-group">
                        <input type="hidden" name="config_id" value="<?php echo $config_id;?>">
                        <label for="config_value" class="col-sm-4 control-label">Érték</label>
                        <div class="col-sm-6">
                            <textarea style="height:100%; widht: 300px;" class="form-control" name="config_value" id="config_value"><?php echo htmlspecialchars($config_value);?></textarea>
                            <?php if(!empty($config_comment)):?>
                            <br/>
                            <p style="color:red"><?php echo $config_comment; ?></textarea></p>
                            <?php endif;?>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-config-save class="btn btn-primary">Mentés</button>
            </div>
        </div>
    </div>
</div>
