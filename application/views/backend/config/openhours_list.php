<input data-openhours-save-url type="hidden" value="<?php echo base_url('openhours-save');?>">
<div data-openhours-data style="display:none"><?php echo $openhours; ?></div>
<div class="row">
    <div class="col-xs-12">
        <div data-modal></div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Nyitvatartás</h3>
                <button type="button" data-openhours-save class="btn btn-primary pull-right"><i class="fa fa-save"></i> Mentés</button>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div id="businessHoursWidget"></div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
