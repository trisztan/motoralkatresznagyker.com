<div class="modal fade" data-user-url="<?php echo base_url('user-add'); ?>" id="usermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Felhasználó hozzáadás</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-user">
                    <div class="form-group">
                        <label for="user_lastname" class="col-sm-4 control-label">Vezetéknév</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="user_lastname" id="user_lastname" placeholder="Vezetéknév">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_firstname" class="col-sm-4 control-label">Keresztnév</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="user_firstname" id="user_firstname" placeholder="Keresztnév">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_nickname" class="col-sm-4 control-label">Felhasználó név</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="user_nickname" id="user_nickname" placeholder="Keresztnév">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_email" class="col-sm-4 control-label">Email cím</label>
                        <div class="col-sm-6">
                            <input type="email" class="form-control" name="user_email" id="user_email" placeholder="E-mail cím">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_password" class="col-sm-4 control-label">Jelszó</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="user_password" id="user_password" placeholder="Jelszó">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_email" class="col-sm-4 control-label">Felhasználó típus</label>
                        <div class="col-sm-6">
                              <select class="form-control" name="user_type" id="user_type" data-user-type-select>
                                <option value="ADMIN">Admin</option>
                                <option value="USER">Felhasználó</option>
                              </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_status" class="col-sm-4 control-label">Felhasználó státusz</label>
                        <div class="col-sm-6">
                              <select class="form-control" name="user_status" id="user_status" data-user-type-select>
                                <option value="1">Aktív</option>
                                <option value="0">Nem aktív</option>
                              </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-user-save class="btn btn-primary">Mentés</button>
            </div>
        </div>
    </div>
</div>