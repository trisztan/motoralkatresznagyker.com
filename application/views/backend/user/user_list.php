<input type="hidden" data-user-modal-url value="<?php echo base_url('user-modal'); ?>">
<div class="row">
    <div class="col-xs-12">
        <div data-modal></div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Felhasználók</h3>
                <button class="btn btn-info pull-right" role="button" data-user-add><i class="fa fa-plus"></i> Felhasználó hozzáadás</button>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="user_list" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Keresztnév</th>
                            <th>Vezetéknév</th>
                            <th>Felhasználónév</th>
                            <th>E-mail</th>
                            <th>Rang</th>
                            <th>Státusz</th>
                            <th>Műveletek</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $users; ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->