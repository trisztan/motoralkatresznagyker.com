<div class="modal fade" data-user-url="<?php echo base_url('user-edit'); ?>" id="usermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Kupon szerkesztés</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-user">
                    <div class="form-group">
                        <input type="hidden" name="user_id" value="<?php echo $user_id;?>">
                        <input type="hidden" name="user_nickname_original" value="<?php echo $user_nickname;?>">
                        <input type="hidden" name="user_email_original" value="<?php echo $user_email;?>">
                        <label for="user_name" class="col-sm-4 control-label">Vezetéknév</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="user_lastname" value="<?php echo $user_lastname;?>" id="user_lastname" placeholder="Vezetéknév">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_code" class="col-sm-4 control-label">Keresztnév</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="user_firstname" value="<?php echo $user_firstname;?>" id="user_firstname" placeholder="Keresztnév">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_nickname" class="col-sm-4 control-label">Felhasználó név</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="user_nickname"  value="<?php echo $user_nickname;?>" id="user_nickname" placeholder="Keresztnév">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_email" class="col-sm-4 control-label">E-mail cím</label>
                        <div class="col-sm-6">
                            <input type="email" class="form-control" name="user_email" value="<?php echo $user_email;?>" id="user_email" placeholder="E-mail cím">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_password" class="col-sm-4 control-label">Jelszó</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="user_password" id="user_password" placeholder="Jelszó">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_email" class="col-sm-4 control-label">Felhasználó típus</label>
                        <div class="col-sm-6">
                              <select class="form-control" name="user_type" id="user_type" data-user-type-select>
                                <option value="ADMIN" <?php if($user_type == "ADMIN"):?>selected<?php endif;?>>Admin</option>
                                <option value="USER" <?php if($user_type == "USER"):?>selected<?php endif;?>>Felhasználó</option>
                              </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_status" class="col-sm-4 control-label">Felhasználó státusz</label>
                        <div class="col-sm-6">
                              <select class="form-control" name="user_status" id="user_status" data-user-type-select>
                                <option value="1" <?php if($user_status == 1):?>selected<?php endif;?>>Aktív</option>
                                <option value="0" <?php if($user_status == 0):?>selected<?php endif;?>>Nem aktív</option>
                              </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-user-save class="btn btn-primary">Mentés</button>
            </div>
        </div>
    </div>
</div>