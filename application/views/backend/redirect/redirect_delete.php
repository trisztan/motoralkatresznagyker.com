<div class="modal fade" data-redirect-url="<?php echo base_url('redirect-delete'); ?>" id="redirectmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Átirányítás törlése</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-redirect">
                    <input type="hidden" name="redirect_id" value="<?php echo $redirect_id;?>">
                </form>
                Valóban törölni szeretnéd az átirányítást?
            </div>
            <div class="modal-footer">
                <button type="button" data-redirect-save class="btn btn-primary">Igen</button>
            </div>
        </div>
    </div>
</div>
