<div class="modal fade" data-redirect-url="<?php echo base_url('redirect-edit'); ?>" id="redirectmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Átirányítás szerkesztés</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-redirect">
                    <div class="form-group">
                        <input type="hidden" name="redirect_id" value="<?php echo $redirect_id;?>">
                        <label for="redirect_from" class="col-sm-4 control-label">Miről</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="redirect_from" value="<?php echo $redirect_from;?>" id="redirect_from" placeholder="Miről">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="redirect_to" class="col-sm-4 control-label">Mire</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="redirect_to" value="<?php echo $redirect_to;?>"id="redirect_to" placeholder="Mire">
                        </div>
                    </div>
                    <input type="hidden" name="redirect_type" value="301">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-redirect-save class="btn btn-primary">Mentés</button>
            </div>
        </div>
    </div>
</div>
