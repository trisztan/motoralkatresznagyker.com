<input type="hidden" data-redirect-modal-url value="<?php echo base_url('redirect-modal'); ?>">
<input type="hidden" data-redirect-datatable-url value="<?php echo base_url('redirect-datatable'); ?>">
<div class="row">
    <div class="col-xs-12">
        <div data-modal></div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Átirányítások</h3>
                <button class="btn btn-info pull-right" role="button" data-redirect-add><i class="fa fa-plus"></i> Átirányítás hozzáadás</button>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="redirect" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Miről</th>
                            <th>Mire</th>
                            <th>Átirányitás kódja</th>
                            <th>Műveletek</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
