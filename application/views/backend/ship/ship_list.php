<input type="hidden" data-ship-modal-url value="<?php echo base_url('ship-modal'); ?>">
<div data-modal></div>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Szállítási módok</h3>
                <button class="btn btn-info pull-right" role="button" data-ship-modal-add="0"><i class="fa fa-plus"></i> Szállító hozzáadás</button>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="ship_list" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Szállítás név</th>
                            <th>Szállítási érték</th>
                            <th>Szállítási ár</th>
                            <th>Műveletek</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $ship; ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
