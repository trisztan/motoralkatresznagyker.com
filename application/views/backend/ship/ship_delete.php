<div class="modal fade" data-ship-url="<?php echo base_url('ship-delete'); ?>" id="shipmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel"><?php if($ship_parentid == 0):?>Szállítási osztály törlése<?php else:?> Szállítás típus törlése<?php endif;?></h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-ship">
                    <input type="hidden" name="ship_parentid" value="<?php echo $ship_parentid;?>">
                    <input type="hidden" name="ship_id" value="<?php echo $ship_id;?>">
                </form>
                <?php if($ship_parentid == 0):?>
                    Valóban törölni szeretnéd a szállítási osztályt és a hozzátartozó szállítás típusokat?
                <?php else:?>
                    Valóban törölni szeretnéd a szállítás típust?
                <?php endif;?>
            </div>
            <div class="modal-footer">
                <button type="button" data-ship-save class="btn btn-primary">Igen</button>
            </div>
        </div>
    </div>
</div>
