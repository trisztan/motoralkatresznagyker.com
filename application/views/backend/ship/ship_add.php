<div class="modal fade" data-ship-url="<?php echo base_url('ship-add'); ?>" id="shipmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel"><?php if($ship_parentid == 0): ?>Szállítási osztály hozzáadása<?php else: ?>Szállítás hozzáadása<?php endif; ?></h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-ship">

                    <?php if(!isset($ship_id) && ($ship_parentid == 0)): ?>
                    <input type="hidden" name="ship_parentid" value="0">
                    <div class="form-group">
                        <label for="ship_name" class="col-sm-4 control-label">Név</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="ship_name" id="ship_name" placeholder="Szállítási osztály neve">
                        </div>
                    </div>
                    <?php else: ?>
                    <input type="hidden" name="ship_parentid" id="ship_parentid" value="<?php echo $ship_id; ?>">
                    <input type="hidden" name="ship_name" id="ship_name" value="<?php echo $ship_name; ?>">
                    <div class="form-group">
                        <label for="ship_valuefrom" class="col-sm-4 control-label">Súly intervallum</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-cube"></i></span>
                                <input type="number" class="form-control" name="ship_valuefrom" id="ship_valuefrom" placeholder="Mettől" step="0.01">
                                <input type="number" class="form-control" name="ship_valueto" id="ship_valueto" placeholder="Meddig" step="0.01">
                                <span class="input-group-addon"> <b>kg</b>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ship_price" class="col-sm-4 control-label">Ár</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                <input type="number" class="form-control" name="ship_price" id="ship_price" placeholder="Ár">
                                <span class="input-group-addon"> <b>Ft</b> </span>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-ship-save class="btn btn-primary">Mentés</button>
            </div>
        </div>
    </div>
</div>
