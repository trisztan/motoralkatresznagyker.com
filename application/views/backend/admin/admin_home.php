<input type="hidden" data-sales-chart value="<?php echo base_url('sales-chart'); ?>">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo offer_count_new(); ?></h3>
                        <p>Új ajánlatkérések</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <a href="<?php echo base_url('offer-list'); ?>" class="small-box-footer">Összes ajánlat <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo order_count_new(); ?></h3>
                        <p>Új rendelések</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="<?php echo base_url('order-list'); ?>" class="small-box-footer">Összes rendelés <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?php echo product_count_all(); ?></h3>
                        <p>Összes termék</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-cubes"></i>
                    </div>
                    <a href="<?php echo base_url('item-list'); ?>" class="small-box-footer">Összes termékek <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?php echo category_count_all(); ?></h3>
                        <p>Összes kategória</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </div>
                    <a href="<?php echo base_url('category-list'); ?>" class="small-box-footer">Összes vásárló <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?php echo buyer_count_all(); ?></h3>
                        <p>Összes vásárló</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="<?php echo base_url('order-list'); ?>" class="small-box-footer">Összes vásárló <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <!-- ./col -->
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?php echo admin_count_all(); ?></h3>
                        <p>Összes admin</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="<?php echo base_url('user-list'); ?>" class="small-box-footer">Összes admin <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12">
                <!-- Custom tabs (Charts with tabs)-->
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-right">
                        <li style="padding:5px"><select dashboard-stat><option value="day">Utolsó 7 nap</option>
                                                  <option value="month">Hónap</option>
                                                  <option value="year">Év</option>
                                                </select>
                        </li>
                        <li class="pull-left header"><i class="fa fa-money"></i> Rendelések</li>
                    </ul>
                    <div class="tab-content no-padding">
                        <!-- Morris chart - Sales -->
                        <div class="chart tab-pane active" id="week-sales-stat" style="position: relative; height: 300px;">
                        </div>
                    </div>
                </div>
                <!-- /.box -->
                <!-- /.nav-tabs-custom -->
            </section>
            <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
