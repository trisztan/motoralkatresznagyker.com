<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo title(); ?> </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url('template/backend/bootstrap/css/bootstrap.min.css'); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('template/backend/dist/css/AdminLTE.min.css'); ?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url('template/backend/plugins/iCheck/square/blue.css'); ?>">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="<?php echo base_url();?>"><img src="<?php echo base_url('template/backend/dist/img/logo.png'); ?>" alt="Carat HTML Template"></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Bejelentkezés</p>
        <?php echo form_open($url);?>
          <div class="form-group <?php if(isset($error['email'])):?>has-error<?php endif;?> has-feedback">
            <?php if(isset($error['email'])):?>
             <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> <?php echo $error['email']; ?></label>
            <?php endif;?>
            <input type="email" name="email" <?php if(isset($error['email'])):?>id="inputError"<?php endif;?> class="form-control" placeholder="E-mail">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group <?php if(isset($error['pass'])):?>has-error<?php endif;?> has-feedback">
             <?php if(isset($error['pass'])):?>
             <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> <?php echo $error['pass']; ?></label>
            <?php endif;?>
            <input type="password" name="pass" <?php if(isset($error['email'])):?>id="inputError"<?php endif;?> class="form-control" placeholder="Jelszó">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
         <div class="row">
             <!--<div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Megjegyzés
                </label>
              </div>
            </div>--><!-- /.col -->
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-user"></i> Belépés</button>
            </div><!-- /.col -->
          </div>
        </form>
        <!--<div class="text-center">
        <p>- vagy -</p>
        <a href="<?php/* echo base_url('admin/password');*/?>">Elfelejtett jelszó</a><br>
        </div>-->
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('template/backend/plugins/jQuery/jQuery-2.1.4.min.js');?>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url('template/backend/bootstrap/js/bootstrap.min.js');?>"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url('template/backend/plugins/iCheck/icheck.min.js');?>"></script>
    <?php $this->enqueue->loadjs(); ?>
  </body>
</html>
