<div class="modal modal-fullscreen modal-transparent fade" data-order-url="<?php echo base_url('order-add'); ?>" id="ordermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Rendelés hozzáadás</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-order">
                    <div class="form-group">
                        <label for="order_fullname" class="col-sm-2 control-label">Név</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_fullname" id="order_fullname" placeholder="Keresztnév">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_company" class="col-sm-2 control-label">Cég</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_company" id="order_company" placeholder="Cég">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_taxnumber" class="col-sm-2 control-label">Adószám</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_taxnumber" id="order_taxnumber" placeholder="Adószám">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_email" class="col-sm-2 control-label">E-mail</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_email" id="order_email" placeholder="E-mail cím">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_mobile" class="col-sm-2 control-label">Telefon</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_mobile" id="order_mobile" placeholder="Telefonszám">
                        </div>
                    </div>
                    <h4 class="modal-title">Szállítási adatok</h4>
                    <div class="divider"></div>
                    <div class="form-group">
                        <label for="order_state" class="col-sm-2 control-label">Megye</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_state" id="order_state" placeholder="Megye">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_city" class="col-sm-2 control-label">Város</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_city" id="order_city" placeholder="Város">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_street" class="col-sm-2 control-label">Utca/házszám/emelet/ajtó</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_street" id="order_street" placeholder="Cím (utca/házszám/emelet/ajtó)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_zip" class="col-sm-2 control-label">Irányítószám</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_zip" id="order_zip" placeholder="Irányítószám">
                        </div>
                    </div>
                    <h4 class="modal-title">Számlázási adatok</h4>
                    <div class="divider"></div>
                    <div class="form-group">
                        <label for="order_invstate" class="col-sm-2 control-label">Megye</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_invstate" id="order_invstate" placeholder="Megye">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_invcity" class="col-sm-2 control-label">Város</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_invcity" id="order_invcity" placeholder="Város">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_invstreet" class="col-sm-2 control-label">Utca/házszám/emelet/ajtó</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_invstreet" id="order_invstreet" placeholder="Cím (utca/házszám/emelet/ajtó)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order_invzip" class="col-sm-2 control-label">Irányítószám</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="order_invzip" id="order_invzip" placeholder="Irányítószám">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-order-save class="btn btn-primary">Mentés</button>
            </div>
        </div>
    </div>
</div>
