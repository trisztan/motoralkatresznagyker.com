<input type="hidden" data-order-modal-url value="<?php echo base_url('order-modal'); ?>">
<input type="hidden" data-order-datatable-url value="<?php echo base_url('order-datatable'); ?>">

<div class="row">
    <div class="col-xs-12">
        <div data-modal></div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Rendelések</h3>
                <button class="btn btn-info pull-right" role="button" data-order-add><i class="fa fa-plus"></i> Rendelés hozzáadás</button>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="order" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="widht: 20px;">Id</th>
                            <th>Megrendelő</th>
                            <th>Szállítási cím</th>
                            <th>Számlázási cím</th>
                            <th>Rendelés állapot</th>
                            <th>Megrendelve</th>
                            <th>Felíratkozott</th>
                            <th>Műveletek</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
