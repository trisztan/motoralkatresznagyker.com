<div class="row">
	<input type="hidden" data-order-id value="<?php echo $order->order_id; ?>">
	<input type="hidden" data-order-save-url value="<?php echo base_url('order-edit-save'); ?>">
	<input type="hidden" data-order-item-add-url value="<?php echo base_url('order-edit-item-add'); ?>">
	<input type="hidden" data-order-item-delete-url value="<?php echo base_url('order-edit-item-delete'); ?>">

	<div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<strong>Megrendelés: #<?php echo $order->order_id; ?></strong>
				<div class="pull-right"><a href="<?php echo base_url('order-list') ?>" class="btn btn-default" style="margin-left: 30px;"><i class="fa fa-arrow-left"></i> Vissza</a></div>
				<div class="pull-right"><button type="button" data-order-save class="btn btn-success"><i class="fa fa-save"></i> Mentés</button></div>
				<div class="pull-right"> <input type="checkbox" data-order-alert> Megrendelő értesítése&nbsp;&nbsp;&nbsp;</div>
			</div>
			<div class="box-body">
        <form class='form-horizontal' id="form-order">
            <input type="hidden" data-order-id value="<?php echo $order->order_id; ?>">
			<div class="alert" style="background: <?php echo $order->ordertemplate_color;?>; color: white;">
			  <h4>Megrendelés állapota: <?php echo $order->ordertemplate_name;?></h4>
			</div>
					<div class="panel panel-default">
						<div class="panel-heading">Megrendelés adatai</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="status_id" class="col-sm-4 control-label">Rendelés státusz</label>
										<div class="col-sm-8">
											<select class="form-control" name="status_id" id="status_id">
												 <?php foreach($order_template as $template):?>
												 <option value="<?php echo $template->ordertemplate_id;?>" <?php if($template->ordertemplate_id == $order->status_id): ?> selected <?php endif; ?>><?php echo $template->ordertemplate_name;?></option>
												<?php endforeach;?>
											 </select>
										</div>
									</div>
									<div class="form-group">
										<label for="email_comment" class="col-sm-4 control-label">E-mail üzenet</label>
										<div class="col-sm-8">
											<textarea name="email_comment" style="width:100%; height: 200px;"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label for="order_comment" class="col-sm-4 control-label">Vásárló megjegyzés</label>
										<div class="col-sm-8">
											<textarea disabled style="width:100%; height: 200px;"><?php echo $order->order_comment; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label for="ship_name" class="col-sm-4 control-label">Futár</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="ship_name" id="ship_name" placeholder="Futár" value="<?php echo $order->ship_name; ?>">
										</div>
									</div>
									<div class="form-group">
										<label for="ship_price" class="col-sm-4 control-label">Szállítási ár</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="ship_price" id="ship_price" placeholder="Szállítási ár" value="<?php echo $order->ship_price; ?>">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel panel-default">
										<div class="panel-heading">Megrendelt termékek</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-sm-4"> <input type="text" class="form-control" id="item_sku" data-item-add-sku placeholder="Sku"> </div>
												<div class="col-sm-2"> <input type="number" class="form-control" id="item_qty" data-item-add-db placeholder="Db"> </div>
												<div class="col-sm-4"> <input type="number" class="form-control"  id="item_price" data-item-add-price placeholder="Ár"> </div>
												<div class="col-sm-2"> <button type="button" data-order-item-add class="btn btn-success"><i class="fa fa-plus"></i></button> </div>
											</div>
		                                    <table class="table table-striped">
		                                        <tbody>
		                                        <?php $total_amount = ""; ?>
		                                        <?php foreach($order_items as $order_item): ?>
		                                        <?php $price = $order_item->item_price; ?>
		                                                <tr>
		                                                    <td align="left"><?php echo $order_item->item_name; ?></td>
															<td align="left"><?php echo $order_item->item_sku; ?></td>
		                                                    <td align="left"><?php echo $order_item->item_qty; ?> db</td>
		                                                    <td align="left"><?php echo number_format($price,0,'','.');?> Ft</td>
		                                                    <td align="left"><?php echo number_format(($price*$order_item->item_qty),0,'','.');?> Ft</td>
															<td align="left"><button type="button" data-order-item-delete="<?php echo $order_item->item_sku; ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></td>
		                                                </tr>
		                                                <?php
		                                                    $total_amount = $total_amount + ($price * $order_item->item_qty);
		                                                ?>
		                                        <?php endforeach ?>
		                                        <?php $total_amount = $total_amount + $order->ship_price; ?>
		                                            <tr>
		                                                <td align="left"></td>
		                                                <td align="left"></td>
														<td align="left"></td>
														<td align="left"></td>
		                                                <td align="left"><b>Szállítási költség:</b></td>
		                                                <td align="left"><?php echo number_format($order->ship_price,0,'','.');?> Ft</td>
		                                            </tr>
		                                            <tr>
		                                                <td align="left"></td>
		                                                <td align="left"></td>
														<td align="left"></td>
														<td align="left"></td>
		                                                <td align="left"><b>Fizetendő összesen:</b></td>
		                                                <td align="left"><?php echo number_format($total_amount,0,'','.');?> Ft</td>
		                                            </tr>
		                                        </tbody>
		                                    </table>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading">Előzmények</div>
										<div class="panel-body">
											<?php if(!empty($order_log)): ?>
											<?php foreach($order_log as $order_lo): ?>
											<div class="direct-chat-msg">
						                      <div class="direct-chat-info clearfix">
						                        <span class="direct-chat-name pull-left"></span>
						                        <span class="direct-chat-timestamp pull-right"><?php echo $order_lo->order_date; ?></span>
						                      </div>
						                      <div class="direct-chat-text ">
						                        <?php echo $order_lo->order_text; ?>
						                      </div>
						                      <!-- /.direct-chat-text -->
						                    </div>
											<?php endforeach;?>
											<?php else:?>
											<div class="alert alert-info" role="alert">Nincs rendelési előzmény</div>
											<?php endif;?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			<div class="panel panel-default">
			  	<div class="panel-heading">Számlázási adatok</div>
			  	<div class="panel-body">
					<div class="form-group">
						<label for="order_invlname" class="col-sm-2 control-label">Vezetéknév</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="order_invlname" id="order_invlname" placeholder="Vezetéknév" value="<?php echo $order->order_invlname; ?>">
						</div>
						<label for="order_invfname" class="col-sm-2 control-label">Keresztnév</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="order_invfname" id="order_invfname" placeholder="Keresztnév" value="<?php echo $order->order_invfname; ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="order_invcompany" class="col-sm-2 control-label">Cég</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="order_invcompany" id="order_invcompany" placeholder="Cég" value="<?php echo $order->order_invcompany; ?>">
						</div>
						<label for="order_taxnumber" class="col-sm-2 control-label">Adószám</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="order_taxnumber" id="order_taxnumber" placeholder="Adószám" value="<?php echo $order->order_taxnumber; ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="order_invcity" class="col-sm-2 control-label">Város</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="order_invcity" id="order_invcity" placeholder="Város" value="<?php echo $order->order_invcity; ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="order_invstreet" class="col-sm-2 control-label">Utca/házszám/emelet/ajtó</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="order_invstreet" id="order_invstreet" placeholder="Cím (utca/házszám/emelet/ajtó)" value="<?php echo $order->order_invstreet; ?>">
						</div>
						<label for="order_invzip" class="col-sm-2 control-label">Irányítószám</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="order_invzip" id="order_invzip" placeholder="Irányítószám" value="<?php echo $order->order_invzip; ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="order_email" class="col-sm-2 control-label">E-mail</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="order_email" id="order_email" placeholder="E-mail cím" value="<?php echo $order->order_email; ?>">
						</div>
						<label for="order_invmobile" class="col-sm-2 control-label">Telefon</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="order_invmobile" id="order_invmobile" placeholder="Telefonszám" value="<?php echo $order->order_invmobile; ?>">
						</div>
					</div>
				</div>
			</div>
					<div class="panel panel-default">
						<div class="panel-heading">Szállítási adatok</div>
						<div class="panel-body">
						<div class="form-group">
							<label for="order_lname" class="col-sm-2 control-label">Vezetéknév</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="order_lname" id="order_lname" placeholder="Vezetéknév" value="<?php echo $order->order_lname; ?>">
							</div>
							<label for="order_fname" class="col-sm-2 control-label">Keresztnév</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="order_fname" id="order_fname" placeholder="Keresztnév" value="<?php echo $order->order_fname; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="order_company" class="col-sm-2 control-label">Cég</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="order_company" id="order_company" placeholder="Cég" value="<?php echo $order->order_company; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="order_city" class="col-sm-2 control-label">Város</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="order_city" id="order_city" placeholder="Város" value="<?php echo $order->order_city; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="order_street" class="col-sm-2 control-label">Utca/házszám/emelet/ajtó</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="order_street" id="order_street" placeholder="Cím (utca/házszám/emelet/ajtó)" value="<?php echo $order->order_street; ?>">
							</div>
							<label for="order_zip" class="col-sm-2 control-label">Irányítószám</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="order_zip" id="order_zip" placeholder="Irányítószám" value="<?php echo $order->order_zip; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="order_mobile" class="col-sm-2 control-label">Telefon</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="order_mobile" id="order_mobile" placeholder="Telefonszám" value="<?php echo $order->order_mobile; ?>">
							</div>
						</div>
					</div>
				</div>
			</div>
        </form>
	</div>
</div>
	</div><!-- /.col -->
</div><!-- /.row -->
