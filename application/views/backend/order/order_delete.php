<div class="modal fade" data-order-url="<?php echo base_url('order-delete'); ?>" id="ordermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Rendelés törlés</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-order">
                    <input type="hidden" name="order_id" value="<?php echo $order_id;?>">
                </form>
                Valóban törölni szeretnéd a rendelést?
            </div>
            <div class="modal-footer">
                <button type="button" data-order-save class="btn btn-primary">Igen</button>
            </div>
        </div>
    </div>
</div>
