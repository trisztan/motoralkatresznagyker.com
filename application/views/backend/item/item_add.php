<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#data" data-toggle="tab">Termék adatok</a></li>
			</ul>
			<div class="tab-content">
				<div class="active tab-pane" id="data">

					<?php echo form_open($url, array('class' => 'form-horizontal')); ?>
						<div class="form-group">
							<label for="inputProductName" class="col-sm-2 control-label">Termék név</label>
							<div class="col-sm-6">
								<input type="text" class="form-control <?php if (isset($error['item_name'])): ?> red-border<?php endif;?>" name="item_name" id="inputProductName" value="<?php echo isset($product['item_name']) ? $product['item_name'] : ''; ?>" placeholder="<?php if (isset($error['item_name'])): ?> <?php echo $error['item_name']; ?><?php else: ?>Termék név<?php endif;?>">
							</div>
						</div>
						<div class="form-group">
							<label for="inputProductUrl" class="col-sm-2 control-label">Termék Url</label>
							<div class="col-sm-6">
								<input type="text" class="form-control <?php if (isset($error['item_slug'])): ?> red-border<?php endif;?>" name="item_slug" id="inputProductUrl" value="<?php if (!isset($error['item_slug'])): ?><?php echo isset($product['item_slug']) ? $product['item_slug'] : ''; ?><?php endif;?>" placeholder="<?php if (isset($error['item_slug'])): ?> <?php echo $error['item_slug']; ?><?php else: ?>Termék url<?php endif;?>">
							</div>
							<div class="col-sm-4">
								<button type="button" data-slug-generate  class="btn btn-info" role="button"><i class="fa fa-link"></i></button>
							</div>
						</div>
						<div class="form-group">
							<label for="inputProductSku" class="col-sm-2 control-label">SKU</label>
							<div class="col-sm-6">
								<input type="text" class="form-control <?php if (isset($error['item_sku'])): ?> red-border<?php endif;?>" name="item_sku" id="inputProductSku" value="<?php echo isset($product['item_sku']) ? $product['item_sku'] : ''; ?>" placeholder="<?php if (isset($error['item_sku'])): ?> <?php echo $error['item_sku']; ?><?php else: ?>Termék sku<?php endif;?>">
							</div>
							<div class="col-sm-4">
								<button type="button" data-sku-generate class="btn btn-info" role="button"><i class="fa fa-barcode"></i></button>
							</div>
						</div>
						<div class="form-group">
							<label for="inputProductTax" class="col-sm-2 control-label">Termék áfa</label>
							<div class="col-sm-2">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-percent"></i></span>
									<input type="number" class="form-control <?php if (isset($error['item_tax'])): ?> red-border<?php endif;?>" name="item_tax" id="inputProductTax" value="<?php echo isset($product['item_tax']) ? $product['item_tax'] : '27'; ?>" placeholder="<?php if (isset($error['item_tax'])): ?> <?php echo $error['item_tax']; ?><?php else: ?>Termék áfa<?php endif;?>">
									<span class="input-group-addon"> <b>%</b> </span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="inputProductWeight" class="col-sm-2 control-label">Termék súly</label>
							<div class="col-sm-2">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-cube"></i></span>
									<input type="number" class="form-control <?php if (isset($error['item_weight'])): ?> red-border<?php endif;?>" name="item_weight" id="inputProductWeight" value="<?php echo isset($product['item_weight']) ? $product['item_weight'] : ''; ?>" placeholder="<?php if (isset($error['item_weight'])): ?> <?php echo $error['item_weight']; ?><?php else: ?>Termék súly<?php endif;?>" step="0.01">
									<span class="input-group-addon"> <b>kg</b> </span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="inputProductDetails" class="col-sm-2 control-label">Termék leírás</label>
							<div class="col-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-list"></i></span>
									<textarea id="item_longdesc" class="form-control" name="item_longdesc" rows="10" cols="80"><?php echo isset($product['item_longdesc']) ? $product['item_longdesc'] : ''; ?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="inputProductPrice" class="col-sm-2 control-label">Termék ár</label>
							<div class="col-sm-2">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-usd"></i></span>
									<input type="number" class="form-control" name="item_netprice" id="inputProductPrice" value="<?php echo isset($product['item_netprice']) ? $product['item_netprice'] : ''; ?>" placeholder="Normál ár">
									<span class="input-group-addon"> <b>Ft</b> </span>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-usd"></i></span>
									<input type="number" class="form-control" name="item_netpricesale" value="<?php echo isset($product['item_netpricesale']) ? $product['item_netpricesale'] : ''; ?>"placeholder="Akciós ár">
									<span class="input-group-addon"> <b>Ft</b> </span>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i> Akció </span>
									<input type="text" class="form-control pull-right active" value="<?php echo isset($product['item_saledate']) ? $product['item_saledate']: ''; ?>" name="item_saledate" id="saledate">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="inputProductStatus" class="col-sm-2 control-label">Látható</label>
							<div class="col-sm-10">
								<div class="checkbox">
									<label>
										<input id="#inputProductStatus" name="item_status" <?php if (isset($product['item_status'])): ?>checked<?php endif;?> type="checkbox">
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-danger">Elküld</button>
							</div>
						</div>
					</form>
				</div><!-- /.tab-pane -->
			</div><!-- /.tab-content -->
		</div><!-- /.nav-tabs-custom -->
	</div><!-- /.col -->
</div><!-- /.row -->
