<input type="hidden" data-csrf value="<?php echo $this->security->get_csrf_hash(); ?>">
<input type="hidden" data-item-del-url value="<?php echo base_url('item-delete'); ?>">
<input type="hidden" data-item-modal-url value="<?php echo base_url('item-modal'); ?>">
<input type="hidden" data-item-import-upload-url value="<?php echo base_url('item-import-upload'); ?>">
<!-- Datatable URL -->
<input type="hidden" data-item-datatable-url value="<?php echo base_url('item-datatable'); ?>">

<div class="modal fade" id="itemdel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                 <input type="hidden" id="inputItemIdDel" name="item_id">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Termék törlés</h4>
            </div>
            <div class="modal-body">
                Valóban törölni szeretnéd a terméket?
            </div>
            <div class="modal-footer">
                <button type="button" data-item-del class="btn btn-success"><i class="fa fa-trash"></i> Törlés</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-xs-12">
    <div data-modal></div>
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Termékek</h3>
        <a href="<?php echo base_url('item-add'); ?>" class="btn btn-info pull-right ml-3" role="button"><i class="fa fa-plus"></i> Termék feltöltés</a>
      </div><!-- /.box-header -->
      <div class="box-body">

        <table id="item" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Id</th>
              <th>Termék név</th>
              <th>Termék sku</th>
              <th>Termék url</th>
              <th>Bolti ár (nettó / bruttó)</th>
              <th>Online ár (nettó / bruttó)</th>
              <th>Állapot</th>
              <th>Műveletek</th>
            </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div><!-- /.col -->
</div><!-- /.row -->
