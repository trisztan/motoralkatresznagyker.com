<div class="modal fade" id="itemmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Termék lista import</h4>
            </div>
            <div class="modal-body">
                <form class="dropzone" id="csvupload">
                    <div id="dropzonePreview" class="dz-default dz-message">
                      <span>Drop files here to upload</span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-coupon-save class="btn btn-primary">Feltöltés inditása</button>
            </div>
        </div>
    </div>
</div>