<div class="row">
	<input type="hidden" data-item-imageupload value="<?php echo base_url('item-image-upload'); ?>">
	<input type="hidden" data-item-imageremove value="<?php echo base_url('item-image-remove'); ?>">
    <input type="hidden" data-item-imagedefault value="<?php echo base_url('item-image-default'); ?>">
	<input type="hidden" data-csrf value="<?php echo $this->security->get_csrf_hash(); ?>">
	<input type="hidden" data-item-id value="<?php echo $products->item_id; ?>">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#data" data-toggle="tab">Termék adatok</a></li>
				<li><a href="#image" data-toggle="tab">Képek</a></li>
			</ul>
			<div class="tab-content">
				<div class="active tab-pane" id="data">
					<?php echo form_open($url, array('class' => 'form-horizontal')); ?>
						<input type="hidden" name="item_id" value="<?php echo $products->item_id; ?>">
						<div class="form-group">
							<label for="inputProductName" class="col-sm-2 control-label">Termék név</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="item_name" id="inputProductName" value="<?php echo $products->item_name; ?>" placeholder="Termék név">
							</div>
						</div>
						<div class="form-group">
							<label for="inputProductUrl" class="col-sm-2 control-label">Termék Url</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="item_slug" id="inputProductUrl" value="<?php echo $products->item_slug; ?>" placeholder="Termék URL">
							</div>
							<div class="col-sm-4">
								<button type="button" data-slug-generate  class="btn btn-info" role="button"><i class="fa fa-link"></i></button>
							</div>
						</div>
						<div class="form-group">
							<label for="inputProductSku" class="col-sm-2 control-label">SKU</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="item_sku" id="inputProductSku" value="<?php echo $products->item_sku; ?>" placeholder="Termék Sku">
							</div>
							<div class="col-sm-4">
								<button type="button" data-sku-generate  class="btn btn-info" role="button"><i class="fa fa-barcode"></i></button>
							</div>
						</div>
						<div class="form-group">
							<label for="inputProductTax" class="col-sm-2 control-label">Termék áfa</label>
							<div class="col-sm-2">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-percent"></i></span>
									<input type="number" class="form-control" name="item_tax" id="inputProductTax" value="<?php echo $products->item_tax; ?>" placeholder="Termék áfa">
									<span class="input-group-addon"> <b>%</b> </span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="inputProductWeight" class="col-sm-2 control-label">Termék súly</label>
							<div class="col-sm-2">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-cube"></i></span>
									<input type="number" class="form-control" name="item_weight" id="inputProductWeight" value="<?php echo $products->item_weight; ?>" placeholder="Termék súly" step="0.01">
									<span class="input-group-addon"> <b>kg</b> </span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="inputProductDetails" class="col-sm-2 control-label">Termék leírás</label>
							<div class="col-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-list"></i></span>
									<textarea id="item_longdesc" class="form-control" name="item_longdesc" rows="10" cols="80"><?php echo $products->item_longdesc; ?></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="inputProductPrice" class="col-sm-2 control-label">Termék ár</label>
							<div class="col-sm-2">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-usd"></i></span>
									<input type="number" class="form-control" name="item_netprice" id="inputProductPrice" value="<?php echo $products->item_netprice; ?>" placeholder="Normál ár">
									<span class="input-group-addon"> <b>Ft</b> </span>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-usd"></i></span>
									<input type="number" class="form-control" name="item_netpricesale" value="<?php echo $products->item_netpricesale; ?>"placeholder="Akciós ár">
									<span class="input-group-addon"> <b>Ft</b> </span>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i> Akció </span>
									<input type="text" class="form-control pull-right active" value="<?php if(!empty($products->item_salestart) && !empty($products->item_saleend)){ echo str_replace('-', '/', $products->item_salestart); ?> - <?php echo str_replace('-', '/', $products->item_saleend); } ?>" name="item_saledate" id="saledate">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="inputProductStatus" class="col-sm-2 control-label">Látható</label>
							<div class="col-sm-10">
								<div class="checkbox">
									<label>
										<input id="#inputProductStatus" name="item_status" <?php if ($products->item_status): ?>checked<?php endif;?> type="checkbox">
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-danger">Elküld</button>
							</div>
						</div>
					</form>
				</div><!-- /.tab-pane -->
				<div class="tab-pane" id="image">
					<form class="dropzone" id="my-awesome-dropzone">
                            <div class="dropzone-previews"></div>
                            <?php if (!empty($images)): ?>
                            <?php foreach ($images as $image): ?>
                                <div class="dz-preview dz-processing dz-image-preview <?php if ($image->image_featured): ?>red-border<?php endif;?>">
                                <div class="dz-image">
                                        <img width="100" height="90" src="<?php echo base_url($image->image_url); ?>">
                                </div>
                                <div class="text-center">
                                <button data-toggle="tooltip" data-placement="top" title="Törlés" type="button" data-item-remove="<?php echo $image->image_id; ?>" data-item-store="<?php echo $image->image_storeid; ?>" class="btn btn-box-tool red"><i class="fa fa-2x fa-times fa-danger"></i>
                                </button>
                                <button data-toggle="tooltip" data-placement="top" title="Alapértelmezett kép" type="button" data-item-default="<?php echo $image->image_id; ?>" class="btn btn-box-tool"><i class="fa fa-2x fa-picture-o"></i>
                                </button>
                              </div>
                            </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </form>
				</div><!-- /.tab-pane -->
				<div class="tab-pane" id="video">
				</div>
			</div><!-- /.tab-content -->
		</div><!-- /.nav-tabs-custom -->
	</div><!-- /.col -->
</div><!-- /.row -->
