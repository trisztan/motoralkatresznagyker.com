<input type="hidden" data-category-modal-url value="<?php echo base_url('category-modal'); ?>">
<input type="hidden" data-category-datatable-url value="<?php echo base_url('category-datatable'); ?>">

<div class="row">
    <div class="col-xs-12">
        <div data-modal></div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Kategóriák</h3>
                <button data-category-add class="btn btn-info pull-right" role="button"><i class="fa fa-plus"></i> Kategória hozzáadás</button>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="category" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Kategória</th>
                            <th>Alkategória</th>
                            <th>Gyártó</th>
                            <th>Motor</th>
                            <th>Tipus</th>
                            <th>Évjárat</th>
                            <th>Sku</th>
                            <th>Url</th>
                            <th>Műveletek</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
  </div><!-- /.col -->
</div><!-- /.row -->
