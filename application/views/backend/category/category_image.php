<div class="modal fade" data-category-url="<?php echo base_url('category-add'); ?>" id="categorymodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form class='form-horizontal' method="POST" action="<?php echo base_url('category-image'); ?>" id="form-category" enctype="multipart/form-data">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Kategória kép</h4>
            </div>
            <div class="modal-body">
                    <input type="hidden" name="image_name" value="<?php echo $id?>">
                    <div class="form-group">
                        <input type="file" name="file" id="fileToUpload">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Feltöltés</button>
            </div>
            </form>
        </div>
    </div>
</div>
