<div class="modal fade" data-category-url="<?php echo base_url('category-edit'); ?>" id="categorymodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Kategória szerkesztés</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-category">
                    <div class="form-group">
                        <input type="hidden" name="category_id" value="<?php echo $category_id;?>">
                        <label for="kategoria" class="col-sm-4 control-label">Kategória</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="kategoria" value="<?php echo $kategoria;?>" id="kategoria" placeholder="Kategória">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="alkategoria" class="col-sm-4 control-label">Alkategória</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="alkategoria" value="<?php echo $alkategoria;?>" id="alkategoria" placeholder="Alkategória">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="gyarto" class="col-sm-4 control-label">Gyártó</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="gyarto" value="<?php echo $gyarto;?>" id="gyarto" placeholder="Gyártó">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ccm" class="col-sm-4 control-label">ccm</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="ccm" value="<?php echo $ccm;?>" id="ccm" placeholder="ccm">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tipus" class="col-sm-4 control-label">Típus</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="tipus" value="<?php echo $tipus;?>" id="tipus" placeholder="Típus">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="evjarat" class="col-sm-4 control-label">Évjárat</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="evjarat" value="<?php echo $evjarat;?>" id="evjarat" placeholder="Évjárat">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sku" class="col-sm-4 control-label">SKU</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="sku" value="<?php echo $sku;?>" id="sku" placeholder="SKU">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-category-save class="btn btn-primary">Mentés</button>
            </div>
        </div>
    </div>
</div>
