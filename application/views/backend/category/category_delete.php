<div class="modal fade" data-category-url="<?php echo base_url('category-delete'); ?>" id="categorymodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Kategória törlés</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-category">
                    <input type="hidden" name="category_id" value="<?php echo $category_id;?>">
                </form>
                Valóban törölni szeretnéd a kategóriát?
            </div>
            <div class="modal-footer">
                <button type="button" data-category-save class="btn btn-primary">Igen</button>
            </div>
        </div>
    </div>
</div>
