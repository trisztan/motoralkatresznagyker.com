<input type="hidden" data-ordertemplate-modal-url value="<?php echo base_url('ordertemplate-modal'); ?>">
<div class="row">
    <div class="col-xs-12">
        <div data-modal></div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Rendelési sablonok</h3>
                <button class="btn btn-info pull-right" role="button" data-ordertemplate-add><i class="fa fa-plus"></i> Sablon hozzáadás</button>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="ordertemplate_list" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Név</th>
                            <th>Szín</th>
                            <th>Műveletek</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $ordertemplates; ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
