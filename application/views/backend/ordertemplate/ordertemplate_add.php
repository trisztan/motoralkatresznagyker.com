<div class="modal modal-fullscreen modal-transparent fade in" data-ordertemplate-url="<?php echo base_url('ordertemplate-add'); ?>" id="ordertemplatemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Sablon hozzáadás</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-ordertemplate">
                    <div class="form-group">
                        <label for="ordertemplate_lastname" class="col-sm-4 control-label">Név</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="ordertemplate_name" id="ordertemplate_name" placeholder="Név">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ordertemplate_firstname" class="col-sm-4 control-label">Szín</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="ordertemplate_color" id="ordertemplate_color" placeholder="Szín">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-header">
                                    Sablon
                                </div>
                                <div class="box-body">
                            <div class="ordertemplate_template">
                                <table border="0" cellpadding="0" cellspacing="0" style="width:100%">
                                      <tbody>
                                          <tr>
                                              <td>
                                              <table border="0" cellpadding="0" cellspacing="0" style="width:100%">
                                                  <tbody>
                                                      <tr>
                                                          <td style="width:50%">
                                                          <h1>MOTORALKATRÉSZ</h1>

                                                          <h2>NAGYKER</h2>
                                                          </td>
                                                          <td style="width:50%"><strong>Megrendelés azonosító:</strong> #order_id<br>
                                                          <strong>Megrendelés időpontja:</strong> #order_date<br>
                                                          <strong>Átvétel módja:</strong> Futárszolgálat<br>
                                                          <strong>Átvétel helye:</strong> Kiszállításkor<br>
                                                          <strong>Fizetési mód:</strong> #order_bill<br>
                                                          <strong>Futár:</strong> #order_curier</td>
                                                      </tr>
                                                  </tbody>
                                              </table>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                              <table border="0" cellpadding="0" cellspacing="0" style="width:100%">
                                                  <tbody>
                                                      <tr>
                                                          <td><strong>Tisztelt #order_username</strong></td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                          <h2 style="font-style:italic;"><strong>Rendelési állapot: Folyamatban</strong></h2>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>#comment</td>
                                                      </tr>
                                                      <tr>
                                                          <td>Rendszerünk fogadta megrendelését.<br>
                                                          Ön a következő termék(ek)re adott le rendelést webáruházunkban:</td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                          <p>&nbsp;</p>

                                                          <p><br>
                                                          <strong>Nyomkövetési azonosító:</strong><br>
                                                          #order_track_url</p>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>&nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                          <td colspan="2">#order_items</td>
                                                      </tr>
                                                  </tbody>
                                                  <tbody>
                                                      <tr>
                                                          <td>&nbsp;</td>
                                                          <td>&nbsp;</td>
                                                          <td>Szállítási költség:</td>
                                                          <td>#order_ship</td>
                                                      </tr>
                                                  </tbody>
                                                  <tbody>
                                                      <tr>
                                                          <td>&nbsp;</td>
                                                          <td>&nbsp;</td>
                                                          <td>Fizetendő összesen:</td>
                                                          <td>#order_total</td>
                                                      </tr>
                                                  </tbody>
                                              </table>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td colspan="2">
                                              <table border="0" cellpadding="0" cellspacing="0" style="width:100.0%">
                                                  <tbody>
                                                      <tr>
                                                          <td>
                                                          <p><em>A feltüntetett árak bruttó árak, és tartalmazzák a 27% ÁFA-t.</em></p>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>&nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                          <td style="height:15px">&nbsp;</td>
                                                      </tr>
                                                  </tbody>
                                              </table>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>&nbsp;</td>
                                          </tr>
                                          <tr>
                                              <td colspan="2"><br>
                                              <br>
                                              <strong>Általános szerződési feltételeinket, és az elállási jogra vonatkozó információkat az alábbi linken olvashatja:</strong><br>
                                              <a href="#aszf" target="_blank">Megnyitás</a><br>
                                              &nbsp;</td>
                                          </tr>
                                          <tr>
                                              <td>&nbsp;</td>
                                          </tr>
                                          <tr>
                                              <td colspan="2">Köszönjük, hogy bizalmával megtisztelt minket és nálunk vásárol!<br>
                                              <br>
                                              Üdvözlettel:<br>
                                              MOTORALKATRÉSZ NAGYKER<br>
                                              Telefon: #tel<br>
                                              E-mail: <a href="mailto:#email" target="_blank">#email</a> | Web: <a href="#base_url" target="_blank">#base_url</a>&#8203;</td>
                                          </tr>
                                          <tr>
                                              <td colspan="2" style="text-align:center"><br>
                                              <br>
                                              <br>
                                              Ha megrendelését törölni szeretné kérjük írásban jelezze a <a href="mailto:#email" target="_blank">#email</a> e-mail címre küldött levélben, a megrendelés azonosítóra való hivatkozással.</td>
                                          </tr>
                                      </tbody>
                                    </table>
                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-ordertemplate-save class="btn btn-primary">Mentés</button>
            </div>
        </div>
    </div>
</div>
