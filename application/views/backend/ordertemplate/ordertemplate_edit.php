<div class="modal modal-fullscreen modal-transparent fade in" data-ordertemplate-url="<?php echo base_url('ordertemplate-edit'); ?>" id="ordertemplatemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Sablon szerkesztés</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-ordertemplate">
                    <div class="form-group">
                        <input type="hidden" name="ordertemplate_id" value="<?php echo $ordertemplate_id; ?>">
                        <label for="ordertemplate_lastname" class="col-sm-4 control-label">Név</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="ordertemplate_name" id="ordertemplate_name" value="<?php echo $ordertemplate_name; ?>" placeholder="Név">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ordertemplate_firstname" class="col-sm-4 control-label">Szín</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="ordertemplate_color" id="ordertemplate_color" value="<?php echo $ordertemplate_color; ?>" placeholder="Szín">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-header">
                                    Sablon
                                </div>
                                <div class="box-body">
                                    <div class="ordertemplate_template">
                                        <?php echo $ordertemplate_template; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="alert alert-info">
                      <strong>Mágikus változók</strong>
                        <ul>
                          <li>#order_id - Megrendelés id</li>
                          <li>#order_date - Megrendelés ideje</li>
                          <li>#order_username - Megrendelő neve</li>
                          <li>#order_track_url - Nyomkövetési url</li>
                          <li>#order_items - Megrendelt termékek</li>
                          <li>#order_ship - Szállítási költség</li>
                          <li>#order_total - Fizetendő összesen</li>
                          <li><b>A többi változó pl: #aszf a beállításokból jön.</b></li>
                        </ul>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-ordertemplate-save class="btn btn-primary">Mentés</button>
            </div>
        </div>
    </div>
</div>
