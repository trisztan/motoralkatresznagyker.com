<div class="modal fade" data-ordertemplate-url="<?php echo base_url('ordertemplate-delete'); ?>" id="ordertemplatemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Sablon törlés</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-ordertemplate">
                    <input type="hidden" name="ordertemplate_id" value="<?php echo $ordertemplate_id;?>">
                </form>
                Valóban törölni szeretnéd a sablont?
            </div>
            <div class="modal-footer">
                <button type="button" data-ordertemplate-save class="btn btn-primary">Igen</button>
            </div>
        </div>
    </div>
</div>
