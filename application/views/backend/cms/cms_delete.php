<div class="modal fade" data-cms-url="<?php echo base_url('cms-delete'); ?>" id="cmsmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Oldal törlés</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-cms">
                    <input type="hidden" name="cms_id" value="<?php echo $cms_id;?>">
                </form>
                Valóban törölni szeretnéd az oldalt?
            </div>
            <div class="modal-footer">
                <button type="button" data-cms-save class="btn btn-primary">Igen</button>
            </div>
        </div>
    </div>
</div>
