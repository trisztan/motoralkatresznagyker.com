<div class="modal modal-fullscreen modal-transparent fade" data-cms-url="<?php echo base_url('cms-edit'); ?>" id="cmsmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Oldal szerkesztés</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-cms">
                    <input type="hidden" name="cms_id" value="<?php echo $cms_id;?>">
                    <input type="hidden" name="cms_slug_original" value="<?php echo $cms_slug;?>">
                    <div class="form-group">
                        <label for="cms_title" class="col-sm-2 control-label">Oldal név</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="cms_title" value="<?php echo $cms_title;?>" id="cms_title" placeholder="Oldal név">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cms_slug" class="col-sm-2 control-label">Oldal url</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="cms_slug" id="cms_slug" value="<?php echo $cms_slug;?>" placeholder="Oldal url">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cms_keywords" class="col-sm-2 control-label">Meta kulcsszavak</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="cms_keywords" id="cms_keywords" placeholder="Meta kulcsszavak">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cms_description" class="col-sm-2 control-label">Meta leírás</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="cms_description" id="cms_description" placeholder="Meta leírás">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cms_position" class="col-sm-2 control-label">Oldal státusz</label>
                        <div class="col-sm-10">
                              <select class="form-control" name="cms_position" id="cms_position">
                                <option value="fent" <?php if($cms_status == 'fent'):?>selected<?php endif;?>>Fent</option>
                                <option value="lent" <?php if($cms_status == 'lent'):?>selected<?php endif;?>>Lent</option>
                              </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cms_content" class="col-sm-2 control-label">Tartalom</label>
                        <div class="col-sm-10">
                            <textarea id="cms_content" class="form-control" name="cms_content" rows="10" cols="80"><?php echo $cms_content;?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cms_status" class="col-sm-2 control-label">Oldal státusz</label>
                        <div class="col-sm-10">
                              <select class="form-control" name="cms_status" id="cms_status" data-cms-type-select>
                                <option value="1" <?php if($cms_status == 1):?>selected<?php endif;?>>Aktív</option>
                                <option value="0" <?php if($cms_status == 0):?>selected<?php endif;?>>Nem aktív</option>
                              </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-cms-save class="btn btn-primary">Mentés</button>
            </div>
        </div>
    </div>
</div>
