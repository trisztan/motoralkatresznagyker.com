<div class="modal modal-fullscreen modal-transparent fade" data-cms-url="<?php echo base_url('cms-add'); ?>" id="cmsmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Oldal hozzáadás</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-cms">
                    <div class="form-group">
                        <label for="cms_title" class="col-sm-2 control-label">Oldal név</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="cms_title" id="cms_title" placeholder="Oldal név">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cms_code" class="col-sm-2 control-label">Oldal url</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="cms_slug" id="cms_slug" placeholder="Oldal url">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cms_keywords" class="col-sm-2 control-label">Meta kulcsszavak</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="cms_keywords" id="cms_keywords" placeholder="Meta kulcsszavak">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cms_description" class="col-sm-2 control-label">Meta leírás</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="cms_description" id="cms_description" placeholder="Meta leírás">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cms_position" class="col-sm-2 control-label">Oldal státusz</label>
                        <div class="col-sm-10">
                              <select class="form-control" name="cms_position" id="cms_position">
                                <option value="fent">Fent</option>
                                <option value="lent">Lent</option>
                              </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cms_content" class="col-sm-2 control-label">Tartalom</label>
                        <div class="col-sm-10">
                            <textarea id="cms_content" class="form-control" name="cms_content" rows="10" cols="80"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cms_status" class="col-sm-2 control-label">Oldal státusz</label>
                        <div class="col-sm-10">
                              <select class="form-control" name="cms_status" id="cms_status" data-cms-type-select>
                                <option value="1">Aktív</option>
                                <option value="0">Nem aktív</option>
                              </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-cms-save class="btn btn-primary">Mentés</button>
            </div>
        </div>
    </div>
</div>
