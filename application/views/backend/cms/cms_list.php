<input type="hidden" data-cms-modal-url value="<?php echo base_url('cms-modal'); ?>">
<div class="row">
    <div class="col-xs-12">
        <div data-modal></div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Oldalak</h3>
                <button class="btn btn-info pull-right" role="button" data-cms-add><i class="fa fa-plus"></i> Oldal hozzáadás</button>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="cms_list" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Név</th>
                            <th>Url</th>
                            <th>Meta kulcsszavak</th>
                            <th>Meta leírás</th>
                            <th>Pozició</th>
                            <th>Státusz</th>        
                            <th>Műveletek</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $cmss; ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
