<input type="hidden" data-impexp-upload-url value="<?php echo base_url('impexp-upload'); ?>">
<input type="hidden" data-impexp-import-url value="<?php echo base_url('impexp-import'); ?>">
<input type="hidden" data-impexp-jstree-url value="<?php echo base_url('impexp-jstree'); ?>">
<input type="hidden" data-impexp-export-url value="<?php echo base_url('impexp-export'); ?>">

<!-- Datatable URL -->
<div class="box">
  <div class="box-header">
          <h3 class="box-title">Importálás</h3>
        </div><!-- /.box-header -->
        <div class="box-body">

<div class="row">
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <div class="info-box">
        <form class="dropzone" id="my-awesome-dropzone">
          <div class="dropzone-previews"></div>
          <div class="form-group">
                <label for="coupon_status" class="col-sm-12 control-label">Milyen csv fájlt akarsz feltölteni?</label>
                <div class="col-sm-12">
                      <select class="form-control" data-file-type>
                        <option value="products">Termék lista</option>
                        <option value="categories">Kategória lista</option>
                      </select>
                </div>
            </div>
        </form>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <div class="col-md-9 col-sm-6 col-xs-12">
          <div class="info-box" id="products">
            <span class="info-box-icon bg-blue"><i class="fa fa-cubes"></i></span>

            <div class="info-box-content bg-white">
              <span class="info-box-text">Termék importálás <button data-import-start="products" class="btn btn-info btn-sm pull-right" role="button"><i class="fa fa-upload"></i> Termék importálás inditása</button></span>
              <span class="info-box-number"></span>

              <div class="progress">
                <div class="progress-bar" style="width: 0%"></div>
              </div>
              <span class="progress-description">
                0%
              </span>

            </div>

            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
    <div class="col-md-9 col-sm-6 col-xs-12">
          <div class="info-box" id="categories">
            <span class="info-box-icon bg-blue"><i class="fa fa-ellipsis-h"></i></span>

            <div class="info-box-content bg-white">
              <span class="info-box-text">Kategória importálás <button data-import-start="categories" class="btn btn-info btn-sm pull-right" role="button"><i class="fa fa-upload"></i> Kategória importálás inditása</button></span>
              <span class="info-box-number"></span>

              <div class="progress">
                <div class="progress-bar" style="width: 0%"></div>
              </div>
              <span class="progress-description">
                0%
              </span>

            </div>

            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
</div><!-- /.row -->
</div>
</div>
<div class="box">
    <div class="box-header">
          <h3 class="box-title">Exportálás</h3>
          <button data-export-start="category" class="btn btn-info btn-sm pull-right" style="margin-left: 20px;" role="button"><i class="fa fa-download"></i> Kategória exportálás</button>
          <button data-export-start="product" class="btn btn-info btn-sm pull-right"  style="margin-left: 20px;" role="button"><i class="fa fa-download"></i> Termék exportálás</button>
          <button data-export-start="unproduct" class="btn btn-info btn-sm pull-right" role="button"><i class="fa fa-download"></i> Nem kategorzált termék exportálás</button>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div id="tree_4" class="tree-demo"> </div>
    </div><!-- /.row -->
</div>
</div>
