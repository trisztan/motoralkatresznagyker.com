<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo title(); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf" value="<?php echo $this->security->get_csrf_hash(); ?>" data-csrf>
    <meta name="robots" content="noindex, nofollow">
    <meta name="author" content="Balogh Ferenc">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url('template/backend/bootstrap/css/bootstrap.min.css'); ?>">
    <!-- Font Awesome -->
    <?php $this->enqueue->loadcss(); ?>
    <?php if (!empty($css)): ?>
    <?php foreach ($css as $cs): ?>
    <?php if (strpos($cs, '://') !== false): ?>
      <link rel="stylesheet" href="<?php echo $cs; ?>">
    <?php else: ?>
      <link rel="stylesheet" href="<?php echo base_url($cs); ?>">
    <?php endif;?>
    <?php endforeach;?>
    <?php endif;?>
    <link rel="stylesheet" href="<?php echo base_url('template/backend/plugins/font-awesome/css/font-awesome.min.css'); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('template/backend/dist/css/AdminLTE.min.css'); ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url('template/backend/dist/css/skins/_all-skins.min.css'); ?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url('template/backend/plugins/iCheck/flat/blue.css'); ?>">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo base_url('template/backend/plugins/morris/morris.css'); ?>">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url('template/backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css'); ?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url('template/backend/plugins/datepicker/datepicker3.css'); ?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url('template/backend/plugins/daterangepicker/daterangepicker-bs3.css'); ?>">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url('template/backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><?php echo get_cfg('sitenameshort'); ?></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><?php echo get_cfg('sitename'); ?> admin</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="btn-info dropdown user user-menu ">
                <a href="<?php echo base_url('/') ?>">
                  <span class="hidden-xs"><i class="fa fa-home"></i> Oldal előnézete</span>
                </a>
              </li>
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="hidden-xs"><?php echo get_fullname_by_email(); ?></span>
                </a>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li class="btn-danger dropdown user user-menu">
                <a href="<?php echo base_url('admin-logout'); ?>"><i class="fa fa-close"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
           <ul class="sidebar-menu">
            <li class="treeview">
              <a href="<?php echo base_url('admin') ?>">
                <i class="fa fa-dashboard"></i> <span>Vezérlőpult</span></i>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url('item-list') ?>">
                <i class="fa fa-cubes"></i> <span>Termékek</span></i>
                <!--<small class="label pull-right bg-red">3</small>-->
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url('category-list') ?>">
                <i class="fa fa-ellipsis-h"></i> <span>Kategóriák</span></i>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url('coupon-list') ?>">
                <i class="fa fa-tag"></i> <span>Kuponok</span></i>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url('offer-list') ?>">
                <i class="fa fa-envelope"></i> <span>Ajánlatkérések</span>
                <?php if (offer_count_new() > 0): ?><small class="label pull-right bg-red"><?php echo offer_count_new(); ?> új</small><?php endif;?>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url('order-list') ?>">
                <i class="fa fa-shopping-cart"></i> <span>Rendelések</span>
                <?php if (order_count_new() > 0): ?><small class="label pull-right bg-red"><?php echo order_count_new(); ?> új</small><?php endif;?>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url('ship-list') ?>">
                <i class="fa fa-truck"></i> <span>Szállítás</span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url('cms-list') ?>">
                <i class="fa fa-th"></i> <span>Oldalak</span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url('user-list') ?>">
                <i class="fa fa-user"></i> <span>Adminok</span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url('impexp-list') ?>">
                <i class="fa fa-upload"></i> <span>Import / Export</span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url('openhours-list') ?>">
                <i class="fa fa-calendar"></i> <span>Nyitvatartás</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-cog"></i>
                <span>Beállítások</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url('redirect-list') ?>"><i class="fa fa-refresh"></i> Átirányítások</a></li>
                <li><a href="<?php echo base_url('config-list') ?>"><i class="fa fa-wrench"></i> Oldal beállítások</a></li>
                <li><a href="<?php echo base_url('offertemplate-list') ?>"><i class="fa fa-envelope"></i> Ajánlat sablonok</a></li>
                <li><a href="<?php echo base_url('ordertemplate-list') ?>"><i class="fa fa-envelope"></i> Rendelés sablonok</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Admin
            <small><?php echo breadcrumb(); ?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin') ?>"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active"><?php echo breadcrumb(); ?></li>
            <li class="pull-right"><button type="button" class="btn btn-danger btn-sm back"> <span class="glyphicon glyphicon-chevron-left"></span> Vissza </button></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
