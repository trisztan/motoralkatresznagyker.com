<div class="modal fade" data-coupon-url="<?php echo base_url('coupon-delete'); ?>" id="couponmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Kupon törlés</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-coupon">
                    <input type="hidden" name="coupon_id" value="<?php echo $coupon_id;?>">
                </form>
                Valóban törölni szeretnéd a kupont?
            </div>
            <div class="modal-footer">
                <button type="button" data-coupon-save class="btn btn-primary">Igen</button>
            </div>
        </div>
    </div>
</div>
