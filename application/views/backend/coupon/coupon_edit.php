<div class="modal fade" data-coupon-url="<?php echo base_url('coupon-edit'); ?>" id="couponmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Kupon szerkesztés</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-coupon">
                    <div class="form-group">
                        <input type="hidden" name="coupon_id" value="<?php echo $coupon_id;?>">
                        <input type="hidden" name="coupon_code_original" value="<?php echo $coupon_code;?>">
                        <label for="coupon_name" class="col-sm-4 control-label">Kupon név</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="coupon_name" value="<?php echo $coupon_name;?>" id="coupon_name" placeholder="Kupon név">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="coupon_code" class="col-sm-4 control-label">Kupon kód</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="coupon_code" value="<?php echo $coupon_code;?>"id="coupon_code" placeholder="Kupon kód">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="coupon_type" class="col-sm-4 control-label">Kupon típus</label>
                        <div class="col-sm-6">
                              <select class="form-control" name="coupon_type" id="coupon_type" data-coupon-type-select>
                                <option value="PERCENT" <?php if($coupon_code == "PERCENT"):?>selected<?php endif;?>>% alapú kedvezmény</option>
                                <option value="SUM" <?php if($coupon_code == "SUM"):?>selected<?php endif;?>>Összeg alapú kedvezmény</option>
                              </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="coupon_status" class="col-sm-4 control-label">Kupon státusz</label>
                        <div class="col-sm-6">
                              <select class="form-control" name="coupon_status" id="coupon_status" data-coupon-type-select>
                                <option value="1" <?php if($coupon_status == 1):?>selected<?php endif;?>>Aktív</option>
                                <option value="0" <?php if($coupon_status == 0):?>selected<?php endif;?>>Nem aktív</option>
                              </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="coupon_value" class="col-sm-4 control-label">Kupon érték</label>
                        <div class="col-sm-6">
                            <div class="input-group">  
                                <input type="number" class="form-control" min="1" value="<?php echo $coupon_value;?>"name="coupon_value" id="coupon_value" value="" placeholder="Kupon érték">
                                 <span class="input-group-addon" data-coupon-type><i class="fa fa-percent"></i></span>
                            </div>
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="coupon_life" class="col-sm-4 control-label">Felhasználható (alkalom)</label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" min="1" name="coupon_life" value="<?php echo $coupon_life;?>" id="coupon_life" value="" placeholder="Felhasználható (alkalom)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="coupon_datestart" class="col-sm-4 control-label">Érvényesség kezdete</label>
                        <div class="col-sm-6">
                            <input type="date"" class="form-control" name="coupon_datestart" value="<?php echo $coupon_datestart;?>"id="coupon_datestart" placeholder="Érvényesség kezdete">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="coupon_dateend" class="col-sm-4 control-label">Érvényesség vége</label>
                        <div class="col-sm-6">
                            <input type="date" class="form-control" name="coupon_dateend" value="<?php echo $coupon_dateend;?>"id="coupon_dateend" placeholder="Érvényesség vége">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-coupon-save class="btn btn-primary">Mentés</button>
            </div>
        </div>
    </div>
</div>