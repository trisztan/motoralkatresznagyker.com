<input type="hidden" data-coupon-modal-url value="<?php echo base_url('coupon-modal'); ?>">
<div class="row">
    <div class="col-xs-12">
        <div data-modal></div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Kuponok</h3>
                <button class="btn btn-info pull-right" role="button" data-coupon-add><i class="fa fa-plus"></i> Kupon hozzáadás</button>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="coupon_list" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Név</th>
                            <th>Kód</th>
                            <th>Érték</th>
                            <th>Érvényeség kezdete</th>
                            <th>Érvényeség vége</th>
                            <th>Státusz</th>
                            <th>Műveletek</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $coupons; ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->