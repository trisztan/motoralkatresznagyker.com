<div class="modal fade" data-offer-url="<?php echo base_url('offer-archived'); ?>" id="offermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Ajánlatkérés archiválása</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-offer">
                    <input type="hidden" name="offer_id" value="<?php echo $offer_id;?>">
                </form>
                Valóban szeretnéd archiválni az ajánlatkérést?
            </div>
            <div class="modal-footer">
                <button type="button" data-offer-save class="btn btn-primary">Igen</button>
            </div>
        </div>
    </div>
</div>
