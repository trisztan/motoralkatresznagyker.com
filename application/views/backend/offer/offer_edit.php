<div class="modal fade" data-offer-url="<?php echo base_url('offer-edit'); ?>" id="offermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><a href="" class="btn btn-danger btn-sm" role="button"><i class="fa fa-times"></i></a></span></button>
                <h4 class="modal-title" id="myModalLabel">Ajánlatkérés válasz</h4>
            </div>
            <div class="modal-body">
                <form class='form-horizontal' id="form-offer">
                    <div class="form-group">
                        <input type="hidden" name="offer_id" value="<?php echo $offer_id;?>">
                        <label for="offer_replymsg" class="col-sm-4 control-label">Válasz üzenet</label>
                        <div class="col-sm-8">
                            <textarea name="offer_replymsg" id="offer_replymsg" style="height:200px; width: 100%;"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="offer_statusid" class="col-sm-4 control-label">Válasz sablon</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="offer_statusid" id="offer_statusid">
                                 <?php foreach($offers as $offer):?>
                                 <option value="<?php echo $offer->offertemplate_id;?>"><?php echo $offer->offertemplate_name;?></option>
                                <?php endforeach;?>
                             </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-offer-save class="btn btn-primary">Mentés</button>
            </div>
        </div>
    </div>
</div>
