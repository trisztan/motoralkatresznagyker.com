<input type="hidden" data-offer-modal-url value="<?php echo base_url('offer-modal'); ?>">
<input type="hidden" data-offer-datatable-url value="<?php echo base_url('offer-datatable'); ?>">

<div class="row">
    <div class="col-xs-12">
        <div data-modal></div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Ajánlatkérések</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="offer" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Cím</th>
                            <th>Üzenet</th>
                            <th>Ajánlatkérő</th>
                            <th>Dátum</th>
                            <th>E-mail</th>
                            <th>Telefon</th>
                            <th>Megválaszolva</th>
                            <th>Archívált</th>
                            <th>Műveletek</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
  </div><!-- /.col -->
</div><!-- /.row -->
