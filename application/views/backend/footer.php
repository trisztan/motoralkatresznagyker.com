<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
           Készítette: <a href="http://deweb.hu"><img src="<?php echo base_url('template/frontend/assets/img/deweb.png');?>"></a>
        </div>
        <strong>Copyright &copy; 2016 Minden jog fenntartva.</strong>
      </footer>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('template/backend/plugins/jQuery/jQuery-2.1.4.min.js');?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url('template/backend/bootstrap/js/bootstrap.min.js')?>"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?php echo base_url('template/backend/plugins/morris/morris.min.js');?>"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url('template/backend/plugins/sparkline/jquery.sparkline.min.js');?>"></script>
    <!-- jvectormap -->
    <script src="<?php echo base_url('template/backend/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
    <script src="<?php echo base_url('template/backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo base_url('template/backend/plugins/knob/jquery.knob.js');?>"></script>
    <!-- daterangepicker -->
    <script src="<?php echo base_url('template/backend/plugins/daterangepicker/daterangepicker.js');?>"></script>
    <!-- datepicker -->
    <script src="<?php echo base_url('template/backend/plugins/datepicker/bootstrap-datepicker.js');?>"></script>
    <!-- Slimscroll -->
    <script src="<?php echo base_url('template/backend/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('template/backend/plugins/fastclick/fastclick.min.js');?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('template/backend/dist/js/app.min.js');?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('template/backend/dist/js/demo.js');?>"></script>
    <script>
    $(document).ready(function(){
        $('button.back').click(function(){
            parent.history.back();
            return false;
        });
    });
    </script>
    <?php if(!empty($js)):?>
    <?php foreach ($js as $j):?>
      <?php if (strpos($j, '://') !== false):?>
         <script src="<?php echo $j; ?>"></script>
        <?php else:?>
    <script src="<?php echo base_url($j); ?>"></script>
     <?php endif;?>
    <?php endforeach;?>
    <?php endif; ?>
    <?php $this->enqueue->loadjs(); ?>
  </body>
</html>
