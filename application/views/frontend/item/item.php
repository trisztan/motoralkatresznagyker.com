<style>
.sidebar .block .block-inner .block-title {
    border: none !important;
    margin-bottom: 0px;
}
</style>
<div id="content" class="page-contact">
   <div class="section gray-light">
       <div class="container">
           <div class="row">
               <div class="col-md-3 col-sm-12">
                   <div class="sidebar">
                           <div class="latest-reviews block block-shadow white">
                               <div class="block-inner">
                                   <div class="block-title">
                                       <h3><i class="fa fa-cog"></i> <span style="margin-top:-10px">Termék kategóriák</span></h3>
                                   </div>
                                   <!-- /.block-title -->
                                   <div class="inner">
                                       <div class="row">
                                           <ul class="nav nav-pills nav-stacked">
                                               <?php foreach($categories as $category):?>
                                               <li><a href="<?php echo base_url($category->slug);?>"><?php echo $category->kategoria; ?></a></li>
                                               <?php endforeach;?>
                                           </ul>
                                       </div>
                                       <!-- /.row -->
                                   </div>
                                   <!-- /.inner -->
                               </div>
                               <!-- /.block-inner -->
                           </div>
                           <!-- /.block -->
                       <div class="latest-reviews block block-shadow white">
                           <div class="block-inner">
                               <div class="block-title">
                                   <h3>Akciós termékeink</h3>
                               </div>
                               <!-- /.block-title -->
                               <div class="inner">
                                   <div class="row">
                                   <?php if(!empty($product_sales)):?>
                                    <?php foreach($product_sales as $product_sale):?>
                                       <div class="item-wrapper col-lg-12 col-md-12 col-sm-4">
                                           <div class="item">
                                               <div class="picture hidden-sm">
                                                    <?php if(!empty($product_sale['images'])):?>
                                                 <?php foreach($product_sale['images'] as $image):?>
                                                   <a href="<?php echo base_url($image->image_url);?>" class="slide">
                                                   <img height="100px" width="100px" src="<?php echo base_url($image->image_url);?>" alt="#">
                                                   </a><!-- /.slide -->
                                                   <?php break;?>
                                                   <?php endforeach;?>
                                                   <?php else:?>
                                                   <img src="<?php echo base_url('template/frontend/assets/img/no-image-100.jpg');?>" alt="<?php echo $product_sale['item_sku']."-".$product_sale['item_name']?>">
                                                   <?php endif;?>

                                               </div>
                                               <!-- /.picture -->
                                               <div class="title">
                                                   <a href="<?php echo base_url($product_sale['item_slug']);?>"><?php echo $product_sale['item_name']?></a>
                                               </div>
                                               <div class="price"><?php echo price_calculator(
                                        $product_sale['item_netprice'],
                                        $product_sale['item_netpricesale'],
                                        $product_sale['item_tax']);?> Ft</div>
                                               <!-- /.title -->
                                               <div class="date"><?php echo $product_sale['item_sku'];?></div>
                                               <!-- /.date -->
                                               <div class="description">
                                                   <p>
                                                      <?php echo word_limiter($product_sale['item_longdesc'],10);?>
                                                   </p>
                                               </div>
                                               <!-- /.description -->
                                           </div>
                                           <!-- /.item -->
                                       </div>
                                   <?php endforeach;?>
                                   <?php else:?>
                                       <div class="col-md-12">
                                           <div class="alert alert-info" role="alert">Jelenleg nincs akciós termék</div>
                                       </div>
                                   <?php endif;?>
                                   </div>
                                   <!-- /.row -->
                               </div>
                               <!-- /.inner -->
                           </div>
                           <!-- /.block-inner -->
                       </div>
                       <!-- /.block -->
                   </div>
                   <!-- /.sidebar -->
               </div>
               <div class="col-md-9 col-sm-12">
                   <div id="main" class="white block block-shadow block-margin white contact">
                       <div class="row">
                           <div class="col-md-12">
                               <div class="block">
                                   <div class="page-header center">
                                       <div class="page-header-inner">
                                           <div class="line">
                                               <hr>
                                           </div><!-- /.line -->

                                           <div class="heading">
                                               <h2><?php echo $item->item_name; ?></h2>
                                           </div><!-- /.heading -->

                                           <div class="line">
                                               <hr>
                                           </div><!-- /.line -->
                                       </div><!-- /.page-header-inner -->
                                   </div>
                                   <div class="col-md-3">
                                       <?php if (!$item->item_image): ?>
                                            <img class="thumbnail img-responsive" src="<?php echo base_url('template/backend/dist/img/no-image-200.jpg'); ?>">
                                      <?php else:?>
                                           <a data-lightbox="image-<?php echo $item->item_id;?>"  href="<?php echo base_url($item->item_image); ?>"><img class="thumbnail img-responsive" src="<?php echo base_url($item->item_image); ?>"></a>
                                       <?php endif;?>
                                   </div>
                                   <div class="col-md-6 col-md-offset-1">
                                       <div class="row">
                                           <div class="col-md-12">
                                               <div class="action-buttons">
                                                    <?php if($item->brutto != 0): ?>
                                                        <?php if($item->brutto == $item->salebrutto): ?>
                                                            <div class="buy-it-now">
                                                                <div class="label">Online ár</div>
                                                                <div class="price"><?php echo number_format($item->brutto,0,'','.')." Ft";?></div>
                                                            </div><!-- /.buy-it-now -->
                                                        <?php elseif($item->brutto > 0 && $item->salebrutto == 0): ?>
                                                           <div class="buy-it-now">
                                                               <div class="label">Bolti ár</div>
                                                               <div class="price"><?php echo number_format($item->brutto,0,'','.')." Ft";?></div>
                                                           </div><!-- /.buy-it-now -->
                                                       <?php elseif($item->brutto > 0 && $item->salebrutto > 0):?>
                                                           <div class="buy-it-now">
                                                               <div class="label">Bolti ár</div>
                                                               <div class="price"><s><?php echo number_format($item->brutto,0,'','.')." Ft";?></s></div>
                                                           </div><!-- /.buy-it-now -->
                                                           <div class="buy-it-financial">
                                                               <div class="label">Online ár</div>
                                                               <div class="price">
                                                               <?php echo number_format($item->salebrutto,0,'','.')." Ft";?>
                                                               </div>
                                                           </div><!-- /.buy-it-financial -->
                                                       <?php endif;?>
                                                   <?php endif;?>



                                                </div>
                                                <hr/>
                                           </div>
                                           <?php if($item->item_netprice != 0):?>
                                           <div class="col-md-3">
                                               <input type="number" value="1" class="form-control" data-add-cart-qty>
                                           </div>
                                           <div class="col-md-3">

                                                <button data-add-to-cart="<?php echo $item->item_id;?>" class="btn btn-danger btn-sm pull-right"><span class="glyphicon glyphicon-shopping-cart"></span> Kosárba</button>

                                           </div>
                                           <?php else:?>
                                           A termék jelenleg nem rendelhető. A termék elérhetőségéről kérjük érdeklődjön az <a href="<?php echo base_url('arajanlatkeres');?>"><strong>Árajánlatkérés</strong></a> menüpontban.
                                           <?php endif;?>
                                       </div>
                                   </div>
                                   <div class="col-md-12">
                                       <!--<table class="table">
                                            <tbody>
                                                <tr>
                                                    <td class="property">Kategória</td>
                                                    <td class="value">New car</td>
                                                </tr>

                                                <tr>
                                                    <td class="property">Alkategória</td>
                                                    <td class="value">Hatchback</td>
                                                </tr>

                                                <tr>
                                                    <td class="property">Engine</td>
                                                    <td class="value">Fuel</td>
                                                </tr>

                                                <tr>
                                                    <td class="property">Performance</td>
                                                    <td class="value">92 KW </td>
                                                </tr>


                                                <tr>
                                                    <td class="property">Transmission</td>
                                                    <td class="value">manual</td>
                                                </tr>

                                                <tr>
                                                    <td class="property">Year</td>
                                                    <td class="value">2013</td>
                                                </tr>
                                                <tr>
                                                    <td class="property">Mileage</td>
                                                    <td class="value">36000</td>
                                                </tr>

                                                <tr>
                                                    <td class="property">Exterier color</td>
                                                    <td class="value">Red</td>
                                                </tr>

                                                <tr>
                                                    <td class="property">Interier color</td>
                                                    <td class="value">White</td>
                                                </tr>
                                            </tbody>
                                        </table>-->
                                        <h2>Termék leírás</h2>
                                        <hr/>
                                        <div class="info">
                                            <p><?php echo $item->item_longdesc;?></p>
                                        </div>
                                        <?php if(!empty($images)):?>
                                        <h2>Termék további képei</h2>
                                        <?php foreach ($images as $image): ?>
                                            <?php if (!$image->image_featured): ?>
                                                <a data-lightbox="roundtrip" href="<?php echo base_url($image->image_url); ?>"><img class="img-thumbnail" width="200" height="200" src="<?php echo base_url($image->image_url); ?>"></a>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                        <?php endif;?>
                                   </div>
                               </div>
                            </div>
                       </div>
                   </div>
                   <!-- /#main -->
               </div>
               <!-- /.col-md-12 -->
          </div>
           <!-- /.row -->
       </div>
       <!-- /.container -->
   </div>
   <!-- /.section -->
</div>
<!-- /#content -->
