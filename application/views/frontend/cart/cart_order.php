<input type="hidden" data-cart-delete-url value="<?php echo base_url('cart-delete'); ?>">
<input type="hidden" data-shipping-calculate-url value="<?php echo base_url('cart-shipping-calculate'); ?>">
<input type="hidden" data-order-save-url value="<?php echo base_url('cart-order-save'); ?>">
<input type="hidden" data-order-thankyou-url value="<?php echo base_url('koszonjuk'); ?>">
<div id="cms" class="page-contact">
    <div class="section gray-light">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="main">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="block block-shadow block-margin white contact">
                                    <div class="block-inner">
                                        <div>
                                          <ul class="nav nav-tabs order" role="tablist">
                                            <li role="presentation" class="active"><a href="#cart" aria-controls="cart" role="tab" data-toggle="tab"><span>1.lépés:</span> Kosár</a></li>
                                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" <?php if(!cart_empty()): ?> data-toggle="tab" <?php endif; ?>><span>2.lépés:</span> Megrendelő adatai</a></li>
                                            <li role="presentation"><a data-order-data href="#order" aria-controls="order" role="tab" <?php if(!cart_empty()): ?> data-toggle="tab" <?php endif; ?>><span>3.lépés:</span> Fizetés & Szállítás</a></li>
                                            <li role="presentation"><a data-order-summary href="#summary" aria-controls="summary" role="tab" <?php if(!cart_empty()): ?> data-toggle="tab" <?php endif; ?>><span>4.lépés:</span> Áttekintés & Megrendelés</a></li>
                                          </ul>
                                          <div class="tab-content tab-order">
                                            <div role="tabpanel" class="tab-pane active" id="cart">
                                                <br/>
                                                <div data-cart-content>
                                        			<?php echo cart_page();?>
                                        		</div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="profile">
                                                <h3 class="order-header">Számlázási adatok</h3>
                                                <form class='form-horizontal' id="form-orderdata">
                                                    <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="order_invlname" class="col-sm-4 control-label"><span class="required">*</span> Vezetéknév</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control" name="order_invlname" id="order_invlname" placeholder="Vezetéknév">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="order_invfname" class="col-sm-4 control-label"><span class="required">*</span> Keresztnév</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control" name="order_invfname" id="order_invfname" placeholder="Keresztnév">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="order_invzip" class="col-sm-4 control-label"><span class="required">*</span> Irányítószám</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control" name="order_invzip" id="order_invzip" placeholder="Irányítószám">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_invcity" class="col-sm-4 control-label"><span class="required">*</span> Város</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control" name="order_invcity" id="order_invcity" placeholder="Város">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_invstreet" class="col-sm-4 control-label"><span class="required">*</span> Utca/házszám</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control" name="order_invstreet" id="order_invstreet" placeholder="Cím (utca/házszám/emelet/ajtó)">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_invmobile" class="col-sm-4 control-label"><span class="required">*</span> Telefon</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control" name="order_invmobile" id="order_invmobile" placeholder="Telefonszám">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_email" class="col-sm-4 control-label"><span class="required">*</span> E-mail</label>
                                                            <div class="col-sm-8">
                                                                <input id="order_email" type="email" class="form-control" name="order_email" value="" placeholder="E-mail cím" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="order_company" class="col-sm-4 control-label">Cégnév</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control" name="order_invcompany" id="order_invcompany" placeholder="Cég">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_taxnumber" class="col-sm-4 control-label">Adószám</label>
                                                            <div class="col-sm-8">
                                                                <input name="order_taxnumber"  class="form-control" id="order_taxnumber" placeholder="Adószám" type="text"
                                                                 />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_comment" class="col-sm-4 control-label">Megjegyzés</label>
                                                            <div class="col-sm-8">
                                                                <textarea name="order_comment" id="order_comment" style="height: 200px; width: 100%;"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
</div>




                                                    <h3 class="order-header ship-header" data-show-delivery-click><input type="checkbox" data-show-delivery> A számlázási címtől eltérő postacím megadásához kattintson ide</h3>
                                                    <div data-delivery-data style="display: none">
                                                        <div class="form-group">
                                                            <label for="order_lname" class="col-sm-2 control-label"><span class="required">*</span> Vezetéknév</label>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control" name="order_lname" id="order_lname" placeholder="Vezetéknév">
                                                            </div>
                                                            <label for="order_company" class="col-sm-2 control-label">Cégnév</label>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control" name="order_company" id="order_company" placeholder="Cég">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_fname" class="col-sm-2 control-label"><span class="required">*</span> Keresztnév</label>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control" name="order_fname" id="order_fname" placeholder="Keresztnév">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_zip" class="col-sm-2 control-label"><span class="required">*</span> Irányítószám</label>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control" name="order_zip" id="order_zip" placeholder="Irányítószám">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_city" class="col-sm-2 control-label"><span class="required">*</span> Város</label>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control" name="order_city" id="order_city" placeholder="Város">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_street" class="col-sm-2 control-label"><span class="required">*</span> Utca/házszám</label>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control" name="order_street" id="order_street" placeholder="Cím (utca/házszám/emelet/ajtó)">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="order_mobile" class="col-sm-2 control-label"><span class="required">*</span> Telefon</label>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control" name="order_mobile" id="order_mobile" placeholder="Telefonszám">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <a href="#" class="btn btnPrev btn-warning pull-left">Vissza</a>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <button type="button" class="btn btn-success btnNext pull-right">Tovább</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="order">
                                                <h3 class="shipping-mode-header order-header" style="display: none">Szállítási mód</h3>
                                                <form class='form-horizontal' id="form-order">
                                                    <div data-shipping-mode></div>
                                                    <h3 class="order-header">Fizetési mód</h3>

                                                    <div class="form-group">

                                                        <div class="col-sm-10">
                                                            <label for="order_state" class="col-sm-2 control-label">Utánvét</label>
                                                            <div class="checkbox">
                                                              <label>
                                                                <input type="radio" name="billing_mod" data-billing-mod="utanvet" value="utanvet" checked>
                                                              </label>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-10">
                                                            <label for="order_state" class="col-sm-2 control-label">Előreutalás</label>
                                                            <div class="checkbox">
                                                              <label>
                                                                <input type="radio" name="billing_mod" data-billing-mod="utalas" value="utalas">
                                                              </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <a href="#" class="btn btnPrev btn-warning pull-left">Vissza</a>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <button type="button" data-order-summary  class="btn btnNext pull-right">Tovább</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="summary">
                                                <br/>
                                                <div class="alert alert-info" role="alert"><b><?php echo get_cfg('order_info'); ?></b></div>
                                                <div data-order-summary-html>
                                                </div>
                                                <form class='form-horizontal'>
                                                    <div class="form-group">
                                                        <label for="order_aszf" class="col-sm-2 control-label"></label>
                                                        <div class="col-sm-10">
                                                            <label>
                                                              <input type="checkbox" name="aszf" value="true"> Elolvastam és elfogadtam az <a href="<?php echo base_url('aszf') ?>">Általános Szerződési feltételeket</a>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="order_subscribe" class="col-sm-2 control-label"></label>
                                                        <div class="col-sm-10">
                                                            <label>
                                                              <input type="checkbox" name="order_subscribe" value="true">
                                                             Felíratkozom a hírlevélre
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <button type="button" class="btn btn-warning btnPrev pull-left">Vissza</button>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <button type="button" class="btn btn-success pull-right" data-order-save>Megrendelés</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.row -->
                    </div>
                    <!-- /#main -->
                </div>
                <!-- /.col-md-12 -->
           </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.section -->
</div>
