<input type="hidden" data-cart-delete-url value="<?php echo base_url('cart-delete'); ?>">
<div id="content" class="frontpage">
    <div class="section gray-light">
		<div class="container" data-cart-content>
			<?php echo cart_page();?>
		</div>
	</div>
</div>
