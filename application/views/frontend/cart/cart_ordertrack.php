<div id="cms" class="page-contact">
    <div class="section gray-light">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="main">
                        <div class="row">
                            <div class="block block-shadow block-margin white contact">
                                <div class="block-inner">
                                    <div class="row">
                                        <div class="col-md-12"><h2>Megrendelés  #<?php echo $order[0]->order_id; ?></h2></div>
                                        <div class="col-md-12">
                                            <div class="btn btn-primary pull-right" type="button" style="background: <?php echo $order[0]->ordertemplate_color;?>">
                                             Állapot: <?php echo $order[0]->ordertemplate_name;?>
                                            </div>
                                        </div>
                                    </div>
                                    <h3>Számlázási cím: </h3>
                                    <div class="row">
                                        <div class="col-md-2"><b>Megrendelő neve:</b></div>
                                        <div class="col-md-10"><?php echo $order[0]->order_invlname; ?> <?php echo $order[0]->order_invfname; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2"><b>Cím:</b></div>
                                        <div class="col-md-10"><?php echo $order[0]->order_invzip; ?> <?php echo $order[0]->order_invcity; ?>  <?php echo $order[0]->order_invstreet; ?></div>
                                    </div>
                                    <?php if(!empty($order[0]->order_invcompany)): ?>
                                    <div class="row">
                                        <div class="col-md-2"><b>Cég:</b></div>
                                        <div class="col-md-10"><?php echo $order[0]->order_invcompany; ?></div>
                                    </div>
                                    <?php endif; ?>
                                    <?php if(!empty($order[0]->order_taxnumber)): ?>
                                    <div class="row">
                                        <div class="col-md-2"><b>Adószám:</b></div>
                                        <div class="col-md-10"> <?php echo $order[0]->order_taxnumber; ?></div>
                                    </div>
                                    <?php endif; ?>
                                    <div class="row">
                                        <div class="col-md-2"><b>Email:</b></div>
                                        <div class="col-md-10"><?php echo $order[0]->order_email; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2"><b>Telefon:</b></div>
                                        <div class="col-md-10"><?php echo $order[0]->order_invmobile; ?></div>
                                    </div>
                                    <?php if($order[0]->delivery == 1): ?>
                                    <h3>Szállítási cím: </h3>
                                    <div class="row">
                                        <div class="col-md-2"><b>Megrendelő neve:</b></div>
                                        <div class="col-md-10"><?php echo $order[0]->order_lname; ?> <?php echo $order[0]->order_fname; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2"><b>Szállítási cím:</b></div>
                                        <div class="col-md-10"><?php echo $order[0]->order_zip; ?> <?php echo $order[0]->order_city; ?>  <?php echo $order[0]->order_street; ?></div>
                                    </div>
                                    <?php if(isset($order[0]->order_company)): ?>
                                    <div class="row">
                                        <div class="col-md-2"><b>Cég:</b></div>
                                        <div class="col-md-10"><?php echo $order[0]->order_company; ?></div>
                                    </div>
                                    <?php endif; ?>
                                    <div class="row">
                                        <div class="col-md-2"><b>Telefon:</b></div>
                                        <div class="col-md-10"><?php echo $order[0]->order_mobile; ?></div>
                                    </div>
                                    <?php endif; ?>
                                    <h3>Megrendelés adatai: </h3>
                                    <div class="row">
                                        <div class="col-md-2"><b>Megrendelés időpontja:</b></div>
                                        <div class="col-md-10"><?php echo $order[0]->order_date; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2"><b>Átvétel módja:</b></div>
                                        <div class="col-md-10">Futárszolgálat</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2"><b>Átvétel helye:</b></div>
                                        <div class="col-md-10">Kiszállításkor</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2"><b>Fizetési mód:</b></div>
                                        <div class="col-md-10"><?php echo billing($order[0]->billing);?> </div>
                                    </div>
                                    <h3>Megrendelt termékek: </h3>
                                    <table cellspacing="0" cellpadding="10" width="100%">
                                        <thead>
                                            <tr>
                                                <th style="font-size:14px;border-bottom:2px solid black" align="left">Termék</th>
                                                <th style="font-size:14px;border-bottom:2px solid black" align="left">Mennyiség</th>
                                                <th style="font-size:14px;border-bottom:2px solid black" align="left">Web shop ár</th>
                                                <th style="font-size:14px;border-bottom:2px solid black" align="left">Összesen</th>
                                            </tr>
                                        </thead>
                                        <tbody style="border:1px solid black">
                                        <?php $total_amount = ""; ?>
                                        <?php foreach($order as $item_data): ?>
                                        <?php $price = $item_data->item_price; ?>
                                                <tr>
                                                    <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left"><?php echo $item_data->item_name; ?></td>
                                                    <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left"><?php echo $item_data->item_qty; ?> db</td>
                                                    <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left"><?php echo number_format($price,0,'','.');?> Ft</td>
                                                    <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left"><?php echo number_format(($price*$item_data->item_qty),0,'','.');?> Ft</td>
                                                </tr>
                                                <?php
                                                    $total_amount = $total_amount + ($price * $item_data->item_qty);
                                                ?>
                                        <?php endforeach ?>
                                        <?php $total_amount = $total_amount + $order[0]->ship_price; ?>
                                            <tr>
                                                <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left"></td>
                                                <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left"></td>
                                                <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left">Szállítási költség:</td>
                                                <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left"><?php echo number_format($order[0]->ship_price,0,'','.');?> Ft</td>
                                            </tr>
                                            <tr>
                                                <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left"></td>
                                                <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left"></td>
                                                <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left">Fizetendő összesen:</td>
                                                <td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left"><?php echo number_format($total_amount,0,'','.');?> Ft</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- /.row -->
                    </div>
                    <!-- /#main -->
                </div>
                <!-- /.col-md-12 -->
           </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.section -->
</div>
