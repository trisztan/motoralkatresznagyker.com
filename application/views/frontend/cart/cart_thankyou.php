<div class="section gray-light">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="main" class="white block block-shadow block-margin white contact">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-header center">
                                <div class="page-header-inner">
                                    <div class="line">
                                        <hr> </div>
                                    <!-- /.line -->
                                    <div class="heading">
                                        <h2>Köszönjük</h2>
                                    </div>
                                    <!-- /.heading -->
                                    <div class="line">
                                        <hr>
                                    </div>
                                    <!-- /.line -->
                                </div>
                                <!-- /.page-header-inner -->
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <p>A megrendelését rögzítettük, a megrendelés részleteit e-mailben elküldtük.</p>
                                    <h3>Megrendelését az alábbi linkre kattintva követheti:</h3>
                                        <a href="<?php echo $order_track_url;?>"><?php echo $order_track_url;?></a>
                                    <br/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /#main -->
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
