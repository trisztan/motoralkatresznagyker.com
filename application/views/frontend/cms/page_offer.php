<input type="hidden" data-offer-url value="<?php echo base_url('offer-send'); ?>">
<div id="content" class="page-contact">
   <div class="section gray-light">
       <div class="container">
           <div class="row">
               <div class="col-md-12">
                   <div id="main" class="white block block-shadow block-margin white contact">
                       <div class="row">
                           <div class="col-md-12">
                               <div class="block">
                                   <div class="page-header center">
                                       <div class="page-header-inner">
                                           <div class="line">
                                               <hr>
                                           </div><!-- /.line -->

                                           <div class="heading">
                                               <h2>Ajánlatkérés</h2>
                                           </div><!-- /.heading -->

                                           <div class="line">
                                               <hr>
                                           </div><!-- /.line -->
                                       </div><!-- /.page-header-inner -->
                                   </div>

                                   <form data-offer-form>

                                       <div class="form-inline">
                                           <div class="form-group col-sm-3 col-md-3">
                                               <label>Teljes név</label>
                                               <input type="text" name="offer_name" id="offer_name" class="form-control">
                                           </div>
                                           <div class="form-group col-sm-3 col-md-3">
                                               <label>E-mail cím</label>
                                               <input type="text" name="offer_email" id="offer_email" class="form-control">
                                           </div>
                                           <div class="form-group col-sm-3 col-md-3">
                                               <label>Telefonszám</label>
                                               <input type="text" name="offer_phone" id="offer_phone" class="form-control">
                                           </div>
                                           <div class="form-group col-sm-3 col-md-3">
                                               <label>Gyártó, Típus, Cm3, Évjárat</label>
                                               <input type="text" name="offer_title" id="offer_title" class="form-control">
                                           </div>
                                       </div>

                                       <div class="textarea form-group col-sm-12 col-md-12">
                                           <label>Üzenet</label>
                                           <textarea class="form-control" name="offer_message" id="offer_message"></textarea>
                                       </div>

                                       <div class="form-group col-md-3">
                                           <button class="btn btn-primary btn-block" type="button" data-offer-send>Küldés</button>
                                       </div>
                                   </form>
                               </div>
                            </div>
                       </div>
                   </div>
                   <!-- /#main -->
               </div>
               <!-- /.col-md-12 -->
          </div>
           <!-- /.row -->
       </div>
       <!-- /.container -->
   </div>
   <!-- /.section -->
</div>
<!-- /#content -->
