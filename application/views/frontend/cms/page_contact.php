<input type="hidden" data-contact-url value="<?php echo base_url('contact-send'); ?>">
<div id="content" class="page-contact">
   <div class="section gray-light">
       <div class="container">
           <div class="row">
               <div class="col-md-12">
                   <div id="main" class="white block block-shadow block-margin white contact">
                       <div class="row">
                           <div class="col-md-12">
                               <div class="page-header center">
                                   <div class="page-header-inner">
                                       <div class="line">
                                           <hr>
                                       </div><!-- /.line -->

                                       <div class="heading">
                                           <h2>Lépjen kapcsolatba velünk</h2>
                                       </div><!-- /.heading -->

                                       <div class="line">
                                           <hr>
                                       </div><!-- /.line -->
                                   </div><!-- /.page-header-inner -->
                               </div>
                           </div>
                           <div class="col-md-6">
                               <div class="block">
                                   <form data-contact-form>
                                       <div class="form-inline">
                                           <div class="form-group col-md-12">
                                               <label>Teljes név</label>
                                               <input type="text" name="contact_name" id="contact_name" class="form-control">
                                           </div>
                                           <div class="form-group col-md-6">
                                               <label>E-mail cím</label>
                                               <input type="text" name="contact_email" id="contact_email" class="form-control">
                                           </div>
                                           <div class="form-group col-md-6">
                                               <label>Telefonszám</label>
                                               <input type="text" name="contact_phone" id="contact_phone" class="form-control">
                                           </div>
                                           <div class="textarea form-group col-sm-12 col-md-12">
                                               <label>Milyen ügyben keres minket?</label>
                                               <textarea class="form-control" name="contact_message" id="contact_message"></textarea>
                                           </div>

                                           <div class="form-group col-md-3">
                                               <button class="btn btn-primary btn-block" type="button" data-contact-send>Küldés</button>
                                           </div>
                                       </div>
                                   </form>
                               </div>
                            </div>
                            <div class="col-md-6">
                                <div class="block block-margin white contact">
                                    <div class="block-inner">
                                        <div class="block-title">
                                            <h3>
                                                Elérhetőség
                                            </h3>
                                        </div>
                                            <p><?php echo get_cfg('company');?></p>
                                            <p><i class="icon icon-normal-pointer-pin"></i> <?php echo get_cfg('company_address');?></p>
                                            <p><i class="icon icon-normal-phone"></i> Telefon:</strong> <?php echo get_cfg('company_phone');?></p>
                                            <p><i class="icon icon-normal-mail"></i> E-mail:</strong> <?php echo get_cfg('email');?></p>
                                        </p>
                                    </div>
                                </div>
                                <div class="block block-margin white about">
                                    <div class="block-inner">
                                        <div class="block-title">
                                            <h3>
                                                Rólunk
                                            </h3>
                                        </div>

                                        <div class="description">
                                            <?php echo get_cfg('about_us_short');?>
                                        </div>

                                        <div class="social">
                                            <div class="inner">
                                                <ul class="social-links">
                                                    <li class="social-icon google-plus"><a href="#">Google+</a></li>
                                                    <li class="social-icon twitter"><a href="#">Twitter</a></li>
                                                    <li class="social-icon pinterest"><a href="#">Pinterest</a></li>
                                                    <li class="social-icon facebook"><a href="#">Facebook</a></li>
                                                </ul><!-- /.social-links -->
                                            </div><!-- /.inner -->
                                        </div><!-- /.social -->

                                    </div>
                                </div>

                            </div>
                       </div>
                   </div>
                   <!-- /#main -->
               </div>
               <!-- /.col-md-12 -->
          </div>
           <!-- /.row -->
       </div>
       <!-- /.container -->
   </div>
   <!-- /.section -->
</div>
<!-- /#content -->
