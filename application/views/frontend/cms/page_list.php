<div id="cms" class="page-contact">
    <div class="section gray-light">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="main">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                            <?php if(!empty($cms->cms_content)):?>
                                <div class="block block-shadow block-margin white contact">
                                    <div class="block-inner content-size">
                                        <?php echo $cms->cms_content; ?>
                                    </div>
                                </div>
                                <?php else:?>
                                <div class="alert alert-info">
                                    <strong>Nincs tartalom</strong><br/> A tartalom feltöltés alatt.
                                </div>
                                <?php endif;?>
                            </div>
                        </div><!-- /.row -->
                    </div>
                    <!-- /#main -->
                </div>
                <!-- /.col-md-12 -->
           </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.section -->
</div>
