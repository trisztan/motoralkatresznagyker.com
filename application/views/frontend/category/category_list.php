<input type="hidden" data-item-datatable-url value="<?php echo base_url('home-datatable')?>">
<div id="content" class="frontpage">
    <div class="section gray-light">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                        <div class="sidebar">
                            <div class="latest-reviews block white">
                                <div class="block-inner">
                                    <div class="block-title">
                                        <h3><i class="fa fa-cog"></i> <span style="margin-top:-10px">Termék kategóriák</span></h3>
                                    </div>
                                    <!-- /.block-title -->
                                    <div class="inner">
                                        <div class="row">
                                            <ul class="nav nav-pills nav-stacked">
                                                <?php foreach($categories_main as $cat_main):?>
                                                <li><a href="<?php echo base_url($cat_main->slug);?>"><?php echo $cat_main->kategoria; ?></a></li>
                                                <?php endforeach;?>
                                            </ul>
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                    <!-- /.inner -->
                                </div>
                                <!-- /.block-inner -->
                            </div>
                            <!-- /.block -->
                        <div class="latest-reviews block block-shadow white">
                            <div class="block-inner">
                                <div class="block-title">
                                    <h3>Akciós termékeink</h3>
                                </div>
                                <!-- /.block-title -->
                                <div class="inner">
                                    <div class="row">
                                    <?php if(!empty($product_sales)):?>
                                     <?php foreach($product_sales as $product_sale):?>
                                        <div class="item-wrapper col-lg-12 col-md-12 col-sm-4">
                                            <div class="item">
                                                <div class="picture hidden-sm">
                                                     <?php if(!empty($product_sale['images'])):?>
                                                  <?php foreach($product_sale['images'] as $image):?>
                                                    <a href="<?php echo base_url($image->image_url);?>" class="slide">
                                                    <img height="100px" widht="100px" src="<?php echo base_url($image->image_url);?>" alt="#">
                                                    </a><!-- /.slide -->
                                                    <?php break;?>
                                                    <?php endforeach;?>
                                                    <?php else:?>
                                                    <img src="<?php echo base_url('template/frontend/assets/img/no-image-100.jpg');?>" alt="<?php echo $product_sale['item_sku']."-".$product_sale['item_name']?>">
                                                    <?php endif;?>

                                                </div>
                                                <!-- /.picture -->
                                                <div class="title">
                                                    <a href="<?php echo base_url($product_sale['item_slug']);?>"><?php echo $product_sale['item_name']?></a>
                                                </div>
                                                <div class="price"><?php echo price_calculator($product_sale['item_netprice'],$product_sale['item_netpricesale'],$product_sale['item_tax']);?> Ft</div>
                                                <!-- /.title -->
                                                <div class="date"><?php echo $product_sale['item_sku'];?></div>
                                                <!-- /.date -->
                                                <div class="description">
                                                    <p>
                                                       <?php echo word_limiter($product_sale['item_longdesc'],10);?>
                                                    </p>
                                                </div>
                                                <!-- /.description -->
                                            </div>
                                            <!-- /.item -->
                                        </div>
                                    <?php endforeach;?>
                                    <?php else:?>
                                    <div class="col-md-12">
                                        <div class="alert alert-info" role="alert">Jelenleg nincs akciós termék</div>
                                    </div>
                                    <?php endif;?>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.inner -->
                            </div>
                            <!-- /.block-inner -->
                        </div>
                        <div class='block default white'>
                            <div class="block-inner">
                                <div class="fb-page" data-href="https://www.facebook.com/motoralkatresz.nagyker/" data-tabs="timeline" data-height="130" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/motoralkatresz.nagyker/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/motoralkatresz.nagyker/">Motoralkatrész Nagyker</a></blockquote></div>
                            </div>
                        </div>
                        <!-- /.block -->
                    </div>
                    <!-- /.sidebar -->
                </div>
                <div class="col-md-9 col-sm-12">
                            <div class="row">
                <!-- /.col-md-3 -->
                <div class="col-md-12 col-sm-12">
                    <div class="sidebar">
                        <div class="latest-reviews block block-shadow white">
                            <div class="block-inner">
                                <div class="block-title">
                                    <h3><strong><?php echo where();?></strong> <span class="pull-right">
                                    <button type="button" class="btn btn-default btn-sm back">
                                      <span class="glyphicon glyphicon-chevron-left"></span> Vissza
                                    </button></span></h3>
                                </div>
                                <div id="bc1" class="btn-group btn-breadcrumb" style="margin-bottom: 20px;">
                                    <a href="<?php echo base_url();?>" class="btn btn-default"><i class="fa fa-home"></i></a>
                                    <?php echo category_breadcrumb(); ?>
                                </div>

                                <!-- /.block-title -->
                                <div class="inner">
                                    <div class="row">
                                    <?php if(!empty($categories) && ($categories[0]->cnt != 0)):?>
                                    <?php foreach($categories as $category):?>
                                        <?php if(!empty($category->kategoria)):?>
                                        <div class="col-md-4">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                <?php if(file_exists(FCPATH.'upload/category/'.create_slug($category->kategoria).'.jpg')):?>
                                                    <a href="<?php echo base_url($category->slug);?>">
                                                <img style="height: 50px; width: 50px;" src="<?php echo base_url('upload/category/'.create_slug($category->kategoria).'.jpg');?>">
                                            </a>
                                                  <?php endif;?>
                                              <a href="<?php echo base_url($category->slug);?>"><?php echo $category->kategoria;?></a><span class="badge"><?php echo $category->cnt;?> db</span></li>
                                            </ul>
                                        </div>
                                        <?php else:?>
                                        <div class="col-md-12">
                                            <div class="alert alert-info" role="alert">Nincs több választási lehetőség. A vissza gombal bármikor visszaléphetsz.</div>
                                        </div>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                    <?php else:?>
                                    <div class="col-md-12">
                                        <div class="alert alert-info" role="alert">Nincs több választási lehetőség. A vissza gombal bármikor visszaléphetsz.</div>
                                    </div>
                                    <?php endif;?>
                                    <!-- /.row -->
                                </div>
                                <!-- /.inner -->
                            </div>
                            <!-- /.block-inner -->
                        </div>
                        <!-- /.block -->
                    </div>
                    <!-- /.sidebar -->
                    <?php if(where() != 'Válassz alkategóriát'):?>
                    <div id="main">
                        <div class="row-block block" id="best-deals">
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="item" class="table table-bordered table-striped">
                                      <thead>
                                        <tr>
                                          <th></th>
                                          <th>Termék név</th>
                                          <th>Ár</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                    <!-- /.content -->
                                </div>
                                <!-- /.col-md-12 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.block -->
                    </div>
                    <?php endif;?>
                    <!-- /#main -->
                </div>
            </div>
                </div>
            </div>

            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.section -->
</div>
</div>
<!-- /#content -->
