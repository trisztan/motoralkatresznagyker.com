<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>

<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf" value="<?php echo $this->security->get_csrf_hash(); ?>" data-csrf>
    <link rel="stylesheet" href="<?php echo base_url('template/frontend/libraries/jquery-ui-1.12.0/jquery-ui.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('template/frontend/libraries/jquery-ui-1.12.0/jquery-ui.theme.min.css'); ?>">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('template/frontend/assets/img/favicon/apple-icon-57x57.png');?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('template/frontend/assets/img/favicon/apple-icon-60x60.png');?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('template/frontend/assets/img/favicon/apple-icon-72x72.png');?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('template/frontend/assets/img/favicon/apple-icon-76x76.png');?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('template/frontend/assets/img/favicon/apple-icon-114x114.png');?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('template/frontend/assets/img/favicon/apple-icon-120x120.png');?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('template/frontend/assets/img/favicon/apple-icon-144x144.png');?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('template/frontend/assets/img/favicon/apple-icon-152x152.png');?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('template/frontend/assets/img/favicon/apple-icon-180x180.png');?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url('template/frontend/assets/img/favicon/android-icon-192x192.png');?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('template/frontend/assets/img/favicon/favicon-32x32.png');?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('template/frontend/assets/img/favicon/favicon-96x96.png');?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('template/frontend/assets/img/favicon/favicon-16x16.png');?>">
    <link rel="manifest" href="<?php echo base_url('template/frontend/assets/img/favicon/manifest.json');?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url('template/frontend/assets/img/favicon/ms-icon-144x144.png');?>">
    <meta name="theme-color" content="#ffffff">
    <?php echo get_cfg('site_header');?>
    <?php
        if(thankyou() == true) {
            $thankyou_header = get_cfg('thankyou_header');
            $thankyou_header = str_replace('#price',$total_price,$thankyou_header);
            echo $thankyou_header;
        }
    ?>
    <?php $this->enqueue->loadcss(); ?>
    <?php if (!empty($css)): ?>
    <?php foreach ($css as $cs): ?>
    <?php if (strpos($cs, '://') !== false): ?>
      <link rel="stylesheet" href="<?php echo $cs; ?>">
    <?php else: ?>
      <link rel="stylesheet" href="<?php echo base_url($cs); ?>">
    <?php endif;?>
    <?php endforeach;?>
    <?php endif;?>
    <link rel="stylesheet" href="<?php echo base_url('template/frontend/libraries/cdwharton-cookie/cookiecuttr.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('template/frontend/libraries/lightbox/css/lightbox.min.css'); ?>">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,400,700,400italic,700italic" rel="stylesheet" type="text/css"  media="screen, projection">
    <title><?php echo $title; ?></title>
</head>
<body>
<input type="hidden" data-base-url value="<?php echo base_url() ?>">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/hu_HU/sdk.js#xfbml=1&version=v2.8&appId=342141409191570";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php echo get_cfg('site_body');?>
<?php
    if(thankyou() == true) {
        $thankyou_body = get_cfg('thankyou_body');
        $thankyou_body = str_replace('#price',$total_price,$thankyou_body);
        echo $thankyou_body;
    }
?>
<header id="header">
    <input type="hidden" data-csrf value="<?php echo $this->security->get_csrf_hash(); ?>">
    <input type="hidden" data-add-to-cart-url value="<?php echo base_url('add-to-cart'); ?>">
    <input type="hidden" data-cart-delete-url value="<?php echo base_url('cart-delete');?>">
    <input type="hidden" data-cart-qty-url value="<?php echo base_url('cart-update');?>">
    <input type="hidden" data-item-search-url value="<?php echo base_url('item-search');?>">
    <div class="header-inner">
        <div class="container">
            <div class="row">
                <div class="col-md-12 clearfix">
                    <div class="brand">
						<div class="logo">
							<a href="<?php echo base_url();?>">
								<img src="<?php echo base_url('template/frontend/assets/img/logo.png');?>" alt="MOTORALKATRÉSZNAGYKER">
							</a>
						</div><!-- /.logo -->
					</div>

                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <nav class="collapse navbar-collapse navbar-collapse" role="navigation">
                        <ul class="navigation">
                        <li><a href="<?php echo base_url();?>">Főoldal</a></li>
                        <?php if(header_menu()):?>
                        <?php foreach(header_menu() as $menu):?>
                        <li><a href="<?php echo base_url($menu->router_slug);?>"><?php echo $menu->router_pagename;?></a></li>
                        <?php endforeach;?>
                        <?php endif;?>
                        </ul><!-- /.nav -->
                    </nav>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.header-inner -->
</header><!-- /#header -->
<div class="infobar">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="input-group search-group">
                   <input type="text" class="form-control ui-autocomplete" id="item-search" placeholder="Nyomja meg a keresés gombot a kereséshez">
                   <span class="input-group-addon search-icon-red"><i class="fa fa-search fa-2x"></i></button></span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="contact pull-right">
                    <?php if(openhours() == true): ?>
                    <div class="contact-item phone">
                        <div class="label"><i class="icon icon-normal-mobile-phone"></i></div><!-- /.label -->
                        <div class="value"><?php echo get_cfg('mobile');?></div><!-- /.value -->
                    </div><!-- /.phone -->
                    <?php endif; ?>
                    <div class="contact-item mail">
                        <div class="label"><i class="icon icon-normal-mail"></i></div><!-- /.label -->
                        <div class="value"><a href="mailto:<?php echo get_cfg('email');?>"><?php echo get_cfg('email');?></a></div><!-- /.value -->
                    </div><!-- /.mail -->
                </div>
            </div>
            <div class="col-md-2">
                <div class="contact pull-right">
                    <div class="contact-item infobar-cart">
                        <div class="label"><i class="icon icon-normal-cart"></i><span class="cart-price-total"><?php echo cart_total();?></span></div>
                        <div class="dropdown value">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kosár <span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-cart">
                        <?php echo cart_content();?>
                        </ul>
                      </div>
                    </div><!-- /.mail -->
                </div><!-- /.contact -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.infobar -->
