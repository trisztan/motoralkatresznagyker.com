<input type="hidden" data-item-datatable-url value="<?php echo base_url('home-datatable')?>">
<div id="content" class="frontpage">
    <div class="section gray-light">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="sidebar">

                            <div class="latest-reviews block white">
                                <div class="block-inner">
                                    <div class="block-title">
                                        <h3><i class="fa fa-cog"></i> <span style="margin-top:-10px">Termék kategóriák</span></h3>
                                    </div>
                                    <!-- /.block-title -->
                                    <div class="inner">
                                        <div class="row">
                                            <ul class="nav nav-pills nav-stacked">
                                                <?php foreach($categories as $category):?>
                                                <li><a href="<?php echo base_url($category->slug);?>"><?php echo $category->kategoria; ?></a></li>
                                                <?php endforeach;?>
                                            </ul>
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                    <!-- /.inner -->
                                </div>
                                <!-- /.block-inner -->
                            </div>
                            <!-- /.block -->
                        <div class="latest-reviews block white">
                            <div class="block-inner">
                                <div class="block-title">
                                    <h3>Akciós termékeink</h3>
                                </div>
                                <!-- /.block-title -->
                                <div class="inner">
                                    <div class="row">
                                    <?php if(!empty($product_sales)):?>
                                     <?php foreach($product_sales as $product_sale):?>
                                        <div class="item-wrapper col-lg-12 col-md-12 col-sm-4">
                                            <div class="item">
                                                <div class="picture hidden-sm">
                                                     <?php if(!empty($product_sale['images'])):?>
                                                  <?php foreach($product_sale['images'] as $image):?>
                                                    <a href="<?php echo base_url($image->image_url);?>" class="slide">
                                                    <img height="100px" widht="100px" src="<?php echo base_url($image->image_url);?>" alt="#">
                                                    </a><!-- /.slide -->
                                                    <?php break;?>
                                                    <?php endforeach;?>
                                                    <?php else:?>
                                                    <img src="<?php echo base_url('template/frontend/assets/img/no-image-100.jpg');?>" alt="<?php echo $product_sale['item_sku']."-".$product_sale['item_name']?>">
                                                    <?php endif;?>

                                                </div>
                                                <!-- /.picture -->
                                                <div class="title">
                                                    <a href="<?php echo base_url($product_sale['item_slug']);?>"><?php echo $product_sale['item_name']?></a>
                                                </div>
                                                <div class="price"><?php echo price_calculator(
                                         $product_sale['item_netprice'],
                                         $product_sale['item_netpricesale'],
                                         $product_sale['item_tax']);?> Ft</div>
                                                <!-- /.title -->
                                                <div class="date"><?php echo $product_sale['item_sku'];?></div>
                                                <!-- /.date -->
                                                <div class="description">
                                                    <p>
                                                       <?php echo word_limiter($product_sale['item_longdesc'],10);?>
                                                    </p>
                                                </div>
                                                <!-- /.description -->
                                            </div>
                                            <!-- /.item -->
                                        </div>
                                    <?php endforeach;?>
                                    <?php else:?>
                                        <div class="col-md-12">
                                            <div class="alert alert-info" role="alert">Jelenleg nincs akciós termék</div>
                                        </div>
                                    <?php endif;?>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.inner -->
                            </div>
                            <!-- /.block-inner -->
                        </div>
                        <!-- /.block -->
                        <div class='block default white'>
                            <div class="block-inner">
                                <div class="fb-page" data-href="https://www.facebook.com/motoralkatresz.nagyker/" data-tabs="timeline" data-height="130" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/motoralkatresz.nagyker/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/motoralkatresz.nagyker/">Motoralkatrész Nagyker</a></blockquote></div>
                            </div>
                        </div>
                    </div>
                    <!-- /.sidebar -->
                </div>
                <div class="col-md-9 col-sm-12">
                    <div class="row">
		                <!-- /.col-md-3 -->
		                <div class="col-md-12 col-sm-12">
		                    <div class="sidebar">
		                        <div class="latest-reviews block block-shadow white">
		                            <div class="block-inner">
		                                <div class="block-title">
		                                    <h3><strong>Milyen alkatrészt szeretnél a motorodhoz?</strong></h3>
		                                </div>
		                                <!-- /.block-title -->
		                                <div class="inner">
		                                    <div class="row">
		                                    <?php foreach($categories as $category):?>
		                                        <div class="col-md-4">
		                                            <ul class="list-group">
		                                              <li class="list-group-item">
                                                          <?php if(file_exists(FCPATH.'upload/category/'.$category->slug.'.jpg')):?>
                                                              <a href="<?php echo base_url($category->slug);?>">
                                                          <img style="height: 50px; width: 50px;" src="<?php echo base_url('upload/category/'.$category->slug.'.jpg');?>">
                                                      </a>
                                                            <?php endif;?>
                                                          <a href="<?php echo base_url($category->slug);?>"><?php echo $category->kategoria;?></a><span class="badge"><?php echo $category->cnt;?> db</span></li>
		                                            </ul>
		                                        </div>
		                                    <?php endforeach;?>
		                                    <!-- /.row -->
		                                	</div>
		                                <!-- /.inner -->
		                            	</div>
		                            <!-- /.block-inner -->
		                        	</div>
		                        <!-- /.block -->
		                   	 	</div>
		                    <!-- /.sidebar -->
                            <?php if(!empty($search)):?>
                            <h3><i class="fa fa-search"></i> <span style="margin-top:-10px">Keresési eredménye: <?php echo $search; ?> kifejezésre</span></h3>
                            <input type="hidden" data-search-string value="<?php echo $search; ?>">
		                    <div id="main">
		                        <div class="row-block block" id="best-deals">
		                            <div class="row">
		                                <div class="col-md-12">
		                                    <table id="item" class="table table-bordered table-striped">
		                                      <thead>
		                                        <tr>
                                                    <th></th>
                                                    <th>Termék név</th>
                                                    <th>Ár</th>
                                                    <th></th>
		                                        </tr>
		                                      </thead>
		                                      <tbody>

		                                      </tbody>
		                                    </table>

		                                </div>
		                            </div>
		                        </div>
		                    </div>
                            <?php endif;?>
		                    <!-- /#main -->
		                </div>
           		 	</div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.section -->
</div>
</div>
<!-- /#content -->
