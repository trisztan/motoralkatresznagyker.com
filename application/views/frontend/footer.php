<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<footer id="footer">
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12 clearfix">
                    <div class="copyright">
                        Készítette:<span class="separator"></span> <a href="http://deweb.hu"><img src="<?php echo base_url('template/frontend/assets/img/deweb.png');?>"></a> <span class="separator"></span>&copy;  Minden jog fenntartva.
                    </div><!-- /.pull-left -->

                    <ul class="nav nav-pills">
                        <?php if (footer_menu()):?>
                        <?php foreach (footer_menu() as $menu):?>
                        <li><a href="<?php echo base_url($menu->router_slug);?>"><?php echo $menu->router_pagename;?></a></li>
                        <?php endforeach;?>
                        <?php endif;?>
                    </ul><!-- /.nav -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.footer-bottom -->
</footer><!-- /#footer -->
<!-- asd -->
    <?php $this->enqueue->loadjs(); ?>
<!-- asd -->
<?php if (!empty($js)):?>
<?php foreach ($js as $j):?>
  <?php if (strpos($j, '://') !== false):?>
     <script src="<?php echo $j; ?>"></script>
    <?php else:?>
<script src="<?php echo base_url($j); ?>"></script>
 <?php endif;?>
<?php endforeach;?>
<?php endif; ?>
<script>
$(document).ready(function(){
    $('button.back').click(function(){
        parent.history.back();
        return false;
    });
});
</script>
<?php
    if(thankyou() == true) {
        $thankyou_footer = get_cfg('thankyou_footer');
        $thankyou_footer = str_replace('#price',$total_price,$thankyou_footer);
        echo $thankyou_footer;
    }
?>
<script src="<?php echo base_url('template/frontend/libraries/jquery-ui-1.12.0/jquery-ui.min.js'); ?>"></script>
<script src="<?php echo base_url('template/frontend/libraries/jquery-cookie-master/jquery.cookie.js'); ?>"></script>
<script src="<?php echo base_url('template/frontend/libraries/cdwharton-cookie/jquery.cookiecuttr.js'); ?>"></script>
<script src="<?php echo base_url('template/frontend/libraries/lightbox/js/lightbox.min.js'); ?>"></script>
<script>
$(document).ready(function () {
    $.cookieCuttr({
        cookieAnalytics: false,
        cookieAcceptButtonText: 'Értem',
        cookieMessage: '<?php echo get_cfg('cookie_text')?>',
        cookiePolicyLink: '<?php echo $this->config->item('base_url') ?>'
    });

    /*$('#item-search').autocomplete({
        source: function(request, response) {
            $.post($('[data-item-search-url]').val(), {
                "search": request.term,
                "validator_token": $('[data-csrf]').attr('value')
            }, function(result) {
                result = JSON.parse(result);
                response(result.search_result);
                $('[data-csrf]').attr('value', result.csrf);
            });
        },
        focus: function (event, ui) {
				$(event.target).val(ui.item.label);
				return false;
		},
		select: function (event, ui) {
			$(event.target).val(ui.item.label);
			window.location = ui.item.value;
			return false;
		},
        minLength: 3
    });*/
    $('.search-icon-red').css( 'cursor', 'pointer' );

    /* Kupon szerkesztés */
    $(document).on("click", ".search-icon-red", function () {
        var text = $('#item-search').val();
        var burl = $('[data-base-url]').val();
        var redir = burl+'kereses/'+text;
        window.location.href = redir;
    });

    /*$(window).resize(function() {
        ellipses1 = $("#bc1 :nth-child(2)")
        if ($("#bc1 a:hidden").length >0) {ellipses1.show()} else {ellipses1.hide()}
    })*/
});
</script>
<?php echo get_cfg('site_footer');?>
</body>
</html>
