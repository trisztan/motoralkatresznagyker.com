<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('get_fullname_by_email'))
{

	function get_fullname_by_email()
	{
		$ci =& get_instance();
        $email = base64_decode($ci->session->userdata('user'));
        $ci->db->select('user_firstname, user_lastname'); 
        $ci->db->where('user_email',$email);
        $query = $ci->db->get('user');
        return $query->num_rows() > 0 ? $query->result()[0]->user_lastname." ".$query->result()[0]->user_firstname : false;
	}
}
?>