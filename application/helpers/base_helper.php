<?php
defined('BASEPATH') OR exit('No direct script access allowed');


if (!function_exists('where')) {
	function where() {
		$ci = &get_instance();
		$slug = $ci->uri->segment(1);

        $query = $ci->db->query("SELECT slug FROM category_import WHERE slug = '".$slug."' LIMIT 1");
        if ($query->num_rows() > 0) {
            return "Válassz alkategóriát";
        }

		$query = $ci->db->query("SELECT slug FROM category_import WHERE slug2 = '".$slug."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return "Válassz gyártót";
		}

		$query = $ci->db->query("SELECT slug FROM category_import WHERE slug3 = '".$slug."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return "Válassz cm³";
		}

		$query = $ci->db->query("SELECT slug FROM category_import WHERE slug4 = '".$slug."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return "Válassz típust";
		}

		$query = $ci->db->query("SELECT slug FROM category_import WHERE slug5 = '".$slug."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return "Válassz évjáratot";
		}

		$query = $ci->db->query("SELECT slug FROM category_import WHERE slug6 = '".$slug."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return "Nincs több választási lehetőség";
		}
	}
}

if (!function_exists('category_breadcrumb')) {
	function category_breadcrumb() {
		$ci = &get_instance();
		$slug = $ci->uri->segment(1);
		$html = '';
        $query = $ci->db->query("SELECT slug FROM category_import WHERE slug = '".$slug."' LIMIT 1");
        if ($query->num_rows() > 0) {
			$html .= '<a href="#" class="btn btn-default"><div>Kategória</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Alkategória</div></a>';
            return $html;
        }

		$query = $ci->db->query("SELECT slug FROM category_import WHERE slug2 = '".$slug."' LIMIT 1");
		if ($query->num_rows() > 0) {
			$html .= '<a href="#" class="btn btn-default"><div>Kategória</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Alkategória</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Gyártó</div></a>';
			return $html;
		}

		$query = $ci->db->query("SELECT slug FROM category_import WHERE slug3 = '".$slug."' LIMIT 1");
		if ($query->num_rows() > 0) {
			$html .= '<a href="#" class="btn btn-default"><div>Kategória</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Alkategória</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Gyártó</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>cm³</div></a>';

			return $html;
		}

		$query = $ci->db->query("SELECT slug FROM category_import WHERE slug4 = '".$slug."' LIMIT 1");
		if ($query->num_rows() > 0) {
			$html .= '<a href="#" class="btn btn-default"><div>Kategória</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Alkategória</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Gyártó</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>cm³</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Típus</div></a>';

			return $html;
		}

		$query = $ci->db->query("SELECT slug FROM category_import WHERE slug5 = '".$slug."' LIMIT 1");
		if ($query->num_rows() > 0) {
			$html .= '<a href="#" class="btn btn-default"><div>Kategória</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Alkategória</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Gyártó</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>cm³</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Típus</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Évjárat</div></a>';

			return $html;
		}else {
			$html .= '<a href="#" class="btn btn-default"><div>Kategória</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Alkategória</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Gyártó</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>cm³</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Típus</div></a>';
			$html .= '<a href="#" class="btn btn-default"><div>Évjárat</div></a>';

			return $html;
		}
	}
}

if (!function_exists('category_title')) {
	function category_title() {
		$ci = &get_instance();
		$slug = $ci->uri->segment(1);

        $query = $ci->db->query("SELECT kategoria FROM category_import WHERE slug = '".$slug."' LIMIT 1");
        if ($query->num_rows() > 0) {
            return strtolower($query->result()[0]->kategoria);
        }

		$query = $ci->db->query("SELECT kategoria, alkategoria FROM category_import WHERE slug2 = '".$slug."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return strtolower($query->result()[0]->kategoria.", ".$query->result()[0]->alkategoria);
		}

		$query = $ci->db->query("SELECT kategoria, alkategoria,gyarto FROM category_import WHERE slug3 = '".$slug."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return strtolower($query->result()[0]->kategoria.", ".$query->result()[0]->alkategoria.", ".$query->result()[0]->gyarto);
		}

		$query = $ci->db->query("SELECT kategoria, alkategoria, gyarto, ccm FROM category_import WHERE slug4 = '".$slug."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return strtolower($query->result()[0]->kategoria.", ".$query->result()[0]->alkategoria.", ".$query->result()[0]->gyarto.", ".$query->result()[0]->ccm);
		}

		$query = $ci->db->query("SELECT kategoria, alkategoria, gyarto, ccm, tipus FROM category_import WHERE slug5 = '".$slug."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return strtolower($query->result()[0]->kategoria.", ".$query->result()[0]->alkategoria.", ".$query->result()[0]->gyarto.", ".$query->result()[0]->ccm.", ".$query->result()[0]->tipus);
		}

		$query = $ci->db->query("SELECT kategoria, alkategoria, gyarto, ccm, tipus, evjarat FROM category_import WHERE slug6 = '".$slug."' LIMIT 1");
		if ($query->num_rows() > 0) {
			return strtolower($query->result()[0]->kategoria.", ".$query->result()[0]->alkategoria.", ".$query->result()[0]->gyarto.", ".$query->result()[0]->ccm.", ".$query->result()[0]->tipus.", ".$query->result()[0]->evjarat);
		}
	}
}

if (!function_exists('pre')) {
	function pre($array = array('Empty')) {
		print '<pre>';
		print_r($array);
		print '</pre>';
	}
}

if (!function_exists('title')) {
	function title($title = '') {
        return (!empty($title) ? $title:"Főoldal")." ". get_cfg('title_seperator')." ".get_cfg('home_title');
	}
}

if (!function_exists('thankyou')) {
	function thankyou() {
		$ci = &get_instance();
		$slug = $ci->uri->segment(1);
		return ($slug == 'koszonjuk') ? true : false;
	}
}

if (!function_exists('get_cfg')) {

	function get_cfg($key) {
		$ci = &get_instance();
		$ci->db->where('config_key',$key);
		$query = $ci->db->get('config');
        return $query->num_rows() > 0 ? $query->result()[0]->config_value : false;
	}
}

if (!function_exists('csrf')) {
	function csrf() {
		$ci = &get_instance();
		return $ci->security->get_csrf_hash();
	}
}

if (!function_exists('breadcrumb')) {

	function breadcrumb() {
		$ci = &get_instance();
		$slug = $ci->uri->segment(1);
		$ci->db->where('router_slug',$slug);
		$query = $ci->db->get('router');
        return $query->num_rows() > 0 ? $query->result()[0]->router_pagename : "";
	}
}
function send_email_alert($order_id, $comment = "") {

	$ci = &get_instance();

	$query = $ci->db->query("SELECT o.*,oi.*,ot.*,os.*,i.item_sku,i.item_name FROM `order` o
		LEFT JOIN order_item oi ON oi.order_id = o.order_id
		LEFT JOIN order_template ot ON o.status_id = ot.ordertemplate_id
		LEFT JOIN item i ON oi.item_sku = i.item_sku
		LEFT JOIN order_ship os ON os.order_id = o.order_id
		WHERE o.order_id = '".$order_id."'");

	$data['order_items'] = $query->num_rows() > 0 ? $query->result_array() : false;
	$order = $data['order_items'][0];

	// Sender details
	$email['email'] = $ci->config->item('baseEmailAddress');
	$email['sender'] = $ci->config->item('baseEmailName');

	// Template body;
	$query = $ci->db->query("SELECT * FROM order_template WHERE ordertemplate_id = ".$order['status_id']);
	$order_template = $query->num_rows() > 0 ? $query->result()[0] : false;

	$order['url'] = base_url('nyomkovetes/'.$order['order_id']);

	$order_items = '
	<table cellspacing="0" cellpadding="10" width="100%">
		<thead>
			<tr>
				<th style="font-size:14px;border-bottom:2px solid black" align="left">Termék</th>
				<th style="font-size:14px;border-bottom:2px solid black" align="left">Mennyiség</th>
				<th style="font-size:14px;border-bottom:2px solid black" align="left">Web shop ár</th>
				<th style="font-size:14px;border-bottom:2px solid black" align="left">Összesen</th>
			</tr>
		</thead>
	<tbody style="border:1px solid black">';

	$total_amount = "";
	foreach($data['order_items'] as $order_item) {
		$price = $order_item['item_price'];
		$qty = $order_item['item_qty'];
		$order_items .= '<tr>
			<td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left">'.$order_item['item_name'].'</td>
			<td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left">'.$qty.' db</td>
			<td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left">'.number_format($price,0,'','.').'</td>
			<td style="font-size:13px;border-bottom:1px solid black;padding:5px" align="left">'.number_format(($price*$qty),0,'','.').' Ft</td>
		</tr>';
		$total_amount = $total_amount + ($price * $qty);
	}
	$total_amount = $total_amount + $order['ship_price'];
	$order_items .= "</tbody>";

	$body = $order_template->ordertemplate_template;
	$body = str_replace('#email_logo',get_cfg('email_logo'),$body);
	$body = str_replace('#order_id',$order['order_id'],$body);
	$body = str_replace('#order_date',$order['order_date'],$body);
	$body = str_replace('#order_curier',$order['ship_name'],$body);
	$body = str_replace('#order_bill',billing($order['billing']),$body);
	$body = str_replace('#order_username',$order['order_invlname']." ".$order['order_invfname'],$body);
	$body = str_replace('#order_track_url',$order['url'],$body);
	$body = str_replace('#order_items',$order_items,$body);
	$body = str_replace('#order_ship',number_format($order['ship_price'],0,'','.')." Ft",$body);
	$body = str_replace('#order_total',number_format($total_amount,0,'','.')." Ft",$body);
	$body = str_replace('#tel',get_cfg('company_phone'),$body);
	$body = str_replace('#email',get_cfg('email'),$body);
	$body = str_replace('#base_url',base_url(),$body);
	$body = str_replace('#aszf',base_url(get_cfg('aszf')),$body);
	if(!empty($comment)){
		$body = str_replace('#comment',$comment,$body);
	}else {
		$body = str_replace('#comment','',$body);
	}

	// Email küldése
	$ci->email->from($email['email'], $email['sender']);
	$ci->email->to($order['order_email']);
	$ci->email->subject('Megrendelés #'.$order['order_id']);
	$ci->email->message($body);
	$ci->email->send();
}

function send_offer_alert($offer_id, $comment = "") {

	$ci = &get_instance();

	$query = $ci->db->query("SELECT * FROM `offer` o LEFT JOIN offer_template ot ON o.offer_statusid = ot.offertemplate_id WHERE o.offer_id = '".$offer_id."'");
	$offer = $query->num_rows() > 0 ? $query->result_array()[0] : false;

	// Sender details
	$email['email'] = $ci->config->item('baseEmailAddress');
	$email['sender'] = $ci->config->item('baseEmailName');

	$body = $offer['offertemplate_template'];
	$body = str_replace('#email_logo',get_cfg('email_logo'),$body);
	$body = str_replace('#tel',get_cfg('company_phone'),$body);
	$body = str_replace('#email',get_cfg('email'),$body);
	$body = str_replace('#base_url',base_url(),$body);
	$body = str_replace('#aszf',base_url(get_cfg('aszf')),$body);
	if(!empty($comment)){
		$body = str_replace('#comment',$comment,$body);
	}else {
		$body = str_replace('#comment','',$body);
	}

	// Email küldése
	$ci->email->from($email['email'], $email['sender']);
	$ci->email->to($offer['offer_email']);
	$ci->email->subject('Ajánlatkérés #'.$offer['offer_id']);
	$ci->email->message($body);
	$ci->email->send();
}
