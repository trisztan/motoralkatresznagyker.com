<?php

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('cart_empty')) {
    function cart_empty(){
        $ci = &get_instance();
        return empty($ci->session->userdata('cart')) ? true:false;
    }
}

if (!function_exists('cart_total')) {
    function cart_total()
    {
        $ci = &get_instance();
        $cart = $ci->session->userdata('cart');
        $ci->load->model('item_model');

        if ($cart && !empty($cart)) {
            foreach ($cart as $key => $value) {
                $cart_contents[$key] = $ci->item_model->item_by_id($key);
                $cart_contents[$key]['qty'] = $value;
            }
            $total = "";
            foreach ($cart_contents as $cart_content) {
                $price = price_calculator($cart_content['item_netprice'],$cart_content['item_netpricesale'],$cart_content['item_tax']);
                $total = $total + ($price * $cart_content['qty']);
            }

            return number_format($total,0,'','.')." Ft";
        }
        else {
            return "A kosár üres.";
        }
    }
}

if (!function_exists('cart_page')) {
    function cart_page()
    {
        $ci = &get_instance();
        $cart = $ci->session->userdata('cart');
        $ci->load->model('item_model');
        if ($cart && !empty($cart)) {
            foreach ($cart as $key => $value) {
                $cart_contents[$key] = $ci->item_model->item_by_id($key);
                $cart_contents[$key]['qty'] = $value;
            }
            $html = '<table id="cart" class="table table-hover table-condensed">
    				<thead>
						<tr>
							<th style="width:50%">Termékek</th>
                            <th style="width:10%">Súly</th>
							<th style="width:10%">Ár</th>
							<th style="width:8%">Mennyiség</th>
							<th style="width:22%" class="text-center">Összeg</th>
							<th style="width:10%"></th>
						</tr>
					</thead>
					<tbody>';

            $total = 0;

            foreach ($cart_contents as $cart_content) {
                $price = price_calculator($cart_content['item_netprice'],$cart_content['item_netpricesale'],$cart_content['item_tax']);

                $total = $total + ($price * $cart_content['qty']);
                $image = $cart_content['item_image'] ? $cart_content['item_image'] : 'template/backend/dist/img/no-image-50.jpg';
                $html .= '<tr>
							<td data-th="Product">
								<div class="row">
									<div class="col-sm-2 hidden-xs">
									<img class="thumbnail" style="height: 70px; width:70px;"src="'.base_url($image).'">
									</div>
									<div class="col-sm-10">
										<h4 class="nomargin">'.$cart_content['item_name'].'</h4>
										<p>'.word_limiter($cart_content['item_longdesc'], 13).'</p>
									</div>
								</div>
							</td>
                            <td data-th="Súly">'.number_format($cart_content['item_weight']*$cart_content['qty'],2).' kg</td>
							<td data-th="Price">'.$price.' Ft</td>
							<td data-th="Quantity">
								<input type="number" min="1" data-item-id="'.$cart_content['item_id'].'" class="cart-qty form-control text-center" value="'.$cart_content['qty'].'">
							</td>
							<td data-th="Subtotal" class="text-center">'.($price * $cart_content['qty']).' Ft </td>
							<td class="actions" data-th="">
								<button class="btn btn-danger" data-item-delete="'.$cart_content['item_id'].'"><i class="icon icon-normal-recycle-bin"></i> Törlés</button>
							</td>
						</tr>';
            }
            $html .= '</tbody>
				</table>
                <hr/>
                <div class="row">
                    <div class="col-md-3 col-md-offset-9"><strong>Összesen:</strong> <span class="total-price">'.$total.'</span> Ft <a href="#" class="btn btnNext btn-success pull-right">Tovább</a></div>
                </div>';

            return $html;
        } else {
            return '<div class="alert alert-info"><strong>Kosár</strong><br> A kosár üres!</div>';
        }
    }
}

if (!function_exists('cart_content')) {
    function cart_content()
    {
        $ci = &get_instance();
        $cart = $ci->session->userdata('cart');
        $ci->load->model('item_model');
        if ($cart && !empty($cart)) {
            foreach ($cart as $key => $value) {
                $cart_contents[$key] = $ci->item_model->item_by_id($key);
                $cart_contents[$key]['qty'] = $value;
            }
            $html = '';
            /* TODO: ha nincs kép */
            foreach ($cart_contents as $cart_content) {
                $image = $cart_content['item_image'] ? $cart_content['item_image'] : 'template/backend/dist/img/no-image-50.jpg';
                $html .= '<li><img style="width:30px;height:30px; border: 1px solid #000; margin:5px;" src="'.base_url($image).'" style="display: inline">
						<a href="'.base_url($cart_content['item_slug']).'">'.character_limiter($cart_content['item_name'], 20,'&nbsp;').'...</a>
						<span class="pull-right" style="display:inline"><span class="cart-badge">'.price_calculator($cart_content['item_netprice'],$cart_content['item_netpricesale'],$cart_content['item_tax']).' Ft</span>><span class="cart-badge bg-red">'.$cart_content['qty'].' db</span></span></li>';
            }
            $html .= '<li class="text-center"><br/><button type="button" data-cart-delete class="btn btn-danger"><i class="fa fa-trash"></i> Termékek törlése</button>  <button type="button" class="btn btn-success" onclick="location.href=\''.base_url('rendeles').'\'"><i class="fa fa-shopping-cart"></i> Tovább a pénztárhoz</button></li>';

            return $html;
        } else {
            return '<li><span class="empty-cart">A kosár tartalma üres</span></li>';
        }
    }
}

if (!function_exists('price_calculator')) {
    function price_calculator($price = 0, $saleprice = 0, $tax = 0){
        if(empty($saleprice)) {
            return round($price * (($tax / 100) + 1));
        }  else {
             return round($saleprice * (($tax / 100) + 1));
        }
    }
}
