<?php

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('billing')) {
    function billing($key)
    {
        if($key == 'utanvet'){
            return 'Utánvét';
        }
        if($key == 'utalas'){
            return 'Előreutalás';
        }
    }
}

if (!function_exists('admin_count_all')) {
    function admin_count_all()
    {
        $ci = &get_instance();
        $ci->db->select('user_id');
        $ci->db->where('user_type', 'ADMIN');
        $query = $ci->db->get('user');

        return $query->num_rows();
    }
}

if (!function_exists('order_count_new')) {
    function order_count_new()
    {
        $ci = &get_instance();
        $ci->db->select('order_id');
        $ci->db->where('status_id', 0);
        $query = $ci->db->get('order');

        return $query->num_rows();
    }
}

if (!function_exists('offer_count_new')) {
    function offer_count_new()
    {
        $ci = &get_instance();
        $ci->db->select('offer_id');
        $ci->db->where('offer_answered', 0);
        $query = $ci->db->get('offer');

        return $query->num_rows();
    }
}

if (!function_exists('product_count_all')) {
    function product_count_all()
    {
        $ci = &get_instance();
        $query = $ci->db->get('item');

        return $query->num_rows();
    }
}
if (!function_exists('category_count_all')) {
    function category_count_all()
    {
        $ci = &get_instance();
        $query = $ci->db->get('category_import');

        return $query->num_rows();
    }
}

if (!function_exists('buyer_count_all')) {
    function buyer_count_all()
    {
        $ci = &get_instance();
        $ci->db->group_by('order_email');
        $query = $ci->db->get('order');

        return $query->num_rows();
    }
}
if (!function_exists('create_slug')) {
    function create_slug($string)
    {
        $replacement = 'dash';
        $CI = &get_instance();
        $CI->load->helper(array('url', 'text', 'string'));
        $string = strtolower(url_title(convert_accented_characters($string), $replacement));

        return reduce_multiples($string, $replacement, false);
    }
}
if (!function_exists('date_range')) {
    function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d')
    {
        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);

        while ($current <= $last) {
            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }
}
