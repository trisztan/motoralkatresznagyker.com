<?php
defined('BASEPATH') OR exit('No direct script access allowed');


if (!function_exists('openhours')) {
	function openhours() {
		$openhours = '['.get_cfg('openhours').']';
		$openhours = json_decode($openhours,true);

		$d = date('N')-1;
		$h = strtotime(date('H:i'));
		$sd = strtotime($openhours[$d]['timeFrom']);
		$ed = strtotime($openhours[$d]['timeTill']);

		if($openhours[$d]['isActive']) {
			if(($h >= $sd) && ($h <= $ed)){
				return true;
			}else {
				return false;
			}
		}
		else {
			return false;
		}
	}
}


if (!function_exists('frontend_menu')) {

	function header_menu() {
		$ci = &get_instance();
		$query = $ci->db->query("

						SELECT cms_slug as router_slug, cms_title as router_pagename FROM cms WHERE cms_status = 1 AND cms_position = 'fent'
						UNION ALL
						SELECT router_slug as router_slug, router_pagename as router_pagename FROM router WHERE router_slug = 'arajanlatkeres' OR router_slug = 'kapcsolat'
						");
        return $query->num_rows() > 0 ? $query->result() : false;
	}
}

if (!function_exists('frontend_menu')) {

	function footer_menu() {
		$ci = &get_instance();
		$query = $ci->db->query("
						SELECT cms_slug as router_slug, cms_title as router_pagename FROM cms WHERE cms_status = 1 AND cms_position = 'lent'
						");
        return $query->num_rows() > 0 ? $query->result() : false;
	}
}

if (!function_exists('get_category')) {

	function get_category($category_slug) {
		$ci = &get_instance();
		if($category_slug) {
			$ci->db->where('category_slug',$category_slug);

		}
		else {
			$ci->db->where('category_parentid',0);
		}
		$query = $ci->db->get('category');
        return $query->num_rows() > 0 ? $query->result() : false;
	}
}
?>
