<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('set_lang')) {
    function set_lang() {
        $ci =& get_instance(); 
        
        if(!$ci->session->userdata('user_lang')) {
            $known_langs = array('en','hu');
            $user_pref_langs = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

            foreach($user_pref_langs as $idx => $lang) {
                $lang = substr($lang, 0, 2);
                if (in_array($lang, $known_langs)) {
                    $ci->session->set_userdata('user_lang',$lang);
                    break;
                }
            }
        }  
    }
}

if ( ! function_exists('__e'))
{
	function __e($string)
	{
		$ci =& get_instance();
        $lang = $ci->session->userdata('user_lang');
        $ci->db->select($lang); 
        $ci->db->where('language_key',$string);
        $query = $ci->db->get('language');
        
        if($query->num_rows() > 0){
            if(!empty($query->result()[0]->$lang)){
                return $query->result()[0]->$lang;
            }
            else {
                return '<span style="color:red; font-size:10px;">Missing translate: '.$string.'</span>';
            }
        }
        else 
        {
            $data['language_key'] = $string;
			$data[$lang] = $string;
			$ci->db->insert('language', $data);
			return $string;
        }
	}
}
?>