$(function() {
    function cart_update() {
        $(":input").bind('keyup change', function(e) {
            if (!$(this).data("previousValue") || $(this).data("previousValue") != $(this).val()) {
                var _this = $(this);
                $.ajax({
                    url: $("[data-cart-qty-url]").val(),
                    method: "POST",
                    data: {
                        "validator_token": $('[data-csrf]').attr('value'),
                        "item_id": _this.attr("data-item-id"),
                        "item_qty": _this.val()
                    }
                }).success(function(result) {
                    result = JSON.parse(result);
                    $('[data-csrf]').attr('value', result.csrf);
                    toastr["success"](result.success);
                    $('[data-cart-content]').empty();
                    $('[data-cart-content]').html(result.cart);
                    cart_update();
                });
                $(this).data("previousValue", $(this).val());
            }
        });
    }
    cart_update();

    $(document).on("click", "[data-item-delete]", function() {
        var _this = $(this);
        $.ajax({
            url: $("[data-item-delete-url]").val(),
            method: "POST",
            data: {
                "validator_token": $('[data-csrf]').attr('value'),
                "item_id": _this.attr("data-item-delete"),
            }
        }).success(function(result) {
            result = JSON.parse(result);
            $('[data-csrf]').attr('value', result.csrf);
            toastr["success"](result.success);
            $('[data-cart-content]').empty();
            $('[data-cart-content]').html(result.cart);
        });
    });
});
