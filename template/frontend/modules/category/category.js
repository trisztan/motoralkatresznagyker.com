$(document).ready(function() {

    $(location).attr('href');

    //pure javascript
    var pathname = window.location.pathname.split('/');

    $('#item').DataTable({
        "responsive": true,
        "serverSide": true,
        "processing": true,
        "searchDelay": 1000,
        "columns": [{
                "orderable": false,
                "width": "5%"
            },
            null,
            null, {
                "orderable": false,
                "width": "25%"
            },
        ],
        "order": [
            [2, "asc"]
        ],
        "ajax": {
            url: $('[data-item-datatable-url]').val(),
            type: "post",
            data: function(e) {
                e.validator_token = $('[data-csrf]').attr('value');
                e.category = pathname[pathname.length - 1];
            },
        },
        "fnDrawCallback": function(data) {
            $('[data-csrf]').attr('value', data.json.csrf);
        },
        "language": {
            "sEmptyTable": "Nincs rendelkezésre álló adat",
            "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
            "sInfoEmpty": "Nulla találat",
            "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
            "sInfoPostFix": "",
            "sInfoThousands": " ",
            "sLengthMenu": "_MENU_ találat oldalanként",
            "sLoadingRecords": "Betöltés...",
            "sProcessing": "Feldolgozás...",
            "sSearch": "Keresés:",
            "sZeroRecords": "Nincs a keresésnek megfelelő találat",
            "oPaginate": {
                "sFirst": "Első",
                "sPrevious": "Előző",
                "sNext": "Következő",
                "sLast": "Utolsó"
            },
            "oAria": {
                "sSortAscending": ": aktiválja a növekvő rendezéshez",
                "sSortDescending": ": aktiválja a csökkenő rendezéshez"
            }
        }
    });
});
