$(document).ready(function() {

    toastr.options = {
      "positionClass": "toast-top-center",
    }
    
    $(document).on("click", "[data-offer-send]", function() {
        var _this = $(this);

        var formdata = $("[data-offer-form]").serializeArray();

        formdata.push({
            "name": "validator_token",
            "value": $('[data-csrf]').attr('value')
        });

        $.ajax({
            url: $("[data-offer-url]").val(),
            method: "POST",
            data: formdata
        }).success(function(result) {
            result = JSON.parse(result);
            $('.red-border').removeClass('red-border');
            if (result.toastr.success) {
                toastr["success"](result.toastr.success);
            } else {
                toastr["error"](result.toastr.error);
                if (result.error) {
                    $.each(result.error, function(key, value) {
                        $("#" + key).addClass('red-border');
                        $("#" + key).val("");
                        $("#" + key).attr('placeholder', value);
                    });
                }
            }
            $('[data-csrf]').attr('value', result.csrf);
        });
    });
});
