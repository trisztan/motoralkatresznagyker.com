$(function() {

    toastr.options = {
      "positionClass": "toast-top-center",
       "timeOut": "1000",
    }

    $(document).on("click", ".btnNext", function() {
        $('.nav-tabs > .active').next('li').find('a').trigger('click');
    });

    $(document).on("click", ".btnPrev", function() {
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });


    $(document).on("click", "[data-order-save]", function() {
        var aszf = $('div#summary').find("input[name='aszf']:checked").val();
        if (!aszf) {
            toastr.options = {
              "positionClass": "toast-top-center",
            }
            toastr["error"]('Az Általános Szerződési feltételeket el kell fogadni!');
        } else {
            var delivery = ($('[data-show-delivery]').is(":checked") ? 1:0);
            var order_subscribe = ($('div#summary').find("input[name='order_subscribe']:checked").val() ? 1 : 0);
            if(!$('[data-billing-price]').length){
                var ship = $('div#order').find("[data-shipping-mode]").find("input[name='shipping_mod']:checked").attr('data-ship-name-value');
            } else {
                var ship = $('[data-billing-price]').find("input[name='shipping_mod']").attr('data-ship-name-value');
            }
            var bill = $('div#order').find("#form-order").find("input[name='billing_mod']:checked").val();
            var data = $('#form-orderdata').serializeArray();
            $.ajax({
                url: $("[data-order-save-url]").val(),
                method: "POST",
                data: {
                    "validator_token": $('[data-csrf]').attr('value'),
                    "data": data,
                    "ship": ship,
                    "bill": bill,
                    "delivery":delivery,
                    "order_subscribe": order_subscribe,
                    "total_price": $('.total-price:first').text()
                }
            }).success(function(result) {
                result = JSON.parse(result);
                $('[data-csrf]').attr('value', result.csrf);
                window.location.replace($('[data-order-thankyou-url]').val());
            });
        }
    });

    function validateEmail(email)
    {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }

    $(document).on("click", "[data-order-data]", function() {
        var data = $('#form-orderdata').serializeArray();
        $('.red-border').removeClass('red-border');
        $('.order-taxnumber').remove();
        $('.order-email').remove();
        var error = false

        if ($('[data-show-delivery]').is(":checked"))
        {
            $.each(data, function(i, field) {
                if (field.name != 'order_invcompany' &&
                    field.name != 'order_taxnumber' &&
                    field.name != 'order_comment' &&
                    field.name != 'order_company') {
                    if (!field.value) {
                        error = true;
                        $('#' + field.name).addClass('red-border');
                    }
                }
                if (field.name == 'order_email' && field.value && !validateEmail(field.value)) {
                     error = true;
                     $('#' + field.name).addClass('red-border');
                     $('#' + field.name).parent().append('<span class="order-email">Helytelen e-mail cím</span>');
                }
            });
        }
        else {
            $.each(data, function(i, field) {
                if (field.name != 'order_invcompany' &&
                field.name != 'order_taxnumber' &&
                field.name != 'order_comment' &&
                field.name != 'order_mobile' &&
                field.name != 'order_lname' &&
                field.name != 'order_fname' &&
                field.name != 'order_company' &&
                field.name != 'order_city' &&
                field.name != 'order_zip' &&
                field.name != 'order_street') {
                    if (!field.value) {
                        error = true;
                        $('#' + field.name).addClass('red-border');
                    }
                }

                if (field.name == 'order_email' && field.value && !validateEmail(field.value)) {
                     error = true;
                     $('#' + field.name).addClass('red-border');
                     $('#' + field.name).parent().append('<span class="order-email">Helytelen e-mail cím</span>');
                }
            });
        }

        if (error == true) {
            $("a[href='#profile']").trigger('click');
        } else {
            $.ajax({
                url: $("[data-shipping-calculate-url]").val(),
                method: "POST",
                data: {
                    "validator_token": $('[data-csrf]').attr('value')
                }
            }).success(function(result) {
                result = JSON.parse(result);
                $('[data-csrf]').attr('value', result.csrf);
                if (!$('#element').is(':empty')) {
                    var checked = $('input[name=shipping_mod]:checked', '#form-order').val()
                }
                $('[data-shipping-mode]').empty();

                var count = result.ship.length;
                if(count > 1) {
                    $('.shipping-mode-header').show();
                    $.each(result.ship, function(i, field) {
                        $('[data-shipping-mode]').append(
                            '<div class="form-group">' +
                            '<label for="order_state" class="col-sm-2 control-label">' + field.ship_name + '</label>' +
                            '<div class="col-sm-10">' +
                            '<div class="checkbox">' +
                            '<label>' +
                            '<input type="radio" name="shipping_mod" data-ship-name-value="' + field.ship_name + '_' + field.ship_price + '" value="' + field.ship_price + '" ' + (checked == field.ship_price ? 'checked' : '') + '> ' + field.ship_price + ' Ft ' +
                            '</label>' +
                            '</div>' +
                            '</div>' +
                            '</div>');
                    });
                }
                else {
                    $('[data-billing-price]').remove();
                    $('.shipping-mode-header').hide();
                    $.each(result.ship, function(i, field) {
                        $('[data-billing-mod]').parent().append('<span data-billing-price>' + field.ship_price + ' Ft <input type="hidden" name="shipping_mod" data-ship-name-value="' + field.ship_name + '_' + field.ship_price + '" value="' + field.ship_price + '"></span>');
                    });
                }
            });
        }
    });

    $(document).on("click", "[data-order-summary]", function() {
        var error = false;
        var data = $('#form-orderdata').serializeArray();
        $('.red-border').removeClass('red-border');
        $('.order-taxnumber').remove();
        $('.order-email').remove();

        if ($('[data-show-delivery]').is(":checked"))
        {
            $.each(data, function(i, field) {
                if (field.name != 'order_invcompany' &&
                    field.name != 'order_taxnumber' &&
                    field.name != 'order_comment' &&
                    field.name != 'order_company') {
                    if (!field.value) {
                        error = true;
                        $('#' + field.name).addClass('red-border');
                    }
                }
                if (field.name == 'order_email' && field.value && !validateEmail(field.value)) {
                     error = true;
                     $('#' + field.name).addClass('red-border');
                     $('#' + field.name).parent().append('<span class="order-email">Helytelen e-mail cím</span>');
                }
            });
        }
        else {
            $.each(data, function(i, field) {
                if (field.name != 'order_invcompany' &&
                field.name != 'order_taxnumber' &&
                field.name != 'order_comment' &&
                field.name != 'order_mobile' &&
                field.name != 'order_lname' &&
                field.name != 'order_fname' &&
                field.name != 'order_company' &&
                field.name != 'order_city' &&
                field.name != 'order_zip' &&
                field.name != 'order_street') {
                    if (!field.value) {
                        error = true;
                        $('#' + field.name).addClass('red-border');
                    }
                }

                if (field.name == 'order_email' && field.value && !validateEmail(field.value)) {
                     error = true;
                     $('#' + field.name).addClass('red-border');
                     $('#' + field.name).parent().append('<span class="order-email">Helytelen e-mail cím</span>');
                }
            });
        }

        if (error == true) {
            $("a[href='#order']").trigger('click');
            return false;
        }

        if(!$('[data-billing-price]').length){
            var ship = $('div#order').find("[data-shipping-mode]").find("input[name='shipping_mod']:checked").val();
        } else {
            var ship = $('[data-billing-price]').find("input[name='shipping_mod']").attr('value');
        }

        var bill = $('div#order').find("#form-order").find("input[name='billing_mod']:checked").val();

        $('.shipping-mod-error').remove();

        if (!ship) {
            $('.shipping-mode-header').append('<span class="shipping-mod-error pull-right" style="font-size:12px; color:red">Szállítási módot kötelező kiválasztani!</span>');
            $("a[href='#order']").trigger('click');
        } else if (!bill) {
            $('.shipping-mode-header').append('<span class="shipping-mod-error pull-right" style="font-size:12px; color:red">A fizetési módot kötelező kiválasztani!</span>');
            $("a[href='#order']").trigger('click');
        } else {
            $('[data-order-summary-html]').empty();
            $('[data-order-summary-html]').append($('div#profile').clone());
            $('[data-order-summary-html]').find('input[type=text]').attr('disabled', 'true');
            $('[data-order-summary-html]').find('input[type=email]').attr('disabled', 'true');
            $('[data-order-summary-html]').find('textarea').attr('disabled', 'true');
            $('[data-order-summary-html]').find('[data-order-copy]').remove();
            $('[data-order-summary-html]').find('#form-orderdata > div:last').remove();
            $('[data-order-summary-html]').find('#form-orderdata > hr:last').remove();
            $('[data-order-summary-html]').append($('div#order').clone());
            if (!$('[data-show-delivery]').is(":checked")) {
                $('[data-order-summary-html]').find('.ship-header').remove();
                $('[data-order-summary-html]').find('[data-delivery-data]').remove();
            }
            $('[data-order-summary-html]').find('#form-order > div:last').remove();
            if(!$('[data-billing-price]').length){
                $('[data-order-summary-html]').find("input[name='shipping_mod']:not(:checked)").parent().parent().parent().parent().remove();
            }
            else {
                $('[data-order-summary-html]').find("input[name='billing_mod']:not(:checked)").parent().parent().parent().remove();
            }

            $('[data-order-summary-html]').find("input[name='shipping_mod']:checked").attr('disabled', 'true');
            $('[data-order-summary-html]').find("input[name='billing_mod']").attr('disabled', 'true');
            $('[data-order-summary-html]').append($('[data-cart-content]').html());
            $('[data-order-summary-html]').find('.cart-qty').attr('disabled', 'true');
            $('[data-order-summary-html]').find('[data-item-delete]').remove();
            $('[data-order-summary-html]').find('.total-price').html(parseInt($('[data-order-summary-html]').find('.total-price').text()) + parseInt(ship));
            $('[data-order-summary-html]').find('.btnNext').remove();
        }
    });

    $('[data-show-delivery]').css( 'cursor', 'pointer' );

    $(document).on("click", "[data-show-delivery-click]", function() {
        $('[data-delivery-data]').find('input').val('');
        $('[data-delivery-data]').toggle();
        if($('[data-delivery-data]').is(":visible")) {
            $("[data-show-delivery]").prop( "checked", true );
        }
        else {
            $("[data-show-delivery]").prop( "checked", false );
        }
    });

    $(document).on("keyup change",".cart-qty:input", function() {
        if (!$(this).data("previousValue") || $(this).data("previousValue") != $(this).val()) {
            var _this = $(this);
            $.ajax({
                url: $("[data-cart-qty-url]").val(),
                method: "POST",
                data: {
                    "validator_token": $('[data-csrf]').attr('value'),
                    "item_id": _this.attr("data-item-id"),
                    "item_qty": _this.val()
                }
            }).success(function(result) {
                result = JSON.parse(result);
                $('[data-csrf]').attr('value', result.csrf);
                toastr["success"](result.success);
                $('[data-cart-content]').empty();
                $('[data-cart-content]').html(result.cart);
                $('.cart-price-total').html(result.total);
                cart_update();
            });
            $(this).data("previousValue", $(this).val());
        }
    });

    $(document).on("click", "[data-item-delete]", function() {
        var _this = $(this);
        $.ajax({
            url: $("[data-cart-delete-url]").val(),
            method: "POST",
            data: {
                "validator_token": $('[data-csrf]').attr('value'),
                "item_id": _this.attr("data-item-delete")
            }
        }).success(function(result) {
            result = JSON.parse(result);
            $('[data-csrf]').attr('value', result.csrf);
            toastr["success"](result.success);
            $('[data-cart-content]').empty();
            $('[data-cart-content]').html(result.cart);
            $('.cart-price-total').html(result.total);
        });
    });

    /* Kosárhoz ürítés */
    $(document).on("click", "[data-cart-delete]", function() {
        var _this = $(this);
        $.ajax({
            url: $("[data-cart-delete-url]").val(),
            method: "POST",
            data: {
                "validator_token": $('[data-csrf]').attr('value'),
            }
        }).success(function(result) {
            result = JSON.parse(result);
            $('[data-csrf]').attr('value', result.csrf);
            toastr["warning"](result.success);
            $('.dropdown-cart').html(result.cart);
            $('.cart-price-total').html(result.total);
            location.reload();
        });
    });

});
