$(function() {

    toastr.options = {
      "positionClass": "toast-top-center",
       "timeOut": "1000",
    }

    /* Kosárhoz adás */
    $(document).on("click", "[data-add-to-cart]", function() {
        var _this = $(this);

        if ($('[data-add-cart-qty]').length > 0) {
            var item_qty = $('[data-add-cart-qty]').val();
        } else {
            var item_qty = _this.parent().siblings().children().val();
        }

        $.ajax({
            url: $("[data-add-to-cart-url]").val(),
            method: "POST",
            data: {
                "validator_token": $('[data-csrf]').attr('value'),
                "item_qty": item_qty,
                "item_id": _this.attr("data-add-to-cart")
            }
        }).success(function(result) {

            result = JSON.parse(result);
            $('[data-csrf]').attr('value', result.csrf);
            toastr["success"](result.success);
            $('.dropdown-cart').empty();
            $('.dropdown-cart').html(result.cart);
            $('.cart-price-total').empty();
            $('.cart-price-total').html(result.total);
        });
    });

    /* Kosárhoz ürítés */
    $(document).on("click", "[data-cart-delete]", function() {
        var _this = $(this);
        $.ajax({
            url: $("[data-cart-delete-url]").val(),
            method: "POST",
            data: {
                "validator_token": $('[data-csrf]').attr('value'),
            }
        }).success(function(result) {
            result = JSON.parse(result);
            $('[data-csrf]').attr('value', result.csrf);
            toastr["warning"](result.success);
            $('.dropdown-cart').html(result.cart);
            $('.cart-price-total').html(result.total);
        });
    });

});
