/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function () {

  "use strict";

  /* jQueryKnob */
  $(".knob").knob();

  /* Morris.js Charts */
  // Sales chart
  var base_url = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname.split('/')[1];

  $("[dashboard-stat]").change(function () {
    salesChart($("[dashboard-stat]").val());
  });

  function salesChart(type) {
    $.ajax({
      url: base_url + '/admin/admin/sales_chart',
      type: 'post',
      data: {
        data: type
      },
      success: function (data) {
        $("#week-sales-stat").empty();
        var area = new Morris.Area({
          element: 'week-sales-stat',
          resize: true,
          data: $.parseJSON(data).result,
          xkey: 'year',
          ykeys: ['item1'],
          labels: [$("#textOrdersPrices").val()],
          lineColors: ['#a0d0e0', '#3c8dbc'],
          hideHover: 'auto',
          postUnits: " Ft"
        });
      }
    });
  }

  salesChart("day");

  $("[dashboard-reservation-stat]").change(function () {
    reservationChart($("[dashboard-reservation-stat]").val());
  });

  function reservationChart(type) {
    $.ajax({
      url: base_url + '/admin/admin/reservation_chart',
      type: 'post',
      data: {
        data: type
      },
      success: function (data) {
        $("#week-reservation-stat").empty();
        var area = new Morris.Area({
          element: 'week-reservation-stat',
          resize: true,
          data: $.parseJSON(data).result,
          xkey: 'year',
          ykeys: ['item1'],
          labels: [$("#textReservation").val()],
          lineColors: ['#a0d0e0', '#3c8dbc'],
          hideHover: 'auto',
          postUnits: " db",
          gridIntegers: false,
          ymin: 0
        });
      }
    });
  }

  reservationChart("day");

});