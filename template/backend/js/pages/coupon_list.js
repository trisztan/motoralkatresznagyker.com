$(document).ready(function () {

    function coupon_list(data) {
        if (data) {
            $('#coupon_list').DataTable().destroy();
            $('#coupon_list > tbody').html(data);
        }

        $('#coupon_list').DataTable({
            "paging": true,
            "lengthChange": true,
            "ordering": false,
            "order": [],
            "searching": true,
            "info": true,
            "autoWidth": false,
            "language": {
                "sEmptyTable": "Nincs rendelkezésre álló adat",
                "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
                "sInfoEmpty": "Nulla találat",
                "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
                "sInfoPostFix": "",
                "sInfoThousands": " ",
                "sLengthMenu": "_MENU_ találat oldalanként",
                "sLoadingRecords": "Betöltés...",
                "sProcessing": "Feldolgozás...",
                "sSearch": "Keresés:",
                "sZeroRecords": "Nincs a keresésnek megfelelő találat",
                "oPaginate": {
                    "sFirst": "Első",
                    "sPrevious": "Előző",
                    "sNext": "Következő",
                    "sLast": "Utolsó"
                },
                "oAria": {
                    "sSortAscending": ": aktiválja a növekvő rendezéshez",
                    "sSortDescending": ": aktiválja a csökkenő rendezéshez"
                }
            }
        });
    }

    coupon_list();
    
    /* Dinamikus modal betöltés */
    function dynamic_modal(type,id) {
        $.ajax({
            url: $("[data-coupon-modal-url]").val(),
            method: "POST",
            data: {"type":  type,
                   "id":id,
                   "validator_token" : $('[data-csrf]').attr('value')}
        }).success(function (result) {
            result = JSON.parse(result);
            $('[data-modal]').empty();
            $('[data-modal]').html(result.data);
            $('[data-csrf]').attr('value',result.csrf);
            $('#couponmodal').modal('show');
        });
    }

    /* Kupon modal hozzáadás */
    $(document).on("click", "[data-coupon-add]", function () {
        dynamic_modal("add");
    });

    /* Kupon szerkesztés */
    $(document).on("click", "[data-coupon-edit]", function () {
        var id = $(this).attr('data-coupon-edit');
        dynamic_modal("edit",id);
    });

    /* Kupon törlés */
    $(document).on("click", "[data-coupon-delete]", function () {
        var id = $(this).attr('data-coupon-delete');
        dynamic_modal("delete",id);
    });  

    /* Kupon mentés */
    $(document).on("click", "[data-coupon-save]", function () {
        var _this = $(this);
        var formdata = $("#form-coupon").serializeArray();
        formdata.push({"name": "validator_token", "value": $('[data-csrf]').attr('value')});
        console.log(formdata);

        $.ajax({
            url: $("[data-coupon-url]").attr('data-coupon-url'),
            method: "POST",
            data: formdata
        }).success(function (result) {
            result = JSON.parse(result);
            $('.red-border').removeClass('red-border');
            if (result.toastr.success) {
                $('#couponmodal').modal('hide');
                toastr["success"](result.toastr.success);
            } else {
                toastr["error"](result.toastr.error);
                if (result.error) {
                    $.each(result.error, function( key, value ) {
                        $("#"+key).addClass('red-border');
                        $("#"+key).val("");
                        $("#"+key).attr('placeholder',value);
                    });
                }
            }
            $('[data-csrf]').attr('value',result.csrf);
            coupon_list(result.data);
        });
    });

    /* Kupon modal kiegészítés */
    $(document).on("change", "[data-coupon-type-select]", function () {
        value = $(this).val();
        if(value == 'SUM') {
            $('[data-coupon-type]').html('Ft');
        } 
        else 
        {
            $('[data-coupon-type]').html('<i class="fa fa-percent"></i>');
        }
    }); 

});
