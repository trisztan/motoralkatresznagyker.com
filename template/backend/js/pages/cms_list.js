$(document).ready(function () {

    function cms_list(data) {
        if (data) {
            $('#cms_list').DataTable().destroy();
            $('#cms_list > tbody').html(data);
        }

        $('#cms_list').DataTable({
            "paging": true,
            "lengthChange": true,
            "ordering": false,
            "order": [],
            "searching": true,
            "info": true,
            "autoWidth": false,
            "language": {
                "sEmptyTable": "Nincs rendelkezésre álló adat",
                "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
                "sInfoEmpty": "Nulla találat",
                "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
                "sInfoPostFix": "",
                "sInfoThousands": " ",
                "sLengthMenu": "_MENU_ találat oldalanként",
                "sLoadingRecords": "Betöltés...",
                "sProcessing": "Feldolgozás...",
                "sSearch": "Keresés:",
                "sZeroRecords": "Nincs a keresésnek megfelelő találat",
                "oPaginate": {
                    "sFirst": "Első",
                    "sPrevious": "Előző",
                    "sNext": "Következő",
                    "sLast": "Utolsó"
                },
                "oAria": {
                    "sSortAscending": ": aktiválja a növekvő rendezéshez",
                    "sSortDescending": ": aktiválja a csökkenő rendezéshez"
                }
            }
        });
    }
    
    cms_list();
    
    /* Dinamikus modal betöltés */
    function dynamic_modal(type,id) {
        $.ajax({
            url: $("[data-cms-modal-url]").val(),
            method: "POST",
            data: {"type":  type,
                   "id":id,
                   "validator_token" : $('[data-csrf]').attr('value')}
        }).success(function (result) {
            result = JSON.parse(result);
            $('[data-modal]').empty();
            $('[data-modal]').html(result.data);
            $('[data-csrf]').attr('value',result.csrf);
            $('#cmsmodal').modal('show');
            CKEDITOR.replace('cms_content');
        });
    }

    /* Oldal modal hozzáadás */
    $(document).on("click", "[data-cms-add]", function () {
        dynamic_modal("add");
    });

    /* Oldal szerkesztés */
    $(document).on("click", "[data-cms-edit]", function () {
        var id = $(this).attr('data-cms-edit');
        dynamic_modal("edit",id);
    });

    /* Oldal törlés */
    $(document).on("click", "[data-cms-delete]", function () {
        var id = $(this).attr('data-cms-delete');
        dynamic_modal("delete",id);
    });  

    /* Oldal mentés */
    $(document).on("click", "[data-cms-save]", function () {
        var _this = $(this);
        var formdata = $("#form-cms").serializeArray();
        formdata.push({"name": "validator_token", "value": $('[data-csrf]').attr('value')});

        $.ajax({
            url: $("[data-cms-url]").attr('data-cms-url'),
            method: "POST",
            data: formdata,
        }).success(function (result) {
            result = JSON.parse(result);
            $('.red-border').removeClass('red-border');
            if (result.toastr.success) {
                $('#cmsmodal').modal('hide');
                toastr["success"](result.toastr.success);
            } else {
                toastr["error"](result.toastr.error);
                if (result.error) {
                    $.each(result.error, function( key, value ) {
                        $("#"+key).addClass('red-border');
                        $("#"+key).val("");
                        $("#"+key).attr('placeholder',value);
                    });
                }
            }
            $('[data-csrf]').attr('value',result.csrf);
            cms_list(result.data);
        });
    });

    timer = setInterval(updateDiv,100);
    function updateDiv() {
        var editorText = CKEDITOR.instances.cms_content.getData();
        $('#cms_content').html(editorText);
    }
});
