$(document).ready(function() {

    function ship_list(data) {
        if (data) {
            $('#ship_list').DataTable().destroy();
            $('#ship_list > tbody').html(data);
        }

        $('#ship_list').DataTable({
            "paging": true,
            "lengthChange": true,
            "ordering": false,
            "order": [],
            "searching": true,
            "info": true,
            "autoWidth": false,
            "language": {
                "sEmptyTable": "Nincs rendelkezésre álló adat",
                "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
                "sInfoEmpty": "Nulla találat",
                "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
                "sInfoPostFix": "",
                "sInfoThousands": " ",
                "sLengthMenu": "_MENU_ találat oldalanként",
                "sLoadingRecords": "Betöltés...",
                "sProcessing": "Feldolgozás...",
                "sSearch": "Keresés:",
                "sZeroRecords": "Nincs a keresésnek megfelelő találat",
                "oPaginate": {
                    "sFirst": "Első",
                    "sPrevious": "Előző",
                    "sNext": "Következő",
                    "sLast": "Utolsó"
                },
                "oAria": {
                    "sSortAscending": ": aktiválja a növekvő rendezéshez",
                    "sSortDescending": ": aktiválja a csökkenő rendezéshez"
                }
            }
        });
    }

    ship_list();

    function dynamic_modal(type, id) {
        $.ajax({
            url: $("[data-ship-modal-url]").val(),
            method: "POST",
            data: {
                "type": type,
                "id": id,
                "validator_token": $('[data-csrf]').attr('value')
            }
        }).success(function(result) {
            result = JSON.parse(result);
            $('[data-modal]').empty();
            $('[data-modal]').html(result.data);
            $('[data-csrf]').attr('value', result.csrf);
            $('#shipmodal').modal('show');

        });
    }

    /* Szállítási osztály hozzáadása */
    $(document).on("click", "[data-ship-modal-add]", function() {
        var id = $(this).attr("data-ship-modal-add");
        dynamic_modal('add', id);
    });

    /* Kupon szerkesztés */
    $(document).on("click", "[data-ship-modal-edit]", function() {
        var id = $(this).attr('data-ship-modal-edit');
        dynamic_modal("edit", id);
    });

    /* Kupon törlés */
    $(document).on("click", "[data-ship-modal-del]", function() {
        var id = $(this).attr('data-ship-modal-del');
        dynamic_modal("delete", id);
    });

    /* Kupon mentés */
    $(document).on("click", "[data-ship-save]", function() {
        var _this = $(this);
        var formdata = $("#form-ship").serializeArray();
        formdata.push({
            "name": "validator_token",
            "value": $('[data-csrf]').attr('value')
        });

        $.ajax({
            url: $("[data-ship-url]").attr('data-ship-url'),
            method: "POST",
            data: formdata
        }).success(function(result) {
            result = JSON.parse(result);
            $('.red-border').removeClass('red-border');
            if (result.toastr.success) {
                $('#shipmodal').modal('hide');
                toastr["success"](result.toastr.success);
            } else {
                toastr["error"](result.toastr.error);
                if (result.error) {
                    $.each(result.error, function(key, value) {
                        $("#" + key).addClass('red-border');
                        $("#" + key).val("");
                        $("#" + key).attr('placeholder', value);
                    });
                }
            }
            $('[data-csrf]').attr('value', result.csrf);
            ship_list(result.data);
        });
    });
});
