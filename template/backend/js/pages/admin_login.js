$('input').iCheck({
  checkboxClass: 'icheckbox_square-blue',
  radioClass: 'iradio_square-blue',
  increaseArea: '20%' // optional
});

$(function() {

    $('[data-change-language]').change(function() {
        $.ajax({
            url: baseurl,
            type: 'post',
            data: {
                lang : $('[data-change-language]').val()
            },
            dataType: 'json',
            success: function( result )
            {
                $('[data-language-textLoginImportant]').text(result.textLoginImportant);
                $('[data-language-textEmailorUsername]').attr('placeholder',result.textEmailorUsername);
                $('[data-language-textPassword]').attr('placeholder', result.textPassword);
                $('[data-language-textRememberMe]').text(result.textRememberMe);
                $('[data-language-textLogin]').text(result.textLogin);
                $('[data-language-textForgottenPassword]').html(result.textForgottenPassword);
            }
        });
    });              
});