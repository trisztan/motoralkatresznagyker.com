$(document).ready(function () {

    function user_list(data) {
        if (data) {
            $('#user_list').DataTable().destroy();
            $('#user_list > tbody').html(data);
        }

        $('#user_list').DataTable({
            "paging": true,
            "lengthChange": true,
            "ordering": false,
            "order": [],
            "searching": true,
            "info": true,
            "autoWidth": false,
            "language": {
                "sEmptyTable": "Nincs rendelkezésre álló adat",
                "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
                "sInfoEmpty": "Nulla találat",
                "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
                "sInfoPostFix": "",
                "sInfoThousands": " ",
                "sLengthMenu": "_MENU_ találat oldalanként",
                "sLoadingRecords": "Betöltés...",
                "sProcessing": "Feldolgozás...",
                "sSearch": "Keresés:",
                "sZeroRecords": "Nincs a keresésnek megfelelő találat",
                "oPaginate": {
                    "sFirst": "Első",
                    "sPrevious": "Előző",
                    "sNext": "Következő",
                    "sLast": "Utolsó"
                },
                "oAria": {
                    "sSortAscending": ": aktiválja a növekvő rendezéshez",
                    "sSortDescending": ": aktiválja a csökkenő rendezéshez"
                }
            }
        });
    }

    user_list();
    
    /* Dinamikus modal betöltés */
    function dynamic_modal(type,id) {
        $.ajax({
            url: $("[data-user-modal-url]").val(),
            method: "POST",
            data: {"type":  type,
                   "id":id,
                   "validator_token" : $('[data-csrf]').attr('value')}
        }).success(function (result) {
            result = JSON.parse(result);
            $('[data-modal]').empty();
            $('[data-modal]').html(result.data);
            $('[data-csrf]').attr('value',result.csrf);
            $('#usermodal').modal('show');
        });
    }

    /* Felhasználó modal hozzáadás */
    $(document).on("click", "[data-user-add]", function () {
        dynamic_modal("add");
    });

    /* Felhasználó szerkesztés */
    $(document).on("click", "[data-user-edit]", function () {
        var id = $(this).attr('data-user-edit');
        dynamic_modal("edit",id);
    });

    /* Felhasználó törlés */
    $(document).on("click", "[data-user-delete]", function () {
        var id = $(this).attr('data-user-delete');
        dynamic_modal("delete",id);
    });  

    /* Felhasználó mentés */
    $(document).on("click", "[data-user-save]", function () {
        var _this = $(this);
        var formdata = $("#form-user").serializeArray();
        formdata.push({"name": "validator_token", "value": $('[data-csrf]').attr('value')});
        console.log(formdata);

        $.ajax({
            url: $("[data-user-url]").attr('data-user-url'),
            method: "POST",
            data: formdata
        }).success(function (result) {
            result = JSON.parse(result);
            $('.red-border').removeClass('red-border');
            if (result.toastr.success) {
                $('#usermodal').modal('hide');
                toastr["success"](result.toastr.success);
            } else {
                toastr["error"](result.toastr.error);
                if (result.error) {
                    $.each(result.error, function( key, value ) {
                        $("#"+key).addClass('red-border');
                        $("#"+key).val("");
                        $("#"+key).attr('placeholder',value);
                    });
                }
            }
            $('[data-csrf]').attr('value',result.csrf);
            user_list(result.data);
        });
    }); 
});
