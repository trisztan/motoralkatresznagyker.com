$(document).ready(function () {
    var myDropzone = Dropzone.options.myAwesomeDropzone = {
        maxFilesize: 10,
        url: $('[data-impexp-upload-url]').val(),
        uploadMultiple: false,
        parallelUploads: 1,
        maxFiles: 100,
        init: function () {
            this.on("sending", function (file, xhr, formData) {
                formData.append("validator_token", $('[data-csrf]').attr('value'));
                formData.append("upload_type", $('[data-file-type]').val());
            });
            this.on("error", function (file, result) {
                result = JSON.parse(result);
                toastr["error"](result.message);
                $('[data-csrf]').attr('value',result.csrf);
            });
            this.on("success", function (file, result) {
                result = JSON.parse(result);
                $('[data-csrf]').attr('value',result.csrf);
                $(".dz-success").remove();
                toastr[result.type](result.message);
            });
        }
    };

    function data_import_start(import_type, limit, offset) {
        $.ajax({
            url: $("[data-impexp-import-url]").val(),
            method: "POST",
            data: {
                "validator_token": $('[data-csrf]').attr('value'),
                "import_type": import_type,
                "limit": limit,
                "offset": offset
            }
        }).success(function (result) {
            result = JSON.parse(result);
            $('[data-csrf]').attr('value',result.csrf);

            if(result.msg) {
                toastr["warning"](result.msg);
            }
            else {
                if(result.percent.toFixed(0) > 100){
                    var percent = 100;
                } else {
                    var percent = result.percent.toFixed(2);
                }
                $('#'+import_type+' .progress-description').html(percent+"%");
                $('#'+import_type+' .progress-bar').css({"width":percent+"%"});
                if(percent != 100){
                    data_import_start(result.import_type,result.limit,result.offset);
                } else {
                    $('#'+import_type+' .progress-bar').css({"background":"green"});
                    toastr["success"]("Importálás sikeresen befejeződött!");

                    setTimeout(function(){
                        $('#'+import_type+' .progress-description').html("0%");
                        $('#'+import_type+' .progress-bar').css({"width":"0%"});
                    }, 3000);
                }
            }

        });
    }

    $(document).on("click", "[data-import-start]", function () {
        data_import_start($(this).attr('data-import-start'), 500, 0);
    });

});

var UITree = function () {

     var ajaxTreeSample = function() {

        $("#tree_4").jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                },
                // so that create works
                "check_callback" : true,
                'data' : {
                    'url' : function (node) {
                         return $('[data-impexp-jstree-url]').val()+"/";
                    },
                    'data' : function (node) {
                      return { 'parent' : node.id };
                    }
                }
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            },
            "state" : { "key" : "demo3" },
            "plugins" : [ "dnd", "state", "types","checkbox"]
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            ajaxTreeSample();
        }
    };

}();


jQuery(document).ready(function() {
   UITree.init();

   $(document).on("click", "[data-export-start]", function () {
       var export_data = new Array();
       var export_type = $(this).attr('data-export-start');

       $(".jstree-clicked").each(function(i, element){
           export_data[i] = $(element).attr("id");
       });
       $.ajax({
           url: $("[data-impexp-export-url]").val(),
           method: "POST",
           data: {
               "validator_token": $('[data-csrf]').attr('value'),
               "export": export_data,
               "export_type": export_type,
           }
       }).success(function (result) {
           window.location.replace($("[data-impexp-export-url]").val());
       })
   });
});
