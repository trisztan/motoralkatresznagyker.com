$(document).ready(function() {

    function ordertemplate_list(data) {
        if (data) {
            $('#ordertemplate_list').DataTable().destroy();
            $('#ordertemplate_list > tbody').html(data);
        }

        $('#ordertemplate_list').DataTable({
            "paging": true,
            "lengthChange": true,
            "ordering": false,
            "order": [],
            "searching": true,
            "info": true,
            "autoWidth": false,
            "language": {
                "sEmptyTable": "Nincs rendelkezésre álló adat",
                "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
                "sInfoEmpty": "Nulla találat",
                "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
                "sInfoPostFix": "",
                "sInfoThousands": " ",
                "sLengthMenu": "_MENU_ találat oldalanként",
                "sLoadingRecords": "Betöltés...",
                "sProcessing": "Feldolgozás...",
                "sSearch": "Keresés:",
                "sZeroRecords": "Nincs a keresésnek megfelelő találat",
                "oPaginate": {
                    "sFirst": "Első",
                    "sPrevious": "Előző",
                    "sNext": "Következő",
                    "sLast": "Utolsó"
                },
                "oAria": {
                    "sSortAscending": ": aktiválja a növekvő rendezéshez",
                    "sSortDescending": ": aktiválja a csökkenő rendezéshez"
                }
            }
        });
    }

    ordertemplate_list();

    /* Dinamikus modal betöltés */
    function dynamic_modal(type, id) {
        $.ajax({
            url: $("[data-ordertemplate-modal-url]").val(),
            method: "POST",
            data: {
                "type": type,
                "id": id,
                "validator_token": $('[data-csrf]').attr('value')
            }
        }).success(function(result) {
            result = JSON.parse(result);
            $('[data-modal]').empty();
            $('[data-modal]').html(result.data);
            $('[data-csrf]').attr('value', result.csrf);
            $('#ordertemplatemodal').modal('show');

            $('.ordertemplate_template').summernote({
              airMode: true,
              dialogsInBody: true,
              lang: "hu-HU"
            });
        });
    }

    /* Felhasználó modal hozzáadás */
    $(document).on("click", "[data-ordertemplate-add]", function() {
        dynamic_modal("add");
    });

    /* Felhasználó szerkesztés */
    $(document).on("click", "[data-ordertemplate-edit]", function() {
        var id = $(this).attr('data-ordertemplate-edit');
        dynamic_modal("edit", id);
    });

    /* Felhasználó törlés */
    $(document).on("click", "[data-ordertemplate-delete]", function() {
        var id = $(this).attr('data-ordertemplate-delete');
        dynamic_modal("delete", id);
    });

    /* Felhasználó mentés */
    $(document).on("click", "[data-ordertemplate-save]", function() {
        var _this = $(this);
        var formdata = $("#form-ordertemplate").serializeArray();
        formdata.push({
            "name": "validator_token",
            "value": $('[data-csrf]').attr('value')
        });
        formdata.push({
            "name": "ordertemplate_template",
            "value": $(".ordertemplate_template").summernote('code')
        });
        $.ajax({
            url: $("[data-ordertemplate-url]").attr('data-ordertemplate-url'),
            method: "POST",
            data: formdata
        }).success(function(result) {
            result = JSON.parse(result);
            $('.red-border').removeClass('red-border');
            if (result.toastr.success) {
                $('#ordertemplatemodal').modal('hide');
                toastr["success"](result.toastr.success);
            } else {
                toastr["error"](result.toastr.error);
                if (result.error) {
                    $.each(result.error, function(key, value) {
                        $("#" + key).addClass('red-border');
                        $("#" + key).val("");
                        $("#" + key).attr('placeholder', value);
                    });
                }
            }
            $('[data-csrf]').attr('value', result.csrf);
            ordertemplate_list(result.data);
        });
    });
});
