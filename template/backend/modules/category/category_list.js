$(document).ready(function () {
    var category = $('#category').DataTable({
        "responsive": true,
        "serverSide": true,
        "processing": true,
        "ajax": {
            url: $('[data-category-datatable-url]').val(),
            type: "post",
            data: function(e) {
                e.validator_token = $('[data-csrf]').attr('value');
            },
        },
        "fnDrawCallback": function(data) {
            $('[data-csrf]').attr('value',data.json.csrf);
        },
        "language": {
            "sEmptyTable": "Nincs rendelkezésre álló adat",
            "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
            "sInfoEmpty": "Nulla találat",
            "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
            "sInfoPostFix": "",
            "sInfoThousands": " ",
            "sLengthMenu": "_MENU_ találat oldalanként",
            "sLoadingRecords": "Betöltés...",
            "sProcessing": '<i class="fa fa-refresh fa-spin fa-2x fa-fw red">',
            "sSearch": "Keresés:",
            "sZeroRecords": "Nincs a keresésnek megfelelő találat",
            "oPaginate": {
                "sFirst": "Első",
                "sPrevious": "Előző",
                "sNext": "Következő",
                "sLast": "Utolsó"
            },
            "oAria": {
                "sSortAscending": ": aktiválja a növekvő rendezéshez",
                "sSortDescending": ": aktiválja a csökkenő rendezéshez"
            }
        }
    });

    function dynamic_modal(type,id) {
      $.ajax({
          url: $("[data-category-modal-url]").val(),
          method: "POST",
          data: {"type":  type,
                 "id":id,
                 "validator_token" : $('[data-csrf]').attr('value')}
      }).success(function (result) {
          result = JSON.parse(result);
          $('[data-modal]').empty();
          $('[data-modal]').html(result.data);
          $('[data-csrf]').attr('value',result.csrf);
          $('#categorymodal').modal('show');

      });
    }

    /* Item import modal */
    $(document).on("click", "[data-category-add]", function () {
        dynamic_modal("add");
    });

    /* Kupon szerkesztés */
    $(document).on("click", "[data-category-edit]", function () {
        var id = $(this).attr('data-category-edit');
        dynamic_modal("edit",id);
    });

    /* Kupon szerkesztés */
    $(document).on("click", "[data-category-image]", function () {
        var id = $(this).attr('data-category-image');
        dynamic_modal("image",id);
    });

    /* Kupon törlés */
    $(document).on("click", "[data-category-delete]", function () {
        var id = $(this).attr('data-category-delete');
        dynamic_modal("delete",id);
    });

    /* Kupon mentés */
    $(document).on("click", "[data-category-save]", function () {
        var _this = $(this);
        var formdata = $("#form-category").serializeArray();
        formdata.push({"name": "validator_token", "value": $('[data-csrf]').attr('value')});

        $.ajax({
            url: $("[data-category-url]").attr('data-category-url'),
            method: "POST",
            data: formdata
        }).success(function (result) {
            result = JSON.parse(result);
            $('.red-border').removeClass('red-border');
            if (result.toastr.success) {
                $('#categorymodal').modal('hide');
                toastr["success"](result.toastr.success);
            } else {
                toastr["error"](result.toastr.error);
                if (result.error) {
                    $.each(result.error, function( key, value ) {
                        $("#"+key).addClass('red-border');
                        $("#"+key).val("");
                        $("#"+key).attr('placeholder',value);
                    });
                }
            }
            $('[data-csrf]').attr('value',result.csrf);
            category.ajax.reload();
        });
    });
});
