$(document).ready(function() {
    var offer = $('#offer').DataTable({
        "responsive": true,
        "serverSide": true,
        "processing": true,
        "ajax": {
            url: $('[data-offer-datatable-url]').val(),
            type: "post",
            data: function(e) {
                e.validator_token = $('[data-csrf]').attr('value');
            },
        },
        "fnDrawCallback": function(data) {
            $('[data-csrf]').attr('value', data.json.csrf);
        },
        "language": {
            "sEmptyTable": "Nincs rendelkezésre álló adat",
            "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
            "sInfoEmpty": "Nulla találat",
            "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
            "sInfoPostFix": "",
            "sInfoThousands": " ",
            "sLengthMenu": "_MENU_ találat oldalanként",
            "sLoadingRecords": "Betöltés...",
            "sProcessing": '<i class="fa fa-refresh fa-spin fa-2x fa-fw red">',
            "sSearch": "Keresés:",
            "sZeroRecords": "Nincs a keresésnek megfelelő találat",
            "oPaginate": {
                "sFirst": "Első",
                "sPrevious": "Előző",
                "sNext": "Következő",
                "sLast": "Utolsó"
            },
            "oAria": {
                "sSortAscending": ": aktiválja a növekvő rendezéshez",
                "sSortDescending": ": aktiválja a csökkenő rendezéshez"
            }
        }
    });

    function dynamic_modal(type, id) {
        $.ajax({
            url: $("[data-offer-modal-url]").val(),
            method: "POST",
            data: {
                "type": type,
                "id": id,
                "validator_token": $('[data-csrf]').attr('value')
            }
        }).success(function(result) {
            result = JSON.parse(result);
            $('[data-modal]').empty();
            $('[data-modal]').html(result.data);
            $('[data-csrf]').attr('value', result.csrf);
            $('#offermodal').modal('show');

        });
    }

    /* Kupon szerkesztés */
    $(document).on("click", "[data-offer-edit]", function() {
        var id = $(this).attr('data-offer-edit');
        dynamic_modal("edit", id);
    });

    /* Kupon törlés */
    $(document).on("click", "[data-offer-delete]", function() {
        var id = $(this).attr('data-offer-delete');
        dynamic_modal("delete", id);
    });

    /* Kupon törlés */
    $(document).on("click", "[data-offer-archive]", function() {
        var id = $(this).attr('data-offer-archive');
        dynamic_modal("archived", id);
    });

    /* Kupon mentés */
    $(document).on("click", "[data-offer-save]", function() {
        var _this = $(this);
        var formdata = $("#form-offer").serializeArray();
        formdata.push({
            "name": "validator_token",
            "value": $('[data-csrf]').attr('value')
        });

        $.ajax({
            url: $("[data-offer-url]").attr('data-offer-url'),
            method: "POST",
            data: formdata
        }).success(function(result) {
            result = JSON.parse(result);
            $('.red-border').removeClass('red-border');
            if (result.toastr.success) {
                $('#offermodal').modal('hide');
                toastr["success"](result.toastr.success);
            } else {
                toastr["error"](result.toastr.error);
                if (result.error) {
                    $.each(result.error, function(key, value) {
                        $("#" + key).addClass('red-border');
                        $("#" + key).val("");
                        $("#" + key).attr('placeholder', value);
                    });
                }
            }
            $('[data-csrf]').attr('value', result.csrf);
            offer.ajax.reload();
        });
    });
});
