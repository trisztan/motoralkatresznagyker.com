$(document).ready(function() {

    $(document).on("click", "[data-order-item-delete]", function() {
        var order_id = $('[data-order-id]').val();
        var item_sku = $(this).attr('data-order-item-delete');

        $.ajax({
            url: $("[data-order-item-delete-url]").val(),
            method: "POST",
            data: {
                "order_id": order_id,
                "item_sku": item_sku,
                "validator_token": $('[data-csrf]').attr('value')
            }
        }).success(function(result) {
            result = JSON.parse(result);
            if (result.toastr.success) {
                toastr["success"](result.toastr.success);
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                toastr["error"](result.toastr.error);
            }

            $('[data-csrf]').attr('value', result.csrf);

        });
    });

    /* Item import modal */
    $(document).on("click", "[data-order-item-add]", function() {
        var order_id = $('[data-order-id]').val();
        var item_sku = $('[data-item-add-sku]').val();
        var item_qty = $('[data-item-add-db]').val();
        var item_price = $('[data-item-add-price]').val();

        $('.red-border').removeClass('red-border');

        $.ajax({
            url: $("[data-order-item-add-url]").val(),
            method: "POST",
            data: {
                "order_id": order_id,
                "item_sku": item_sku,
                "item_qty": item_qty,
                "item_price": item_price,
                "validator_token": $('[data-csrf]').attr('value')
            }
        }).success(function(result) {
            result = JSON.parse(result);
            if (result.toastr.success) {
                toastr["success"](result.toastr.success);
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                toastr["error"](result.toastr.error);
                if (result.error) {
                    $.each(result.error, function(key, value) {
                        $("#" + key).addClass('red-border');
                        $("#" + key).val("");
                        $("#" + key).attr('placeholder', value);
                    });
                }
            }
            $('[data-csrf]').attr('value', result.csrf);

        });
    });

    /* Item import modal */
    $(document).on("click", "[data-order-save]", function() {
        var order_id = $('[data-order-id]').val();
        var formdata = $("#form-order").serializeArray();
        var email_alert = $('[data-order-alert]').prop('checked');

        $.ajax({
            url: $("[data-order-save-url]").val(),
            method: "POST",
            data: {
                "order_id": order_id,
                "form_data": formdata,
                "email_alert": email_alert,
                "validator_token": $('[data-csrf]').attr('value')
            }
        }).success(function(result) {
            result = JSON.parse(result);
            if (result.toastr.success) {
                toastr["success"](result.toastr.success);
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                toastr["error"](result.toastr.error);
            }
            $('[data-csrf]').attr('value', result.csrf);

        });
    });
});
