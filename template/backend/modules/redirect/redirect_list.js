$(document).ready(function() {
    var redirect = $('#redirect').DataTable({
        "responsive": true,
        "serverSide": true,
        "processing": true,
        "ajax": {
            url: $('[data-redirect-datatable-url]').val(),
            type: "post",
            data: function(e) {
                e.validator_token = $('[data-csrf]').attr('value');
            },
        },
        "fnDrawCallback": function(data) {
            $('[data-csrf]').attr('value', data.json.csrf);
        },
        "language": {
            "sEmptyTable": "Nincs rendelkezésre álló adat",
            "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
            "sInfoEmpty": "Nulla találat",
            "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
            "sInfoPostFix": "",
            "sInfoThousands": " ",
            "sLengthMenu": "_MENU_ találat oldalanként",
            "sLoadingRecords": "Betöltés...",
            "sProcessing": '<i class="fa fa-refresh fa-spin fa-2x fa-fw red">',
            "sSearch": "Keresés:",
            "sZeroRecords": "Nincs a keresésnek megfelelő találat",
            "oPaginate": {
                "sFirst": "Első",
                "sPrevious": "Előző",
                "sNext": "Következő",
                "sLast": "Utolsó"
            },
            "oAria": {
                "sSortAscending": ": aktiválja a növekvő rendezéshez",
                "sSortDescending": ": aktiválja a csökkenő rendezéshez"
            }
        }
    });

    function dynamic_modal(type, id) {
        $.ajax({
            url: $("[data-redirect-modal-url]").val(),
            method: "POST",
            data: {
                "type": type,
                "id": id,
                "validator_token": $('[data-csrf]').attr('value')
            }
        }).success(function(result) {
            result = JSON.parse(result);
            $('[data-modal]').empty();
            $('[data-modal]').html(result.data);
            $('[data-csrf]').attr('value', result.csrf);
            $('#redirectmodal').modal('show');

        });
    }

    /* Item import modal */
    $(document).on("click", "[data-redirect-add]", function() {
        dynamic_modal("add");
    });

    /* Kupon szerkesztés */
    $(document).on("click", "[data-redirect-edit]", function() {
        var id = $(this).attr('data-redirect-edit');
        dynamic_modal("edit", id);
    });

    /* Kupon törlés */
    $(document).on("click", "[data-redirect-delete]", function() {
        var id = $(this).attr('data-redirect-delete');
        dynamic_modal("delete", id);
    });

    /* Kupon mentés */
    $(document).on("click", "[data-redirect-save]", function() {
        var _this = $(this);
        var formdata = $("#form-redirect").serializeArray();
        formdata.push({
            "name": "validator_token",
            "value": $('[data-csrf]').attr('value')
        });

        $.ajax({
            url: $("[data-redirect-url]").attr('data-redirect-url'),
            method: "POST",
            data: formdata
        }).success(function(result) {
            result = JSON.parse(result);
            $('.red-border').removeClass('red-border');
            if (result.toastr.success) {
                $('#redirectmodal').modal('hide');
                toastr["success"](result.toastr.success);
            } else {
                toastr["error"](result.toastr.error);
                if (result.error) {
                    $.each(result.error, function(key, value) {
                        $("#" + key).addClass('red-border');
                        $("#" + key).val("");
                        $("#" + key).attr('placeholder', value);
                    });
                }
            }
            $('[data-csrf]').attr('value', result.csrf);
            redirect.ajax.reload();
        });
    });
});
