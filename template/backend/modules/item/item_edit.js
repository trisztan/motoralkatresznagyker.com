$(function() {
    /* Editor */
    $(".select2").select2({
        escapeMarkup: function(m) {
            return m;
        }
    });

    CKEDITOR.replace('item_longdesc');

    /* Url generálás */
    function slugify(text) {
        var a = [
            'À',
            'Á',
            'Â',
            'Ã',
            'Ä',
            'Å',
            'Æ',
            'Ç',
            'È',
            'É',
            'Ê',
            'Ë',
            'Ì',
            'Í',
            'Î',
            'Ï',
            'Ð',
            'Ñ',
            'Ò',
            'Ó',
            'Ô',
            'Õ',
            'Ö',
            'Ø',
            'Ù',
            'Ú',
            'Û',
            'Ü',
            'Ý',
            'ß',
            'à',
            'á',
            'â',
            'ã',
            'ä',
            'å',
            'æ',
            'ç',
            'è',
            'é',
            'ê',
            'ë',
            'ì',
            'í',
            'î',
            'ï',
            'ñ',
            'ò',
            'ó',
            'ô',
            'õ',
            'ö',
            'ø',
            'ù',
            'ú',
            'û',
            'ü',
            'ý',
            'ÿ',
            'Ā',
            'ā',
            'Ă',
            'ă',
            'Ą',
            'ą',
            'Ć',
            'ć',
            'Ĉ',
            'ĉ',
            'Ċ',
            'ċ',
            'Č',
            'č',
            'Ď',
            'ď',
            'Đ',
            'đ',
            'Ē',
            'ē',
            'Ĕ',
            'ĕ',
            'Ė',
            'ė',
            'Ę',
            'ę',
            'Ě',
            'ě',
            'Ĝ',
            'ĝ',
            'Ğ',
            'ğ',
            'Ġ',
            'ġ',
            'Ģ',
            'ģ',
            'Ĥ',
            'ĥ',
            'Ħ',
            'ħ',
            'Ĩ',
            'ĩ',
            'Ī',
            'ī',
            'Ĭ',
            'ĭ',
            'Į',
            'į',
            'İ',
            'ı',
            'Ĳ',
            'ĳ',
            'Ĵ',
            'ĵ',
            'Ķ',
            'ķ',
            'Ĺ',
            'ĺ',
            'Ļ',
            'ļ',
            'Ľ',
            'ľ',
            'Ŀ',
            'ŀ',
            'Ł',
            'ł',
            'Ń',
            'ń',
            'Ņ',
            'ņ',
            'Ň',
            'ň',
            'ŉ',
            'Ō',
            'ō',
            'Ŏ',
            'ŏ',
            'Ő',
            'ő',
            'Œ',
            'œ',
            'Ŕ',
            'ŕ',
            'Ŗ',
            'ŗ',
            'Ř',
            'ř',
            'Ś',
            'ś',
            'Ŝ',
            'ŝ',
            'Ş',
            'ş',
            'Š',
            'š',
            'Ţ',
            'ţ',
            'Ť',
            'ť',
            'Ŧ',
            'ŧ',
            'Ũ',
            'ũ',
            'Ū',
            'ū',
            'Ŭ',
            'ŭ',
            'Ů',
            'ů',
            'Ű',
            'ű',
            'Ų',
            'ų',
            'Ŵ',
            'ŵ',
            'Ŷ',
            'ŷ',
            'Ÿ',
            'Ź',
            'ź',
            'Ż',
            'ż',
            'Ž',
            'ž',
            'ſ',
            'ƒ',
            'Ơ',
            'ơ',
            'Ư',
            'ư',
            'Ǎ',
            'ǎ',
            'Ǐ',
            'ǐ',
            'Ǒ',
            'ǒ',
            'Ǔ',
            'ǔ',
            'Ǖ',
            'ǖ',
            'Ǘ',
            'ǘ',
            'Ǚ',
            'ǚ',
            'Ǜ',
            'ǜ',
            'Ǻ',
            'ǻ',
            'Ǽ',
            'ǽ',
            'Ǿ',
            'ǿ'
        ];

        var b = [
            'A',
            'A',
            'A',
            'A',
            'A',
            'A',
            'AE',
            'C',
            'E',
            'E',
            'E',
            'E',
            'I',
            'I',
            'I',
            'I',
            'D',
            'N',
            'O',
            'O',
            'O',
            'O',
            'O',
            'O',
            'U',
            'U',
            'U',
            'U',
            'Y',
            's',
            'a',
            'a',
            'a',
            'a',
            'a',
            'a',
            'ae',
            'c',
            'e',
            'e',
            'e',
            'e',
            'i',
            'i',
            'i',
            'i',
            'n',
            'o',
            'o',
            'o',
            'o',
            'o',
            'o',
            'u',
            'u',
            'u',
            'u',
            'y',
            'y',
            'A',
            'a',
            'A',
            'a',
            'A',
            'a',
            'C',
            'c',
            'C',
            'c',
            'C',
            'c',
            'C',
            'c',
            'D',
            'd',
            'D',
            'd',
            'E',
            'e',
            'E',
            'e',
            'E',
            'e',
            'E',
            'e',
            'E',
            'e',
            'G',
            'g',
            'G',
            'g',
            'G',
            'g',
            'G',
            'g',
            'H',
            'h',
            'H',
            'h',
            'I',
            'i',
            'I',
            'i',
            'I',
            'i',
            'I',
            'i',
            'I',
            'i',
            'IJ',
            'ij',
            'J',
            'j',
            'K',
            'k',
            'L',
            'l',
            'L',
            'l',
            'L',
            'l',
            'L',
            'l',
            'l',
            'l',
            'N',
            'n',
            'N',
            'n',
            'N',
            'n',
            'n',
            'O',
            'o',
            'O',
            'o',
            'O',
            'o',
            'OE',
            'oe',
            'R',
            'r',
            'R',
            'r',
            'R',
            'r',
            'S',
            's',
            'S',
            's',
            'S',
            's',
            'S',
            's',
            'T',
            't',
            'T',
            't',
            'T',
            't',
            'U',
            'u',
            'U',
            'u',
            'U',
            'u',
            'U',
            'u',
            'U',
            'u',
            'U',
            'u',
            'W',
            'w',
            'Y',
            'y',
            'Y',
            'Z',
            'z',
            'Z',
            'z',
            'Z',
            'z',
            's',
            'f',
            'O',
            'o',
            'U',
            'u',
            'A',
            'a',
            'I',
            'i',
            'O',
            'o',
            'U',
            'u',
            'U',
            'u',
            'U',
            'u',
            'U',
            'u',
            'U',
            'u',
            'A',
            'a',
            'AE',
            'ae',
            'O',
            'o'
        ];

        var i = a.length;

        while (i--) {
            text = text.replace(a[i], b[i]);
        }

        return text.toString().toLowerCase();
    }

    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 8; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    $("[data-slug-generate]").click(function() {
        var productName = $("#inputProductName");
        var productSlug = $("#inputProductUrl");

        var slug = slugify(productName.val());
        productSlug.val(slug);
    });

    $("[data-sku-generate]").click(function() {
        $("#inputProductSku").val(makeid());
    });

    /* Akciós dátum */
    $('#saledate').daterangepicker({
        format: 'YYYY/MM/DD',
        locale: {
            cancelLabel: 'Bezárás',
            applyLabel: 'Beállítás',
            fromLabel: 'Mettől',
            toLabel: 'Meddig'
        }
    });

    $(document).on("click", "[data-item-default]", function() {
        var _this = $(this);
        $.ajax({
            url: $("[data-item-imagedefault]").val(),
            method: "POST",
            data: {
                "validator_token": $('[data-csrf]').attr('value'),
                "item_id": $('[data-item-id]').val(),
                "image_id": _this.attr("data-item-default")
            }
        }).success(function(result) {
            result = JSON.parse(result);
            $('[data-csrf]').attr('value', result.csrf);
            $(".red-border").removeClass("red-border");
            _this.parent().parent().fadeIn(300, function() {
                $(this).addClass("red-border");
            });
        });
    });

    $(document).on("click", "[data-item-remove]", function() {
        var _this = $(this);
        $.ajax({
            url: $("[data-item-imageremove]").val(),
            method: "POST",
            data: {
                "validator_token": $('[data-csrf]').attr('value'),
                "item_id": $('[data-item-id]').val(),
                "image_id": _this.attr("data-item-remove"),
                "store_id": _this.attr("data-item-store")
            }
        }).success(function(result) {
            result = JSON.parse(result);
            $('[data-csrf]').attr('value', result.csrf);
            _this.parent().parent().fadeOut(300, function() {
                $(this).remove();
            });
        });
    });

    Dropzone.options.myAwesomeDropzone = {
        maxFilesize: 10,
        url: $('[data-item-imageupload]').val(),
        uploadMultiple: false,
        parallelUploads: 1,
        acceptedFiles: "image/jpg,image/jpeg",
        maxFiles: 100,
        init: function() {
            this.on("sending", function(file, xhr, formData) {
                formData.append("item_id", $('[data-item-id]').val());
                formData.append("validator_token", $('[data-csrf]').attr('value'));
            });
            this.on("error", function(file, result) {
                result = JSON.parse(result);
                toastr["error"](result);
            });
            this.on("success", function(file, result) {
                result = JSON.parse(result);
                $('[data-csrf]').attr('value', result.csrf);
                $(".dz-success").remove();
                $("#my-awesome-dropzone").append('<div class="dz-preview dz-processing dz-image-preview">' +
                    '<div class="dz-image">' +
                    '<img src="' + result.imgurl + '"' + 'height="90" width="100"></div><div class="text-center">' + '<button type="button" data-toggle="tooltip" data-placement="top" title="Törlés" data-item-remove="' + result.file_id + '" ' + 'data-item-store="' + result.file_store_id + '" class="btn btn-box-tool red" ' + '><i class="fa fa-2x fa-times">' + '</i></button>' + '<button type="button" data-toggle="tooltip" data-placement="top" title="Alapértelmezett kép" data-item-default="' + result.file_id + '" ' + 'class="btn btn-box-tool green"><i class="fa fa-2x fa-picture-o">' + '</i></button>' + '</div></div>');
            });
        }
    };

});
