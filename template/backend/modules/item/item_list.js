$(document).ready(function () {
    $('#item').DataTable({
        "responsive": true,
        "serverSide": true,
        "processing": true,
        "ajax": {
            url: $('[data-item-datatable-url]').val(),
            type: "post",
            data: function(e) {
                e.validator_token = $('[data-csrf]').attr('value');
            },
        },
        "fnDrawCallback": function(data) {
            $('[data-csrf]').attr('value',data.json.csrf);
        },
        "language": {
            "sEmptyTable": "Nincs rendelkezésre álló adat",
            "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
            "sInfoEmpty": "Nulla találat",
            "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
            "sInfoPostFix": "",
            "sInfoThousands": " ",
            "sLengthMenu": "_MENU_ találat oldalanként",
            "sLoadingRecords": "Betöltés...",
            "sProcessing": '<i class="fa fa-refresh fa-spin fa-2x fa-fw red">',
            "sSearch": "Keresés:",
            "sZeroRecords": "Nincs a keresésnek megfelelő találat",
            "oPaginate": {
                "sFirst": "Első",
                "sPrevious": "Előző",
                "sNext": "Következő",
                "sLast": "Utolsó"
            },
            "oAria": {
                "sSortAscending": ": aktiválja a növekvő rendezéshez",
                "sSortDescending": ": aktiválja a csökkenő rendezéshez"
            }
        }
    });

    function dynamic_modal(type,id) {
      $.ajax({
          url: $("[data-item-modal-url]").val(),
          method: "POST",
          data: {"type":  type,
                 "id":id,
                 "validator_token" : $('[data-csrf]').attr('value')}
      }).success(function (result) {
          result = JSON.parse(result);
          $('[data-modal]').empty();
          $('[data-modal]').html(result.data);
          $('[data-csrf]').attr('value',result.csrf);
          $('#itemmodal').modal('show');

      });
       fileupload();
    }

    /* Item import modal */
    $(document).on("click", "[data-item-modal-import]", function () {
        dynamic_modal("import");
    });
    function fileupload(){
        var myDropzone = Dropzone.options.myAwesomeDropzone = {
            maxFilesize: 10,
            url: $('[data-item-import-upload-url]').val(),
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 100,
            init: function () {
                this.on("sending", function (file, xhr, formData) {
                    formData.append("validator_token", $('[data-csrf]').attr('value'));
                });
                this.on("error", function (file, result) {
                    result = JSON.parse(result);
                    toastr["error"](result.message);
                });
                this.on("success", function (file, result) {
                    result = JSON.parse(result);
                    $('[data-csrf]').attr('value',result.csrf);
                    $(".dz-success").remove();
                    toastr[result.type](result.message);
                });
            }
        };
    }
    fileupload();

});
