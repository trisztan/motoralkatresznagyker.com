$(function() {

    "use strict";

    /* Morris.js Charts */
    // Sales chart

    $("[dashboard-stat]").change(function() {
        salesChart($("[dashboard-stat]").val());
    });

    function salesChart(type) {
        $("#week-sales-stat").html('<div class="loading-bar"><i class="fa fa-refresh fa-spin fa-2x fa-fw red"></i></div>');
        $.ajax({
            url: $('[data-sales-chart]').val(),
            type: 'post',
            data: {
                data: type,
                validator_token: $('[data-csrf]').attr('value')
            },
            success: function(data) {
                var type = $.parseJSON(data).type;
                $("#week-sales-stat").empty();
                new Morris.Area({
                    element: 'week-sales-stat',
                    resize: true,
                    data: $.parseJSON(data).result,
                    xkey: 'y',
                    xLabels: type,
                    ykeys: 'a',
                    yLabelFormat: function(x) {
                        return x.toFixed(0) + " Ft";
                    },
                    xLabelFormat: function(d) {
                        if (type == 'day') {
                            return d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
                        }
                        if (type == 'month') {
                            return d.getFullYear() + "-" + (d.getMonth() + 1);
                        }
                        if (type == 'year') {
                            return d.getFullYear();
                        }
                    },
                    labels: ["Rendelések"],
                    lineColors: [
                        '#a0d0e0', '#3c8dbc'
                    ],
                    hideHover: 'auto'
                });
                $('[data-csrf]').attr('value', $.parseJSON(data).csrf);
            }
        });
    }

    salesChart("day");

});
