$(document).ready(function() {

    function config_list(data) {
        if (data) {
            $('#config_list').DataTable().destroy();
            $('#config_list > tbody').html(data);
        }

        $('#config_list').DataTable({
            "paging": true,
            "lengthChange": true,
            "ordering": false,
            "order": [],
            "searching": true,
            "info": true,
            "autoWidth": false,
            "language": {
                "sEmptyTable": "Nincs rendelkezésre álló adat",
                "sInfo": "Találatok: _START_ - _END_ Összesen: _TOTAL_",
                "sInfoEmpty": "Nulla találat",
                "sInfoFiltered": "(_MAX_ összes rekord közül szűrve)",
                "sInfoPostFix": "",
                "sInfoThousands": " ",
                "sLengthMenu": "_MENU_ találat oldalanként",
                "sLoadingRecords": "Betöltés...",
                "sProcessing": "Feldolgozás...",
                "sSearch": "Keresés:",
                "sZeroRecords": "Nincs a keresésnek megfelelő találat",
                "oPaginate": {
                    "sFirst": "Első",
                    "sPrevious": "Előző",
                    "sNext": "Következő",
                    "sLast": "Utolsó"
                },
                "oAria": {
                    "sSortAscending": ": aktiválja a növekvő rendezéshez",
                    "sSortDescending": ": aktiválja a csökkenő rendezéshez"
                }
            }
        });
    }

    config_list();

    /* Dinamikus modal betöltés */
    function dynamic_modal(type, id) {
        $.ajax({
            url: $("[data-config-modal-url]").val(),
            method: "POST",
            data: {
                "type": type,
                "id": id,
                "validator_token": $('[data-csrf]').attr('value')
            }
        }).success(function(result) {
            result = JSON.parse(result);
            $('[data-modal]').empty();
            $('[data-modal]').html(result.data);
            $('[data-csrf]').attr('value', result.csrf);
            $('#configmodal').modal('show');
        });
    }

    /* Konfig szerkesztése */
    $(document).on("click", "[data-config-edit]", function() {
        var id = $(this).attr('data-config-edit');
        dynamic_modal("edit", id);
    });

    /* Kupon mentés */
    $(document).on("click", "[data-config-save]", function() {
        var _this = $(this);
        var formdata = $("#form-config").serializeArray();
        formdata.push({
            "name": "validator_token",
            "value": $('[data-csrf]').attr('value')
        });
        console.log(formdata);

        $.ajax({
            url: $("[data-config-url]").attr('data-config-url'),
            method: "POST",
            data: formdata
        }).success(function(result) {
            result = JSON.parse(result);
            $('.red-border').removeClass('red-border');
            if (result.toastr.success) {
                $('#configmodal').modal('hide');
                toastr["success"](result.toastr.success);
            } else {
                toastr["error"](result.toastr.error);
                if (result.error) {
                    $.each(result.error, function(key, value) {
                        $("#" + key).addClass('red-border');
                        $("#" + key).val("");
                        $("#" + key).attr('placeholder', value);
                    });
                }
            }
            $('[data-csrf]').attr('value', result.csrf);
            config_list(result.data);
        });
    });
});
