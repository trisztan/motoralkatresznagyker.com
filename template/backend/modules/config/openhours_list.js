$(document).ready(function() {

    var businessHoursManager = $("#businessHoursWidget").businessHours({
        postInit: function() {
            $('.operationTimeFrom, .operationTimeTill').timepicker({
                'timeFormat': 'H:i',
                'step': 15
            });
        },
        weekdays: ['Hétfő', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat', 'Vasárnap'],
        operationTime: JSON.parse("[" + $('[data-openhours-data]').html() + "]"),
        dayTmpl: '<div class="dayContainer" style="width: 80px;">' +
            '<div data-original-title="" class="colorBox"><input type="checkbox" class="invisible operationState"></div>' +
            '<div class="weekday"></div>' +
            '<div class="operationDayTimeContainer">' +
            '<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-sun-o"></i></span><input type="text" name="startTime" class="mini-time form-control operationTimeFrom" value=""></div>' +
            '<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-moon-o"></i></span><input type="text" name="endTime" class="mini-time form-control operationTimeTill" value=""></div>' +
            '</div></div>'
    });

    $('[data-openhours-save]').click(function() {
        $.ajax({
            url: $('[data-openhours-save-url]').val(),
            method: 'POST',
            data: {
                'bhours': JSON.stringify(businessHoursManager.serialize()),
                "validator_token": $('[data-csrf]').attr('value')
            }
        }).success(function(result) {
            result = JSON.parse(result);
            if (result.success) {
                toastr["success"](result.success);
            } else {
                toastr["error"](result.error);
            }

            $('[data-csrf]').attr('value', result.csrf);
        });
    });
});
